function [ index ] = parforIndices( ranges, i )
%PARFORINDICES Maps integers to indices into a multidimensional array.
%   
% INPUTS:
%   ranges - (1xn) vector of positive integer values, e.g. [1,2,3]
%   specifies a 1x2x3 matrix.
%   i - specifies i^{th} element in the multidimensional array to find an
%       index for.
% OUTPUTS:
%   index - (1xn) vector of positive integer values
%
% author: Matt Faulkner

[m,n] = size(ranges);
assert(m==1); 
assert(n>=1);
maxIndex = prod(ranges);
assert(i>0);
assert(i<=maxIndex); %uncomment this to allow wrap around


index = zeros(1,n);
previousTerms = 1;
for k=0:n-1
    r = ranges(k+1);
    index(k+1) = ceil(mod( floor((i-1)/previousTerms),r))+1;
    previousTerms = previousTerms*r;
end

end

%% 
% A small, related example: here the goal is to iterate over the parameter
% size, and for each size value perform several iterations of a
% computation. To parallelize with parfor, an intermediate cell array is
% created (as a vector to play nicely with parfor) and indexed into.
% Finally, the vector is reshaped to give a square array, where each column
% corresponds to a parameter value, and each row to an iteration.


% nSizes = 3;
% nIterations = 4;
% nComputations = nSizes*nIterations;
% cOutputs = cell(nComputations,1);
% for i=0:nComputations-1
%     iterationIndex = floor(i/nSizes) + 1;
%     sizeIndex = mod(i,nSizes) + 1;
%     disp(['iter: ' num2str(iterationIndex)  ', sizeIndex: '  num2str(sizeIndex)]);
%     cOutputs{i+1} = ['s = ' num2str(sizeIndex) ' , iteration = ' num2str(iterationIndex)];
% end
% cOutputs = reshape(cOutputs, nSizes, nIterations)';
% %outputs = cell2mat(cOutputs);

% Note: this might be turned into a useful function that takes a function
% handle, and applied the function to each parameter setting
