function openPool()
% openPool - try to open matlabpool if it is not currently open.
%


if matlabpool('size')==0
    try
        matlabpool open
    catch exception
        disp('Exception opening matlab pool')
        disp(exception)
    end
end


end

