function [ bool ] = containsNans( X )
% containsNans Check if an array contains any NaNs
%
% Input:
%   X -. An array - Should work for any dimensionality
%
% Output: 
%   bool - true if any element of X is a NaN

bool = any(isnan(reshape(X, numel(X), 1)));

end

