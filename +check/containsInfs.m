function [ bool ] = containsInfs( X )
% containsInfs Check if an array contains any Infs
%
% Input:
%   X -. An array - Should work for any dimensionality
%
% Output: 
%   bool - true if any element of X is a Inf

bool = any(isinf(reshape(X, numel(X), 1)));

end
