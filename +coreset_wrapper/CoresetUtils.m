classdef CoresetUtils
    % CoresetUtils - Wrapper class for various coreset functionality.
    %   
    % Convention: a data set is represented as a [d x n] matrix, where each
    %   of the n columns is a d-dimensional data point.
    %
    % FYI:
    %   coreset - weighted set of points.
    %
    % author: Matt Faulkner
    % 
    
    % ====================================================================
    
    properties
    end
    
    % ====================================================================
    
    methods(Static)
        
        % ----------------------------------------------------------------
        
        function [X, w, time, C]= computeCoresetKMedian(D, coresetSize, k, beta, W, delta, cType, outlier_resistant)
            %{ 
            
            Descriptions:
                computeCoresetKMedian - wrapper for KMedianCoresetAlg. 
            
                The function is modified to accept an additional argument 
                W as the weight for points in D, enabling the function to 
                construct coreset out of another coreset. See function 
                mergeCoresets (Dai Wei).
           
            Input:
               D - (d x n) matrix of data points. Each column is a data
                   point.
               coresetSize - size of the coreset. Also called "t". This is
                   the number of points drawn, WITH REPLACEMENT, so the size
                   exact size of the set returned may be less than coresetSize. 
               k - number of medians
               beta - KMedian parameter ("Should be on the order of k, maybe twice k?")
               W (optional) - [1 x n] row vector. Weights of the n points in D. (Dai
                              Wei)
               delta(optional) - KMedian parameter

               cType(optional) - KMedianCoresetAlg.linearInK or KMedianCoresetAlg.quadraticInK

           
            Output:
               X - (d x n') matrix of points. n' <= coresetSize
               w - (1 x n') vector. weight for each point in X
               time - time to compute coreset, in seconds.
               C - coreset struct, which is essentially a function set.
                   This contains the same info as X and w combined, but it
                   can be fed into KMedianCoresetAlg.mergedCoreset(...).
            
            %}
           % checks
           
           import coreset_wrapper.*;
           
           if ~exist('delta','var')
              delta = 0.1; 
           end
           
           if ~exist('cType', 'var')
             cType = KMedianCoresetAlg.quadraticInK; 
             %cType = KMedianCoresetAlg.linearInK; 
           end
           
           %{
           if ~exist('no_outliers', 'var')
             % Default to turn off the outlier resistant feature.
             outlier_resistant = KMedianCoresetAlg.NO_OUTLIER_RESISTANT;
           end
           %}
           
           [d,n] = size(D);
            
           % Create matrix.
           M=Matrix(n,d);
           M.m=D';
           M.matrix=D';
           
           % Create PointFunctionSet
           if ~exist('W', 'var')
                % the weight W is all 1's.
                P=PointFunctionSet(M);
           else
                % PointFunctionSet needs column vector for weight.
                M_w=Matrix(W');
                P=PointFunctionSet(M, M_w);
           end
           
           % compute coreset
           tic;
           kc =KMedianCoresetAlg();% the kmeans coreset algorithm.
           %CoresetUtils.setParams(kc, k, beta, delta, coresetSize, cType, outlier_resistant);
           CoresetUtils.setParams(kc, k, beta, delta, coresetSize, cType);
           %kc.setParameters(k, beta, delta, coresetSize, cType);
           kc.computeCoreset(P);
           C=kc.coreset;
           time = toc;
           
           %visualizeCoreset(C);
           
           X = C.M.m';
           if size(X,1) ~= d
              disp('computeCoresetKMedian: X has wrong size'); 
           end
           w = C.W.m';
 
           if size(X,2) < k
               warning(['number of points in coreset ', ...
                    int2str(size(X,2)), ' < k (', int2str(k),...
                    '). May cause problems in EM algorithm.']);
          % else
          %     disp(['Sampled ', int2str(coresetSize), ' points. |C| = ', int2str(size(X,2))]);
           end
        end
        
        
        
        %{
        Matt's original implementation.
        
        
        function [X, w, time]= computeCoresetKMedian(D, coresetSize, k, beta, delta, cType)
           % computeCoresetKMedian - wrapper for KMedianCoresetAlg 
           %
           % Input:
           %    D - (d x n) matrix of data points. Each column is a data
           %        point.
           %    coresetSize - size of the coreset. Also called "t". This is
           %        the number of points drawn, WITH REPLACEMENT, so the size
           %        exact size of the set returned may be less than coresetSize. 
           %    k - number of medians
           %    beta - KMedian parameter ("Should be on the order of k, maybe twice k?")
           %    delta - KMedian parameter

           %    cType - KMedianCoresetAlg.linearInK or KMedianCoresetAlg.quadraticInK
           %
           % Output:
           %    X - (d x n') matrix of points. n' <= coresetSize
           %    w - (1 x n') vector. weight for each point in X
           %    time - time to compute coreset, in seconds.
           
           % checks
           
           if ~exist('delta','var')
              delta = 0.1; 
           end
           
           if ~exist('cType', 'var')
             cType = KMedianCoresetAlg.quadraticInK; 
             %cType = KMedianCoresetAlg.linearInK; 
           end
           
           [d,n] = size(D);
            
           % Create matrix.
           M=Matrix(n,d);
           M.m=D';
           M.matrix=D';
           
           % Create PointFunctionSet
           P=PointFunctionSet(M);
           
           % compute coreset
           tic;
           kc =KMedianCoresetAlg();% the kmeans coreset algorithm.
           kc.setParameters(k, beta, delta, coresetSize, cType);
           kc.computeCoreset(P);
           C=kc.coreset;
           time = toc;
           
           %visualizeCoreset(C);
           
           X = C.M.m';
           if size(X,1) ~= d
              disp('computeCoresetKMedian: X has wrong size'); 
           end
           w = C.W.m';
 
        end
        %}
        
        % ----------------------------------------------------------------
        
        
        function setParams(kc, k, beta, delta, coresetSize, cType, no_outliers)
            %{
            
            Descriptions:
                This function is created to adjust to the changes in 
                KMedianCoresetAlg: function setParameters(...) is removed.
                This is a hackish way of passing the parameters.
            
            TODO:
                Make it into a function in KMedianCoresetAlg class.
            
            Input:
                kc - KMedianCoresetAlg object to receive the parameters.
            
            Author: Dai Wei
            
            %}
           kc.k = k;
           kc.bicriteriaAlg.beta = beta;
           kc.coresetType = cType;
           kc.t = coresetSize;
           %kc.outlier_resistant = no_outliers;
           % Ignore delta. Use default value delta = 0.1 set in 
        end
        
        % ----------------------------------------------------------------
        
        function [X, w, newCoreset]= ...
                mergeCoresets(C1, C2, coresetSize, k, beta, delta, cType)
            %{
                
            Descriptions:
                Wrapper function for mergedCoreset in KMedianCoresetAlg. A
                recommended usage is to get C from the 4th returned value
                in computeCoresetKMedian.
                
            Inputs:
                C1, C2 - Coreset objects (that is, PointFunctionSets)
                         representation of coresets.
                coresetSize, k, beta - See computeCoresetKMedian.
                delta, cType (optional) - See computeCoresetKMedian.
                
            Outputs:
                X, w - See docs in computeCoresetKMedian above. X is the
                       same as D.
                newCoreset - the same as C in docs in computeCoresetKMedian 
                             above.
                
            Author: Dai wei
            
            %}
            
            import coreset_wrapper.*;
                
            if ~exist('delta','var')
                delta = 0.1; 
            end
           
            if ~exist('cType', 'var')
                cType = KMedianCoresetAlg.quadraticInK; 
                %cType = KMedianCoresetAlg.linearInK; 
            end
           
            
            % Create KMedianCoresetAlg object and assign parameters
            kc =KMedianCoresetAlg();
            CoresetUtils.setParams(kc, k, beta, delta, coresetSize, cType);
            %kc.setParameters(k, beta, delta, coresetSize, cType);
            
            % Merge and compress.
            newCoreset = kc.mergedCoreset(C1, C2);
            
            % Convert the format.
            [X, w] = CoresetUtils.PointFunctionSet2Xw(newCoreset);
            
             
           if size(X,2) < k
                warning(['number of points in coreset ', ...
                    int2str(size(X,2)), ' < k (', int2str(k),...
                    '). May cause problems in EM algorithm.']);
           else
               disp(['number of points in coreset: ', int2str(size(X,2))]);
           end
        end
        
        % ----------------------------------------------------------------
        
        function [X, w] = PointFunctionSet2Xw(coreset)
            %{
            Descriptions:
                Convert coreset of type PointFunctionSet to simple
                representation X ([d x n] containing n data points in d
                dimensions) and w ([1 x n] weights for the points).
            
            Outputs:
                X, w - See docs in computeCoresetKMedian above. X is the
                       same as D.
            
            %}
            X = coreset.M.m';
            w = coreset.W.m';
        end
        
        
        % ----------------------------------------------------------------
        
        function coreset = Xw2PointFunctionSet(X, w)
            %{
            Descriptions:
                Convert X ([d x n] containing n data points in d
                dimensions) and w ([1 x n] weights for the points) to
                coreset of type PointFunctionSet.
            
            Inputs:
                X, w - See docs in computeCoresetKMedian above. X is the
                       same as D.
            
            
            %}
            
            coreset = KMedianCoresetAlg.matrixToFunctionSet(X', w');
        end
            
            
            
        
    end
    
    % ====================================================================
end

