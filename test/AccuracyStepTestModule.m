classdef AccuracyStepTestModule < handle
%{
    
Descriptions:
    This class is similar to AccuracyTestModule, except that it isn't
    parallelized and works only for the streaming method.
    
Author: Alejandro (2011) (Based on code from Dai Wei)
    
%}
    
    % ====================================================================
    
    properties
        model %  Model
        count % Number of times ran, to calculate nk
    end
    
    % ====================================================================
    methods
        function obj =  AccuracyStepTestModule()
            % For initialization
        end
        
        function [llh, auc, amodel, time] = stepAccuracyMetrics(obj, Xtrain, Wtrain, Xtest, testLabels, k)
        %{
        
        Inputs:
            Xtrain - [d x n] dataset.
            Wtrain = [1 x n] vector as weights.
            Xtest - [d x n'] dataset.
            testLabels - [1 x n'] integer vector. 1 = earthquake (positive), 2 =
                        negative.
            k - Scalar, number of gaussians
            
        Outputs:
            llh - scalar. Log-likelihood.
            auc - scalar. Area Under ROC Curve.
            model - gmm object that fits Xtrain, Wtrain.
            
        %}
           
            assert(size(Xtest, 2) == size(testLabels, 2), ...
                'Test set size and label size mismatch.');
            
            if ~exist('k', 'var')
                k = 6;
            end 
            
            ticID = tic;
            
            alpha = .55;
            
            if isempty(obj.model)
                obj.count = 1;
                obj.model = gmm.Gmm.fitGmmWeighted(Xtrain, Wtrain, k, 5);

            else
               nk = (obj.count+3)^(-alpha);
               omodel.mu = obj.model.mu;
               omodel.Sigma = obj.model.Sigma;
               omodel.weight = obj.model.weight';
               obj.model = gmm.Gmm.gmmWeightedStep(Xtrain, k, nk, omodel);
               obj.count=obj.count+1;  
            end
            
            time = toc(ticID); % Excluding testing time.
            
            % Find the negative (no earthquake) data to compute LLH.
            llh = obj.model.logLikelihood(Xtest(:,testLabels == 2));
           
            auc = AccuracyStepTestModule.computeROCMetrics(obj.model, Xtest, testLabels);
            
            amodel = obj.model;
        end
        
    end
    
    methods (Static)
        
 
        %------------------------------------------------------------------------
        
        function [AUC, tp, fp] = computeROCMetrics(model, Xtest, testLabels)
        %{
            
        Inputs:
            model - a gmm object.
            Xtest - [d x n] data set.
            testLabels - [1 x n] integer vector. See basicMetrics.

        Outputs:
            AUC - scalar. Area Under ROC Curve
            tp - [nThresh x 1] vector. True positive rate.
            fp - [nThresh x 1] vector. False positive rate.
            
            Note that tp, fp is only necessary for plotting ROC curve. AUC
            itself is already a good metric for performance comparison.
            
        %}
            
            import roc.*;
            
            % Number of threasholds.
            nThresh = 1000;
            
            assert(size(Xtest, 2) == size(testLabels, 2), ...
                'Test set size and label size mismatch.');
            
            P = model.evaluateProbability(Xtest);

            Assignments = -log(P);

            % a low hack to deal with the infinite!
            infIndices = (Assignments == inf);
            Assignments(infIndices) = 1000;
            
            Thresholds = linspace(min(Assignments), max(Assignments), nThresh);
            [tp, fp] =  myROC(testLabels, Assignments, Thresholds);
            AUC = auc(fp,tp);            
        end
        
        % ----------------------------------------------------------------
        
    end
    
end

