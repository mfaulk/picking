%{

Test merging functionality in coreset.

%}

import gmm.*

% ========== Create gmm1 ===============

% number of components:
k = 1;

% dimensionality:
d = 2;

% create means in some bounded square:
scaleFactor = 10;
mu = scaleFactor*rand(k,d) - scaleFactor/2;

% and random variance
sigma = zeros(d,d,k);
for i=1:k
   sigma(:,:,i) = 5 * rand(d,d) .* eye(d); 
end

% mixing weights
p = rand(1,k);
p = p / sum(p);

gmm1 = Gmm(mu', sigma, p');


% ======= Create gmm2 ==========

% create means in some bounded square:
scaleFactor = 10;
mu = scaleFactor*rand(k,d) - scaleFactor/2;

% and random variance
sigma = zeros(d,d,k);
for i=1:k
   sigma(:,:,i) = 5 * rand(d,d) .* eye(d); 
end

% mixing weights
p = rand(1,k);
p = p / sum(p);

gmm2 = Gmm(mu', sigma, p');


nsample = 1000;
s1 = gmm1.sample(nsample);
s2 = gmm2.sample(nsample);


% ========== Use Coreset ===========

%%%%%%%%%%%%%%%%%%%%%%%% Variable to be changed %%%%%%%%%%%%%%%%%%%
coreParams.coresetSize = 1000;   % small compared with chunksize
coreParams.k = 1;  % 6 Gaussians
coreParams.beta = 2 * coreParams.k;

CTS = cumulativeTreeStorage(coreParams, 2);
[X, w] = CTS.addChunkData(s1);
disp(['Coreset returns ', int2str(size(X,2)), ' points.']);
plot(s1(1,:)', s1(2,:)', '.r');
hold on;
scatter(X(1,:)', X(2,:)', w'.*3, 'filled');

figure();
[X, w] = CTS.addChunkData(s2);
plot([s1(1,:)';s2(1,:)'], [s1(2,:)';s2(2,:)'], '.r');
hold on;
scatter(X(1,:)', X(2,:)', w'.*3, 'filled');
disp(['Coreset returns ', int2str(size(X,2)), ' points.']);

