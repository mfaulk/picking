classdef AccuracyTestModule
%{
    
Descriptions:
    This class provides modular functions to help test. The idea is to
    break tests into many small problems so that they can be used in the
    future. Note that this class is in many ways similar to
    class coreset_old.AccuracyUtils, but it's sort of an rearrangement.
    
Author: Dai Wei (2011)
    
%}
    
    % ====================================================================
    
    properties
    end
    
    % ====================================================================
    
    
    methods (Static)
        
        % ----------------------------------------------------------------
        
        function [llh, auc, model, time] = basicAccuracyMetrics(Xtrain, Wtrain, Xtest, testLabels, k, nRestart)
        %{
        
        Inputs:
            Xtrain - [d x n] dataset.
            Wtrain = [1 x n] vector as weights.
            Xtest - [d x n'] dataset.
            testLabels - [1 x n'] integer vector. 1 = earthquake (positive), 2 =
                        negative.
            k - scalar (optional). Number of Gaussians.
            nRestart - scalar (optional). Number of restart in EM
                       algorithm.
        
        Outputs:
            llh - scalar. Log-likelihood.
            auc - scalar. Area Under ROC Curve.
            model - gmm object that fits Xtrain, Wtrain.
            time - scalar. runtime.
            
        %}
           
            assert(size(Xtest, 2) == size(testLabels, 2), ...
                'Test set size and label size mismatch.');
            
            if ~exist('nRestart', 'var')
                nRestart = 5;
            end
            
            if ~exist('k', 'var')
                k = 6;
            end            
            
            ticID = tic;
            
            model = gmm.Gmm.fitGmmWeighted(Xtrain, Wtrain, k, nRestart);
            
            time = toc(ticID);  % The time should just be the fitting time, excluding testing time.
            
            % Find the negative (no earthquake) data to compute LLH.
            llh = model.logLikelihood(Xtest(:,testLabels == 2));
           
            auc = AccuracyTestModule.computeROCMetrics(model, Xtest, testLabels);
            
            
        end
        
        
        % ----------------------------------------------------------------
                
        function [AUC, tp, fp] = computeROCMetrics(model, Xtest, testLabels)
        %{
            
        Inputs:
            model - a gmm object.
            Xtest - [d x n] data set.
            testLabels - [1 x n] integer vector. See basicMetrics.

        Outputs:
            AUC - scalar. Area Under ROC Curve
            tp - [nThresh x 1] vector. True positive rate.
            fp - [nThresh x 1] vector. False positive rate.
            
            Note that tp, fp is only necessary for plotting ROC curve. AUC
            itself is already a good metric for performance comparison.
            
        %}
            
            import roc.*;
            
            % Number of threasholds.
            nThresh = 1000;
            
            assert(size(Xtest, 2) == size(testLabels, 2), ...
                'Test set size and label size mismatch.');
            
            P = model.evaluateProbability(Xtest);

            Assignments = -log(P);

            % a low hack to deal with the infinite!
            infIndices = (Assignments == inf);
            Assignments(infIndices) = 1000;
            
            Thresholds = linspace(min(Assignments), max(Assignments), nThresh);
            [tp, fp] =  myROC(testLabels, Assignments, Thresholds);
            AUC = auc(fp,tp);            
        end

        % ----------------------------------------------------------------
    end
    
    % ====================================================================
    
end

