%% phoneTest
%
%
%



%tree = cumulativeTreeStorage(XXX)

%% Generate the time segments

import phone.*

acsnFile = 'tutorial/acsn/m2.acsn';
acsnDir = 'tutorial/acsn';

phoneRecord = PhoneRecord(acsnFile);
cPhoneRecords = PhoneRecord.loadPhoneRecordDir(acsnDir);

% the synthetic package contains functions for making synthetic
% earthquakes, but also some general purpose code for segmenting phone
% records
import synthetic.*

samplesPerSecond = 50; 

% later, I'm going to make Long-term, short-term features, so I'll make the
% segments long enough to leave room for those computations:
ltLength = 5; % seconds
stLength = 2.5; % seconds
deadTime = 1; % seconds of "dead time" between short term and long term buffers
segmentLengthSeconds = ltLength + stLength +deadTime;

% these are UniformTimeSeries objects, in the timeSeries package.
cPhoneSegments = SegmentSynthesizer.segmentPhoneRecord(cPhoneRecords{1}, segmentLengthSeconds, samplesPerSecond);
plot(cPhoneSegments{1}.getTimes, cPhoneSegments{1}.X);

% alternatively, you could segment each file and concatenate the segments, like this:
cPhoneSegments = {};
for i=1:length(cPhoneRecords)
   cTemp = SegmentSynthesizer.segmentPhoneRecord(cPhoneRecords{i}, segmentLengthSeconds, samplesPerSecond);
   cPhoneSegments = [cPhoneSegments; cTemp]; % for some reason using {} here doesn't work. Look into this...
end

%% Compute Feature Vectors
import gmm.*;

featureModel =  GmLtStParameterSelection();




%% Add the Times to the tree

AccuracyUtils.accuracyAcrossSetSizeParallelv2(cPhoneSegments, Xtest, nIterations, coresetParams, setSizes)         
