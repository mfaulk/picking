%% Step 0: Setups
import online.*
import feature.*
import parallel.*
import gmm.*

%clear;
%clc;

% Set up email
genericCoresetTestUtil.gmailSetup();


load('experiments/coreset_vs_onlineEM/preprocessedMAT/7recordings_combined_262hrs');
%load('experiments/coreset_vs_onlineEM/preprocessedMAT/rjc_10022010.mat');
nChunks = 70; % Keep it < 100, so that nChunks * 3 < 100
cTest_set = cTest_set(:,1:nChunks);
cTrain_set = cTrain_set(:,1:nChunks);


% repeat recordings by increasing nChunks.
nChunks_real = nChunks; 
nEpoch = 3;
nChunks = nEpoch * nChunks_real;  % run the same data multiple times.


% The k's we want to try.
ks = [6]; %[6, 12, 24, 48];
nks = length(ks);

% Prepare all training set, test set in matrix format.
nTest_perHour = size(feature.Feature.unpackFeatureVectors(cTest_set(:,1)), 2); % Half of it is synthetic earthquake
nTrain_perHour = size(feature.Feature.unpackFeatureVectors(cTrain_set(:,1)), 2);
Xtest_all = zeros(17, nChunks_real * nTest_perHour);
Ltest_all = zeros(1, nChunks_real * nTest_perHour); % test labels (row vector)
Xtrain_all = zeros(17, nChunks_real * nTrain_perHour);
for t = 1:nChunks_real
    
    % Unpack test set and test labels
    istart_test = (t - 1) * nTest_perHour + 1;
    iend_test = istart_test + nTest_perHour - 1;
    Xtest_all(:,istart_test:iend_test) = feature.Feature.unpackFeatureVectors(cTest_set(:,t));
    Ltest_all(:,istart_test:iend_test) = feature.Feature.unpackLabels(cTest_set(:,t))';
    
    % Unpack training set.
    istart_train = (t - 1) * nTrain_perHour + 1;
    iend_train = istart_train + nTrain_perHour - 1;
    Xtrain_all(:,istart_train:iend_train) = feature.Feature.unpackFeatureVectors(cTrain_set(:,t));
end

LLH_all = zeros(nks, nChunks);
AUC_all = zeros(nks, nChunks);
Time_all = zeros(nks, nChunks);

%% Main loop.

% Store models for examination
models = cell(nks, nChunks);

openPool();
for ik = 1:nks % loop through each k.
    
    ticID_k = tic;
    
    k = ks(ik);
   
    parfor t = 1:nChunks % Parfor on t instead of ik.

        ticID_aChunk = tic;

        % Get the test and training set (full history)
        Xtest = genericCoresetTestUtil.repData(Xtest_all, nChunks_real, t, nTest_perHour);
        Ltest = genericCoresetTestUtil.repData(Ltest_all, nChunks_real, t, nTest_perHour);
        Xtrain = genericCoresetTestUtil.repData(Xtrain_all, nChunks_real, t, nTrain_perHour);
        
        Wtest = ones(1,t * nTrain_perHour);
        
        % EM
        [LLH_all(ik, t), AUC_all(ik, t), models{ik,t}, Time_all(ik, t)] = AccuracyTestModule.basicAccuracyMetrics(Xtrain, Wtest, Xtest, Ltest, k);
        
        disp(['==> t = ', int2str(t), ' completed in ', int2str(toc(ticID_aChunk)), ' seconds.']);
    end
    
    msg = ['+++> k = ', int2str(k), ' completed in ', int2str(toc(ticID_k)), ' seconds.'];
    disp(msg);
    sendmail('daiwei89@gmail.com', 'Mail from MATLAB', msg);
end
matlabpool close;



%% Compute cumulative LLH and AUC and time.
LLH_cum = zeros(nks, nChunks);
AUC_cum = zeros(nks, nChunks);
Time_cum = zeros(nks, nChunks);

for ik = 1:nks
    LLH_cum(ik, 1) = LLH_all(ik, 1);
    AUC_cum(ik, 1) = AUC_all(ik, 1);
    Time_cum(ik, 1) = Time_all(ik, 1);
    for t = 2:nChunks
        LLH_cum(ik, t) = (LLH_cum(ik, t-1) * (t-1) + LLH_all(ik, t)) / t;
        AUC_cum(ik, t) = (AUC_cum(ik, t-1) * (t-1) + AUC_all(ik, t)) / t;
        Time_cum(ik, t) = (Time_cum(ik, t-1) * (t-1) + Time_all(ik, t)) / t;
    end
end



%% Ploting cumulative LLH and AUC
figure()
coordX = linspace(1,nChunks,nChunks);
for ik = 1:nks
    subplot(2,1,1);
    plot(coordX, LLH_cum(ik, :)');
    xlabel('hours')
    ylabel('Cumulative Average LLH')
    title('Full Dataset');
    hold all;
    
    subplot(2,1,2);
    plot(coordX, AUC_cum(ik, :)');
    xlabel('hours')
    ylabel('Cumulative Average AUC')
    hold all;
end
legend({['k = ', int2str(ks(1))]}, 'Location', 'Southeast')



%% Compute cumulative LLH and AUC by EPOCH
LLH_cum_epoch = zeros(nEpoch, nks, nChunks_real);
AUC_cum_epoch = zeros(nEpoch, nks, nChunks_real);
Time_cum_epoch = zeros(nEpoch, nks, nChunks_real);

for iepoch = 1:nEpoch
    t_start = (iepoch - 1) * nChunks_real + 1;
    t_end = t_start + nChunks_real - 1;
    for ik = 1:nks
        LLH_cum_epoch(iepoch, ik, 1) = LLH_all(ik, t_start);
        AUC_cum_epoch(iepoch, ik, 1) = AUC_all(ik, t_start);
        Time_cum_epoch(iepoch, ik, 1) = Time_all(ik, t_start);
        for t = t_start + 1:t_end
            %disp(int2str(t));
            t_1 = rem(t-1, nChunks_real);
            t_0 = rem(t, nChunks_real);
            if t_0 == 0, t_0 = nChunks_real; end
            if t_1 == 0, error('something strange in stat calculation'); end
            LLH_cum_epoch(iepoch, ik, t_0) = (LLH_cum_epoch(iepoch, ik, t_1) * t_1 + LLH_all(ik, t)) / t_0;
            AUC_cum_epoch(iepoch, ik, t_0) = (AUC_cum_epoch(iepoch, ik, t_1) * t_1 + AUC_all(ik, t)) / t_0;
            Time_cum_epoch(iepoch, ik, t_0) = (Time_cum_epoch(iepoch, ik, t_1) * t_1 + Time_all(ik, t)) / t_0;
        end
    end
end

% Plotting by epoch
figure()
coordX = linspace(1,nChunks_real,nChunks_real);
for iepoch = 1:nEpoch
    subplot(2,1,1);
    plot(coordX, squeeze(LLH_cum_epoch(iepoch, 1, :)));
    xlabel('hours')
    ylabel('Cumulative Average LLH')
    title('Full Dataset');
    hold all;
    
    subplot(2,1,2);
    plot(coordX, squeeze(AUC_cum_epoch(iepoch, 1, :)));
    xlabel('hours')
    ylabel('Cumulative Average AUC')
    title('k = 6');
    hold all;
end
legend({'epoch 1', 'epoch 2', 'epoch 3'}, 'Location', 'Southeast');


%% Plot Time stats
figure()
% Cumulative average time
subplot(2,1,1);
coordX = linspace(1,nChunks,nChunks);
plot(coordX, Time_cum(1, :)');
xlabel('hours')
ylabel('Cumulative Average Time (sec)')
title('Full Dataset: Time stats');
hold all;

subplot(2,1,2);
coordX = linspace(1,nChunks_real,nChunks_real);
for iepoch = 1:nEpoch
    plot(coordX, squeeze(Time_cum_epoch(iepoch, 1, :)));
    xlabel('hours')
    ylabel('Cumulative Average Time (sec)')
    hold all;
end
legend({'epoch 1', 'epoch 2', 'epoch 3'}, 'Location', 'Southeast');

% Cumulative average time by EPOCH

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END OF PROGRAM %%%%%%%%%%%%%%%%%%%%%%%%%%%%




%{



k = 6;
coresetParams.k = k;
coresetParams.beta = k*2;
nIterations = 1;


openPool();
parfor istat = 1 : nStorages
    allStats{istat}.LLH = zeros(nIterations, nChunks);
    allStats{istat}.AUC = zeros(nIterations, nChunks);
    allStats{istat}.Models = cell(nIterations, nChunks);
end


% Test set is the same for all iterations. Do it beforehand.
testSet_mat = cell(nChunks, 1);
TrueLabels = cell(nChunks, 1);
parfor t = 1:nChunks
    
    t_rmd = rem(t, nChunks_real) + 1;
    
    testSet_mat{t} = feature.Feature.unpackFeatureVectors(cTest_set(:,t_rmd));
    TrueLabels{t} = feature.Feature.unpackLabels(cTest_set(:,t_rmd))';
end


%% Step 1: Full Dataset
%ticID_full = tic;
%[fullLLH, fullAUC, fullModels] = genericCoresetTestUtil.fullDatasetAccuracyRun(nChunks, nChunks_real, cTrain_set, testSet_mat, TrueLabels);
%disp(['full dataset completed in ', int2str(toc(ticID_full)), ' seconds.']);

%% Step 2: Storages

% Divide runs into batches to circumvent the memory limitation.
assert(rem(nChunks_real, 2) == 0, 'nChunks_real has to be multiple of 2');
batchSize = nChunks_real / 2;
nBatches = nChunks / batchSize;



for iter = 1:nIterations
    
    % Clear the storages from previous iteration.
    coresetTree.clear();
    coresetSerial.clear();
    randTree.clear();
    randSerial.clear();
    
    ticID_iter = tic;
    
    % hack for parallel.
    ctLLH = zeros(1, nChunks);      ctAUC = zeros(1, nChunks);      ctModel = cell(1, nChunks);
    csLLH = zeros(1, nChunks);      csAUC = zeros(1, nChunks);      csModel = cell(1, nChunks);
    rtLLH = zeros(1, nChunks);      rtAUC = zeros(1, nChunks);      rtModel = cell(1, nChunks);
    rsLLH = zeros(1, nChunks);      rsAUC = zeros(1, nChunks);      rsModel = cell(1, nChunks);
    
    %
    Xct = cell(nChunks,1);    wct = cell(nChunks,1);
    Xcs = cell(nChunks,1);    wcs = cell(nChunks,1);
    Xrt = cell(nChunks,1);    wrt = cell(nChunks,1);
    Xrs = cell(nChunks,1);    wrs = cell(nChunks,1);
    
    % Use batch to circumvent the memory limiation.
    for ibatch = 1:nBatches
        
        ticID_batch = tic;
        
        % Only cover a small range of t.
        tlow = batchSize * (ibatch - 1) + 1;
        thigh = tlow + batchSize - 1;

        

        ticID_StorageTime = tic;


        disp(['===== Preparing cell data =====']);

        for t = tlow:thigh

            disp(['===== t = ' int2str(t), ' =====']);

            t_rmd = rem(t, nChunks_real) + 1;

            % Get training and testing sets.
            trainingSet_mat = Feature.unpackFeatureVectors(cTrain_set(:,t_rmd));

            % Store trainingSet_mat into 5 storages.
            [Xct{t}, wct{t}] = coresetTree.addChunkDataTree(trainingSet_mat);
            [Xcs{t}, wcs{t}] = coresetSerial.addChunkDataSerial(trainingSet_mat);
            [Xrt{t}, wrt{t}] = randTree.addChunkDataTree(trainingSet_mat);
            [Xrs{t}, wrs{t}] = randSerial.addChunkDataSerial(trainingSet_mat);
        end


        disp(['Storage time = ', int2str(toc(ticID_StorageTime)), ' seconds.']);

        parfor t = tlow:thigh

            ticID_chunk = tic;

            [ctLLH(1,t),   ctAUC(1,t), ctModel{t}] = AccuracyTestModule.basicAccuracyMetrics(Xct{t}, wct{t}, testSet_mat{t}, TrueLabels{t});
            [csLLH(1,t),   csAUC(1,t), csModel{t}] = AccuracyTestModule.basicAccuracyMetrics(Xcs{t}, wcs{t}, testSet_mat{t}, TrueLabels{t});
            [rtLLH(1,t),   rtAUC(1,t), rtModel{t}] = AccuracyTestModule.basicAccuracyMetrics(Xrt{t}, wrt{t}, testSet_mat{t}, TrueLabels{t});
            [rsLLH(1,t),   rsAUC(1,t), rsModel{t}] = AccuracyTestModule.basicAccuracyMetrics(Xrs{t}, wrs{t}, testSet_mat{t}, TrueLabels{t});
            
            % Display some info.
            disp(['===== t = ' int2str(t), ' =====']);
            
            disp(['Dataset Size: Coreset(Tree)=', int2str(size(Xct{t},2)), ...
                              ', Coreset(Serial)=', int2str(size(Xcs{t},2)), ...
                              ', Random Subset(Tree)=', int2str(size(Xcs{t},2)), ...
                              ', Random Subset(Serial)=', int2str(size(Xcs{t},2))]);
            
            disp(['==> t = ', int2str(t), ' completed in ', int2str(toc(ticID_chunk)), ' seconds.']);
        end
        
        
        disp(['====> batch = ', int2str(ibatch), ' completed in ', int2str(toc(ticID_batch)), ' seconds.']);
        
    end % for ibatch = 1:nBatches
    
    
    
    % Retrieve the results from parallel execution.
    allStats{1}.LLH(iter,:) = ctLLH;    allStats{1}.AUC(iter,:) = ctAUC;    allStats{1}.Models(iter,:) = ctModel;
    allStats{2}.LLH(iter,:) = csLLH;    allStats{2}.AUC(iter,:) = csAUC;    allStats{2}.Models(iter,:) = csModel;
    allStats{3}.LLH(iter,:) = rtLLH;    allStats{3}.AUC(iter,:) = rtAUC;    allStats{3}.Models(iter,:) = rtModel;
    allStats{4}.LLH(iter,:) = rsLLH;    allStats{4}.AUC(iter,:) = rsAUC;    allStats{4}.Models(iter,:) = rsModel;
    
    disp(['====> Iteration = ', int2str(iter), ' completed in ', int2str(toc(ticID_iter)), ' seconds.']);
    
    
    sendmail('daiwei89@gmail.com', 'Mail from MATLAB', ['Iteration ', int2str(iter), ' is done in ', int2str(toc(ticID_iter)), ' seconds.']);
    
end

matlabpool close;


%% Step 2: Compute stats.

for istat = 1 : nStorages
    
    % Compute LLH
    allStats{istat}.meanLLH = mean(allStats{istat}.LLH,1);
    allStats{istat}.stdDevLLH = std(allStats{istat}.LLH,0,1);
    allStats{istat}.stdErrLLH = allStats{istat}.stdDevLLH / sqrt(nIterations);

    % Compute AUC
    allStats{istat}.meanAUC = mean(allStats{istat}.AUC,1);
    allStats{istat}.stdDevAUC = std(allStats{istat}.AUC,0,1);
    allStats{istat}.stdErrAUC = allStats{istat}.stdDevAUC / sqrt(nIterations);
    
end

%% Step 3: Plotting

figure()
%hold all
%nChunks = 786;
coordX = linspace(1,nChunks,nChunks);

for istat = 1 : nStorages
    subplot(2,1,1);
    %errorbar(coordX, allStats{istat}.meanLLH, allStats{istat}.stdErrLLH, '--x', 'LineWidth', 2);
    plot(coordX, allStats{istat}.meanLLH);
    xlabel('hours')
    ylabel('LLH')
    hold all;
    
    subplot(2,1,2);
    %errorbar(coordX, allStats{istat}.meanAUC, allStats{istat}.stdErrAUC, '--x', 'LineWidth', 2);
    plot(coordX, allStats{istat}.meanAUC);
    xlabel('hours')
    ylabel('AUC')
    legend({'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)', 'Full Set'}, 'Location', 'Southeast')
    hold all;
end

    subplot(2,1,1);
    plot(coordX, fullLLH);
    xlabel('hours')
    ylabel('LLH')
    hold all;
    
    subplot(2,1,2);
    plot(coordX, fullAUC);
    xlabel('hours')
    ylabel('AUC')
    legend({'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)', 'Full Set'}, 'Location', 'Southeast')
    hold all;


%%
for istat = 1 : nStorages
    avgLLH = sum(allStats{istat}.meanLLH) / numel(allStats{istat}.meanLLH);
    avgAUC = sum(allStats{istat}.meanAUC) / numel(allStats{istat}.meanAUC);
    avgStdLLH = sum(allStats{istat}.stdDevLLH) / numel(allStats{istat}.stdDevLLH);
    avgStdAUC = sum(allStats{istat}.stdDevAUC) / numel(allStats{istat}.stdDevAUC);
    disp(['istat=', int2str(istat), ', avgLLH=', num2str(avgLLH), ', avgAUC=', num2str(avgAUC), ', avgStdLLH=', num2str(avgStdLLH), ', avgStdAUC=', num2str(avgStdAUC)]);
end
disp(['full dataset, avgLLH=', num2str(mean(fullLLH)), ', avgAUC=', num2str(mean(fullAUC))]);


%clearvars -except 'allStats' 'nChunks' 'nChunks_real' 'nStorages' 'fullLLH' 'fullAUC' 'fullModels', ;

%}