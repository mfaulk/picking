%% Step 0: Setups
import online.*
import feature.*
import parallel.*
import gmm.*

clear;
clc;


genericCoresetTestUtil.gmailSetup();


%load('experiments/coreset_vs_onlineEM/preprocessedMAT/7recordings_combined_262hrs');
load('experiments/coreset_vs_onlineEM/preprocessedMAT/rjc_10022010.mat');
nChunks = 5; % Keep it < 100, so that nChunks * 3 < 100
cTest_set = cTest_set(:,1:nChunks);
cTrain_set = cTrain_set(:,1:nChunks);

% repeat recordings by increasing nChunks.
nChunks_real = nChunks; 
nEpoch = 3;
nChunks = nEpoch * nChunks_real;  % run the same data multiple times.
nIterations = 3;


% Initialize 4 storages.
k = 6;
coresetParams.k = k;
coresetParams.beta = k*2;
coresetTree = coresetOnline(AbstractOnlineStorage.USE_TREE, coresetParams, 40000);
coresetSerial = coresetOnline(AbstractOnlineStorage.USE_SERIAL, coresetParams, 40000);
randTree = randSubsetOnline(AbstractOnlineStorage.USE_TREE, 40000);
randSerial = randSubsetOnline(AbstractOnlineStorage.USE_SERIAL, 40000);

% Stats: 
% 1=coresetTree(ct), 2=coresetSerial(cs), 3=randTree(rt), 4=randSerial(rs)
nStorages = 4; % number of storages.
allStats = cell(nStorages,1);

%openPool();
%parfor istat = 1 : nStorages
for istat = 1 : nStorages
    allStats{istat}.LLH = zeros(nIterations, nChunks);
    allStats{istat}.AUC = zeros(nIterations, nChunks);
    allStats{istat}.Models = cell(nIterations, nChunks);
    allStats{istat}.Time = zeros(nIterations, nChunks);
end


% Test set is the same for all iterations. Do it beforehand.
nTest_perHour = size(feature.Feature.unpackFeatureVectors(cTest_set(:,1)), 2); % Half of it is synthetic earthquake
Xtest_all = zeros(17, nChunks_real * nTest_perHour);
Ltest_all = zeros(1, nChunks_real * nTest_perHour); % test labels (row vector)
for t = 1:nChunks_real
    % Unpack test set and test labels
    istart_test = (t - 1) * nTest_perHour + 1;
    iend_test = istart_test + nTest_perHour - 1;
    Xtest_all(:,istart_test:iend_test) = feature.Feature.unpackFeatureVectors(cTest_set(:,t));
    Ltest_all(:,istart_test:iend_test) = feature.Feature.unpackLabels(cTest_set(:,t))';
end


%% Step 1: Full Dataset
%ticID_full = tic;
%[fullLLH, fullAUC, fullModels] = genericCoresetTestUtil.fullDatasetAccuracyRun(nChunks, nChunks_real, cTrain_set, testSet_mat, TrueLabels);
%disp(['full dataset completed in ', int2str(toc(ticID_full)), ' seconds.']);

%% Step 2: Storages

for iter = 1:nIterations
    
    % Clear the storages from previous iteration.
    coresetTree.clear();
    coresetSerial.clear();
    randTree.clear();
    randSerial.clear();
    
    ticID_iter = tic;
    
    % Will store only the dataset from the last iteration.
    Xct = cell(nChunks,1);    wct = cell(nChunks,1);
    Xcs = cell(nChunks,1);    wcs = cell(nChunks,1);
    Xrt = cell(nChunks,1);    wrt = cell(nChunks,1);
    Xrs = cell(nChunks,1);    wrs = cell(nChunks,1);

    % Record time.
    ctTime = zeros(1, nChunks);
    csTime = zeros(1, nChunks);
    rtTime = zeros(1, nChunks);
    rsTime = zeros(1, nChunks);

    % Prepare Data
    disp(['===== Preparing cell data =====']);
    ticID_StorageTime = tic;
    for t = 1:nChunks

        disp(['===== t = ' int2str(t), ' =====']);

        t_rmd = rem(t,nChunks_real);
        if t_rmd == 0   % Deal with boundary case.
            t_rmd = nChunks_real;
        end

        % Get training and testing sets.
        trainingSet_mat = Feature.unpackFeatureVectors(cTrain_set(:,t_rmd));

        % Store trainingSet_mat into 5 storages.
        [Xct{t}, wct{t}, ctTime(t)] = coresetTree.addChunkDataTree(trainingSet_mat);
        [Xcs{t}, wcs{t}, csTime(t)] = coresetSerial.addChunkDataSerial(trainingSet_mat);
        [Xrt{t}, wrt{t}, rtTime(t)] = randTree.addChunkDataTree(trainingSet_mat);
        [Xrs{t}, wrs{t}, rsTime(t)] = randSerial.addChunkDataSerial(trainingSet_mat);
    end
    disp(['Storage time = ', int2str(toc(ticID_StorageTime)), ' seconds.']);


    % hack for parallel.
    ctLLH = zeros(1, nChunks);      ctAUC = zeros(1, nChunks);      ctModel = cell(1, nChunks);
    csLLH = zeros(1, nChunks);      csAUC = zeros(1, nChunks);      csModel = cell(1, nChunks);
    rtLLH = zeros(1, nChunks);      rtAUC = zeros(1, nChunks);      rtModel = cell(1, nChunks);
    rsLLH = zeros(1, nChunks);      rsAUC = zeros(1, nChunks);      rsModel = cell(1, nChunks);
    
    % Computation heavy loop.
    %parfor t = 1:nChunks
    for t = 1:nChunks

        ticID_chunk = tic;
        
        % Produce test set on fly rather than beforehand for memory
        % efficiency.
        Xtest = genericCoresetTestUtil.repData(Xtest_all, nChunks_real, t, nTest_perHour);
        Ltest = genericCoresetTestUtil.repData(Ltest_all, nChunks_real, t, nTest_perHour);
        
        % Actual accuracy calculation
        [ctLLH(1,t), ctAUC(1,t), ctModel{t}, t1] = AccuracyTestModule.basicAccuracyMetrics(Xct{t}, wct{t}, Xtest, Ltest, 6);  
        [csLLH(1,t), csAUC(1,t), csModel{t}, t2] = AccuracyTestModule.basicAccuracyMetrics(Xcs{t}, wcs{t}, Xtest, Ltest, 6);
        [rtLLH(1,t), rtAUC(1,t), rtModel{t}, t3] = AccuracyTestModule.basicAccuracyMetrics(Xrt{t}, wrt{t}, Xtest, Ltest, 6);
        [rsLLH(1,t), rsAUC(1,t), rsModel{t}, t4] = AccuracyTestModule.basicAccuracyMetrics(Xrs{t}, wrs{t}, Xtest, Ltest, 6);

        % Add the gmm fitting time.
        ctTime(t) = ctTime(t) + t1;
        csTime(t) = csTime(t) + t2;
        rtTime(t) = rtTime(t) + t3;
        rsTime(t) = rsTime(t) + t4;
        
        % Display some info.
        disp(['==> t = ', int2str(t), ' completed in ', int2str(toc(ticID_chunk)), ' seconds.']);

        disp(['Dataset Size: Coreset(Tree)=', int2str(size(Xct{t},2)), ...
                          ', Coreset(Serial)=', int2str(size(Xcs{t},2)), ...
                          ', Random Subset(Tree)=', int2str(size(Xcs{t},2)), ...
                          ', Random Subset(Serial)=', int2str(size(Xcs{t},2))]);
    end
    
    % Retrieve the results from parallel execution.
    allStats{1}.LLH(iter,:) = ctLLH;    allStats{1}.AUC(iter,:) = ctAUC;    allStats{1}.Models(iter,:) = ctModel;   allStats{1}.Time(iter,:) = ctTime;
    allStats{2}.LLH(iter,:) = csLLH;    allStats{2}.AUC(iter,:) = csAUC;    allStats{2}.Models(iter,:) = csModel;   allStats{2}.Time(iter,:) = csTime;
    allStats{3}.LLH(iter,:) = rtLLH;    allStats{3}.AUC(iter,:) = rtAUC;    allStats{3}.Models(iter,:) = rtModel;   allStats{3}.Time(iter,:) = rtTime;
    allStats{4}.LLH(iter,:) = rsLLH;    allStats{4}.AUC(iter,:) = rsAUC;    allStats{4}.Models(iter,:) = rsModel;   allStats{4}.Time(iter,:) = rsTime;
    
    msg = ['====> Iteration = ', int2str(iter), ' completed in ', int2str(toc(ticID_iter)), ' seconds.'];
    disp(msg);
    sendmail('daiwei89@gmail.com', 'Mail from MATLAB', msg);
    
end

matlabpool close;


%% Step 2: Compute All Stats.

for istat = 1 : nStorages
    
    % Compute LLH
    allStats{istat}.meanLLH = mean(allStats{istat}.LLH,1);
    allStats{istat}.stdDevLLH = std(allStats{istat}.LLH,0,1);
    allStats{istat}.stdErrLLH = allStats{istat}.stdDevLLH / sqrt(nIterations);

    % Compute AUC
    allStats{istat}.meanAUC = mean(allStats{istat}.AUC,1);
    allStats{istat}.stdDevAUC = std(allStats{istat}.AUC,0,1);
    allStats{istat}.stdErrAUC = allStats{istat}.stdDevAUC / sqrt(nIterations);
    
    % Compute Time
    allStats{istat}.meanTime = mean(allStats{istat}.Time,1);
    allStats{istat}.stdDevTime = std(allStats{istat}.Time,0,1);
    allStats{istat}.stdErrTime = allStats{istat}.stdDevTime / sqrt(nIterations);
    
    % Compute cumulative mean LLH and AUC and Time
    allStats{istat}.cumMeanLLH = zeros(1, nChunks);
    allStats{istat}.cumMeanAUC = zeros(1, nChunks);
    allStats{istat}.cumMeanTime = zeros(1, nChunks);
    % The first one is trivial
    allStats{istat}.cumMeanLLH(1) = allStats{istat}.meanLLH(1);
    allStats{istat}.cumMeanAUC(1) = allStats{istat}.meanAUC(1);
    allStats{istat}.cumMeanTime(1) = allStats{istat}.meanTime(1);
    for t = 2:nChunks
        allStats{istat}.cumMeanLLH(t) = (allStats{istat}.cumMeanLLH(t-1) * (t-1) + allStats{istat}.meanLLH(t)) / t;
        allStats{istat}.cumMeanAUC(t) = (allStats{istat}.cumMeanAUC(t-1) * (t-1) + allStats{istat}.meanAUC(t)) / t;
        allStats{istat}.cumMeanTime(t) = (allStats{istat}.cumMeanTime(t-1) * (t-1) + allStats{istat}.meanTime(t)) / t;
    end
    
    % Compute cumulative mean LLH and AUC by EPOCH
    LLH_cum_epoch = zeros(nEpoch, nChunks_real);
    AUC_cum_epoch = zeros(nEpoch, nChunks_real);
    Time_cum_epoch = zeros(nEpoch, nChunks_real);

    for iepoch = 1:nEpoch
        t_start = (iepoch - 1) * nChunks_real + 1;
        t_end = t_start + nChunks_real - 1;
        LLH_cum_epoch(iepoch, 1) = allStats{istat}.meanLLH(t_start);
        AUC_cum_epoch(iepoch, 1) = allStats{istat}.meanAUC(t_start);
        Time_cum_epoch(iepoch, 1) = allStats{istat}.meanTime(t_start);
        for t = t_start + 1 : t_end
            %disp(int2str(t));
            t_1 = rem(t-1, nChunks_real);
            t_0 = rem(t, nChunks_real);
            if t_0 == 0, t_0 = nChunks_real; end
            if t_1 == 0, error('something strange in stat calculation'); end
            LLH_cum_epoch(iepoch, t_0) = (LLH_cum_epoch(iepoch, t_1) * t_1 + allStats{istat}.meanLLH(t)) / t_0;
            AUC_cum_epoch(iepoch, t_0) = (AUC_cum_epoch(iepoch, t_1) * t_1 + allStats{istat}.meanAUC(t)) / t_0;
            Time_cum_epoch(iepoch, t_0) = (Time_cum_epoch(iepoch, t_1) * t_1 + allStats{istat}.meanTime(t)) / t_0;
        end
    end
    
    allStats{istat}.cumMeanLLH_epoch = LLH_cum_epoch;
    allStats{istat}.cumMeanAUC_epoch = AUC_cum_epoch;
    allStats{istat}.cumMeanTime_epoch = Time_cum_epoch;
end


%% Step 3: Plot Cumulative meanLLH and meanAUC.

% You are only allow to set 3 colors. Come on, Matlab, that's it?
set(0,'DefaultAxesLineStyleOrder',{'-o',':s','--+','-.d'})
set(0,'DefaultAxesColorOrder',[0 0 1; 0 1 0; 1 0 0])

figure()
coordX = linspace(1,nChunks,nChunks);


for istat = 1 : nStorages
    subplot(2,1,1);
    %errorbar(coordX, allStats{istat}.cumMeanLLH, allStats{istat}.stdErrLLH, 'LineWidth', 1);
    plot(coordX, allStats{istat}.cumMeanLLH);
    xlabel('hours')
    ylabel('cumulative average LLH')
    title('Four storages');
    hold all;
    
    subplot(2,1,2);
    %errorbar(coordX, allStats{istat}.cumMeanAUC, allStats{istat}.stdErrAUC, 'LineWidth', 1);
    plot(coordX, allStats{istat}.cumMeanAUC);
    xlabel('hours')
    ylabel('cumulative average AUC')
    title('Four storages');
    hold all;
end
legend({'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)'}, 'Location', 'Southeast')

hold off;

%% Step 4: Plot Cumulative meanLLH by EPOCH
figure()
coordX = linspace(1,nChunks_real, nChunks_real);
names = {'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)'};
for istat = 1 : nStorages
    subplot(2,2,istat);
    
    for iepoch = 1:nEpoch
        %errorbar(coordX, allStats{istat}.meanLLH, allStats{istat}.stdErrLLH, '--x', 'LineWidth', 2);
        plot(coordX, allStats{istat}.cumMeanLLH_epoch(iepoch,:));
        xlabel('hours')
        ylabel('cumulative average LLH')
        hold all;
    end
    title(names{istat})
end
legend({'epoch 1', 'epoch 2', 'epoch 3'}, 'Location', 'Southeast');

%% Step 5: Plot Cumulative meanAUC by EPOCH

figure()
coordX = linspace(1,nChunks_real, nChunks_real);
names = {'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)'};
for istat = 1 : nStorages
    subplot(2,2,istat);
    
    for iepoch = 1:nEpoch
        %errorbar(coordX, allStats{istat}.meanLLH, allStats{istat}.stdErrLLH, '--x', 'LineWidth', 2);
        plot(coordX, allStats{istat}.cumMeanAUC_epoch(iepoch,:));
        xlabel('hours')
        ylabel('cumulative average AUC')
        hold all;
    end
    title(names{istat})
end
legend({'epoch 1', 'epoch 2', 'epoch 3'}, 'Location', 'Southeast');

%% Plot Time statistics.

figure();
for istat = 1 : nStorages
    subplot(2,4,[1 4])
    coordX = linspace(1,nChunks, nChunks);
    %errorbar(coordX, allStats{istat}.cumMeanTime, allStats{istat}.stdErrTime, '--.', 'LineWidth', 1);
    plot(coordX, allStats{istat}.cumMeanTime);
    xlabel('hours')
    ylabel('cumulative average Time (sec)')
    legend(names, 'Location', 'Southeast')
    title('All storages');
    hold all;
    
    subplot(2,4,istat + 4);
    coordX = linspace(1,nChunks_real, nChunks_real);
    for iepoch = 1:nEpoch
        %errorbar(coordX, allStats{istat}.meanLLH,
        %allStats{istat}.stdErrLLH, '--x', 'LineWidth', 2);
        plot(coordX, allStats{istat}.cumMeanTime_epoch(iepoch,:));
        xlabel('hours')
        ylabel('cumulative average Time (sec)')
        hold all;
    end
    title(names{istat})
end
legend({'epoch 1', 'epoch 2', 'epoch 3'}, 'Location', 'SoutheastOutside');



%%
%{
%% Step 3: Plot meanLLH.

figure()
coordX = linspace(1,nChunks,nChunks);

for istat = 1 : nStorages
    subplot(2,1,1);
    %errorbar(coordX, allStats{istat}.meanLLH, allStats{istat}.stdErrLLH, '--x', 'LineWidth', 2);
    plot(coordX, allStats{istat}.meanLLH);
    xlabel('hours')
    ylabel('LLH')
    hold all;
    
    subplot(2,1,2);
    %errorbar(coordX, allStats{istat}.meanAUC, allStats{istat}.stdErrAUC, '--x', 'LineWidth', 2);
    plot(coordX, allStats{istat}.meanAUC);
    xlabel('hours')
    ylabel('AUC')
    legend({'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)', 'Full Set'}, 'Location', 'Southeast')
    hold all;
end

    subplot(2,1,1);
    plot(coordX, fullLLH);
    xlabel('hours')
    ylabel('LLH')
    hold all;
    
    subplot(2,1,2);
    plot(coordX, fullAUC);
    xlabel('hours')
    ylabel('AUC')
    legend({'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)', 'Full Set'}, 'Location', 'Southeast')
    hold all;


%%
for istat = 1 : nStorages
    avgLLH = sum(allStats{istat}.meanLLH) / numel(allStats{istat}.meanLLH);
    avgAUC = sum(allStats{istat}.meanAUC) / numel(allStats{istat}.meanAUC);
    avgStdLLH = sum(allStats{istat}.stdDevLLH) / numel(allStats{istat}.stdDevLLH);
    avgStdAUC = sum(allStats{istat}.stdDevAUC) / numel(allStats{istat}.stdDevAUC);
    disp(['istat=', int2str(istat), ', avgLLH=', num2str(avgLLH), ', avgAUC=', num2str(avgAUC), ', avgStdLLH=', num2str(avgStdLLH), ', avgStdAUC=', num2str(avgStdAUC)]);
end
disp(['full dataset, avgLLH=', num2str(mean(fullLLH)), ', avgAUC=', num2str(mean(fullAUC))]);


%clearvars -except 'allStats' 'nChunks' 'nChunks_real' 'nStorages' 'fullLLH' 'fullAUC' 'fullModels', ;
%}
