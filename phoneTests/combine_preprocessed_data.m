
clear;
clc;

% Ge the .mat file names.
list = dir('experiments/coreset_vs_onlineEM/processed_recordings');
nfiles = length(list) - 2; % -2 because . and .. are included

cTest_set_temp = [];
cTrain_set_temp = [];
nChunks_temp = 0;
for i = 1:nfiles
    %clearvars -except 'nfiles' 'list' 'i';
    load(['experiments/coreset_vs_onlineEM/processed_recordings/', list(i+2).name]);
    cTest_set_temp = [cTest_set_temp, cTest_set];
    cTrain_set_temp = [cTrain_set_temp, cTrain_set];
    nChunks_temp = nChunks_temp + nChunks;
    
    disp(['Added ', int2str(nChunks), ' hours from ', list(i+2).name, '. Now we have ', int2str(size(cTest_set_temp, 2)), ' hours.']);
end

cTest_set = cTest_set_temp;
cTrain_set = cTrain_set_temp;
nChunks = nChunks_temp;

clearvars -except 'cTest_set' 'cTrain_set' 'nChunks'