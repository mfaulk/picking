randTrain = reshape(cTrain_set,1,size(cTrain_set,1)*size(cTrain_set,2));
randTrain = randTrain(randperm(length(randTrain)));
randTrain = reshape(randTrain,size(cTrain_set,1),size(cTrain_set,2));

randTest = reshape(cTest_set,1,size(cTest_set,1)*size(cTest_set,2));
randTest = randTest(randperm(length(randTest)));
randTest = reshape(randTest,size(cTest_set,1),size(cTest_set,2));
