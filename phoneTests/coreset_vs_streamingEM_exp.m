%{

Descriptions:
    This script do the actual experiments to see how streaming EM does

%}

%% Setups

close all;


if ~exist('cTrain_set','var') || ~exist('cTest_set','var')
    load('experiments/coreset_vs_onlineEM/matFiles/mat_combined');
    %error('Need cTrain_set and cTest_set');
end


%% Experiments on cTrain_set and cTest_set

import feature.*;
import storage.*;
import gmm.*;

%%%%%%%%%%%%%%%%%%%%%%%% Variable to be changed %%%%%%%%%%%%%%%%%%%
coreParams.coresetSize = 1000;   % small compared with chunksize
coreParams.k = 6;  % 6 Gaussians
coreParams.beta = 2 * coreParams.k;

%CTS = cumulativeTreeStorage(coreParams, nChunks);
GDS = GmmDataStorage(100, 0);


AUC = zeros((nChunks - 1), 1);
cTP = cell((nChunks - 1), 1);
cFP = cell((nChunks - 1), 1);

h = waitbar(0,'Initializing waitbar...');

alpha = .6;

for t = 1 : (nChunks - 1) % The last chunk doesn't have a test set.
    
    
    waitbar(t/(nChunks - 1),h,'Main loop...')
    
    trainingSet_mat = Feature.unpackFeatureVectors(cTrain_set(:,t));
    testSet_mat = Feature.unpackFeatureVectors(cTest_set(:,t+1));
    


    if t == 1
        % Initialize the model with a normal gmm
        k=1;
        nGaussians = coreParams.k;
        nRestart = 10;
        gm = gmm.Gmm.fitGmm(trainingSet_mat, nGaussians, nRestart);
        params.gm = gm;
    else
       % Make a weighted step
       nGaussians = coreParams.k;
       nk = (k+3)^(-alpha);
       model.mu = gm.mu;
       model.Sigma = gm.Sigma;
       model.weight = gm.weight';
       gm = gmm.Gmm.gmmWeightedStep(trainingSet_mat, nGaussians, nk, model);
       k=k+1;
       
    end
    % evaluate training data
    P = gm.evaluateProbability(testSet_mat);

    TrueLabels = Feature.unpackLabels(cTest_set(:,t+1));

    Assignments = -log(P);

    % a low hack to deal with the infinite!
    infIndices = (Assignments == inf);
    Assignments(infIndices) = 1000;
    nThresh = 1000;

    Thresholds = linspace(min(Assignments), max(Assignments), nThresh);

    import roc.*;
    [tp, fp] =  myROC( TrueLabels, Assignments, Thresholds );
    areaUnderCurve = auc(fp,tp);
    %disp(num2str(areaUnderCurve))
    
    AUC(t) = areaUnderCurve;
    cTP{t} = tp;
    cFP{t} = fp;
    %models{i} = gm;
    %cThresholds{i} = Thresholds;
    %cParams{i} = params; % save C, mu, sigma
    
    plot(cFP{t}, cTP{t});
    hold on;
    
end
xlabel('FPR');
ylabel('TPR');
close(h);

%% Plot TPR vs FPR (ROC curve) for each nChunk
%{
for t = 1 : (nChunks - 1)
    plot(cFP{t}, cTP{t});
    hold on;
end

xlabel('FPR');
ylabel('TPR');
%}