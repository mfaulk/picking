%% Step 0: Setups
import online.*
import feature.*
import parallel.*
import gmm.*

clear;
clc;


genericCoresetTestUtil.gmailSetup();


load('experiments/coreset_vs_onlineEM/preprocessedMAT/7recordings_combined_262hrs');
%load('experiments/coreset_vs_onlineEM/preprocessedMAT/rjc_10022010.mat');
%cTest_set = cTest_set(:,1:8);
%cTrain_set = cTrain_set(:,1:8);
%nChunks = 8;

% repeat recordings by increasing nChunks.
nChunks_real = nChunks; 
%nChunks = 3 * nChunks_real;  % run the same data multiple times.


k = 6;
coresetParams.k = k;
coresetParams.beta = k*2;
nIterations = 2;


% Initialize 4 storages.
coresetTree = coresetOnline(AbstractOnlineStorage.USE_TREE, coresetParams, 80000);
coresetSerial = coresetOnline(AbstractOnlineStorage.USE_SERIAL, coresetParams, 80000);
randTree = randSubsetOnline(AbstractOnlineStorage.USE_TREE, 80000);
randSerial = randSubsetOnline(AbstractOnlineStorage.USE_SERIAL, 80000);

% Stats: 
% 1=coresetTree(ct), 2=coresetSerial(cs), 3=randTree(rt), 4=randSerial(rs)
nStorages = 4; % number of storages.
allStats = cell(nStorages,1);

openPool();
parfor istat = 1 : nStorages
    allStats{istat}.LLH = zeros(nIterations, nChunks);
    allStats{istat}.AUC = zeros(nIterations, nChunks);
    allStats{istat}.Models = cell(nIterations, nChunks);
end


% Test set is the same for all iterations. Do it beforehand.
testSet_mat = cell(nChunks, 1);
TrueLabels = cell(nChunks, 1);
parfor t = 1:nChunks
    
    t_rmd = rem(t, nChunks_real) + 1;
    
    testSet_mat{t} = feature.Feature.unpackFeatureVectors(cTest_set(:,t_rmd));
    TrueLabels{t} = feature.Feature.unpackLabels(cTest_set(:,t_rmd))';
end


%% Step 1: Full Dataset
ticID_full = tic;
[fullLLH, fullAUC, fullModels] = genericCoresetTestUtil.fullDatasetAccuracyRun(nChunks, nChunks_real, cTrain_set, testSet_mat, TrueLabels);
disp(['full dataset completed in ', int2str(toc(ticID_full)), ' seconds.']);


%% Step 1a: Full Dataset with Coreset Compression
[fullcoreLLH, fullcoreAUC, fullcoreModels] = genericCoresetTestUtil.fullCoresetAccuracyRun(nChunks, nChunks_real, cTrain_set, testSet_mat, TrueLabels);


%% Step 2: Storages

% Divide runs into batches to circumvent the memory limitation.
assert(rem(nChunks_real, 2) == 0, 'nChunks_real has to be multiple of 2');
batchSize = nChunks_real / 2;
nBatches = nChunks / batchSize;
 
for iter = 1:nIterations
    
    % Clear the storages from previous iteration.
    coresetTree.clear();
    coresetSerial.clear();
    randTree.clear();
    randSerial.clear();
    
    ticID_iter = tic;
    
    % hack for parallel.
    ctLLH = zeros(1, nChunks);      ctAUC = zeros(1, nChunks);      ctModel = cell(1, nChunks);
    csLLH = zeros(1, nChunks);      csAUC = zeros(1, nChunks);      csModel = cell(1, nChunks);
    rtLLH = zeros(1, nChunks);      rtAUC = zeros(1, nChunks);      rtModel = cell(1, nChunks);
    rsLLH = zeros(1, nChunks);      rsAUC = zeros(1, nChunks);      rsModel = cell(1, nChunks);
    
    
    % Use batch to circumvent the memory limiation.
    for ibatch = 1:nBatches
        
        ticID_batch = tic;
        
        % Only cover a small range of t.
        tlow = batchSize * (ibatch - 1) + 1;
        thigh = tlow + batchSize - 1;

        % We have to clear matrices from last batch to conserve memory.
        clearvars 'Xct' 'Xcs' 'Xrt' 'Xrs' 'wct' 'wcs' 'wrt' 'wrs'
        
        % Prepare cells of training data for parfor.
        Xct = cell(nChunks,1);    wct = cell(nChunks,1);
        Xcs = cell(nChunks,1);    wcs = cell(nChunks,1);
        Xrt = cell(nChunks,1);    wrt = cell(nChunks,1);
        Xrs = cell(nChunks,1);    wrs = cell(nChunks,1);

        ticID_StorageTime = tic;


        disp(['===== Preparing cell data =====']);

        for t = tlow:thigh

            disp(['===== t = ' int2str(t), ' =====']);

            t_rmd = rem(t, nChunks_real) + 1;

            % Get training and testing sets.
            trainingSet_mat = Feature.unpackFeatureVectors(cTrain_set(:,t_rmd));

            % Store trainingSet_mat into 5 storages.
            [Xct{t}, wct{t}] = coresetTree.addChunkDataTree(trainingSet_mat);
            [Xcs{t}, wcs{t}] = coresetSerial.addChunkDataSerial(trainingSet_mat);
            [Xrt{t}, wrt{t}] = randTree.addChunkDataTree(trainingSet_mat);
            [Xrs{t}, wrs{t}] = randSerial.addChunkDataSerial(trainingSet_mat);
        end


        disp(['Storage time = ', int2str(toc(ticID_StorageTime)), ' seconds.']);

        parfor t = tlow:thigh

            ticID_chunk = tic;

            [ctLLH(1,t),   ctAUC(1,t), ctModel{t}] = AccuracyTestModule.basicAccuracyMetrics(Xct{t}, wct{t}, testSet_mat{t}, TrueLabels{t});
            [csLLH(1,t),   csAUC(1,t), csModel{t}] = AccuracyTestModule.basicAccuracyMetrics(Xcs{t}, wcs{t}, testSet_mat{t}, TrueLabels{t});
            [rtLLH(1,t),   rtAUC(1,t), rtModel{t}] = AccuracyTestModule.basicAccuracyMetrics(Xrt{t}, wrt{t}, testSet_mat{t}, TrueLabels{t});
            [rsLLH(1,t),   rsAUC(1,t), rsModel{t}] = AccuracyTestModule.basicAccuracyMetrics(Xrs{t}, wrs{t}, testSet_mat{t}, TrueLabels{t});
            
            % Display some info.
            disp(['===== t = ' int2str(t), ' =====']);
            
            disp(['Dataset Size: Coreset(Tree)=', int2str(size(Xct{t},2)), ...
                              ', Coreset(Serial)=', int2str(size(Xcs{t},2)), ...
                              ', Random Subset(Tree)=', int2str(size(Xcs{t},2)), ...
                              ', Random Subset(Serial)=', int2str(size(Xcs{t},2))]);
            
            disp(['==> t = ', int2str(t), ' completed in ', int2str(toc(ticID_chunk)), ' seconds.']);
        end
        
        
        disp(['====> batch = ', int2str(ibatch), ' completed in ', int2str(toc(ticID_batch)), ' seconds.']);
        
    end % for ibatch = 1:nBatches
    
    
    
    % Retrieve the results from parallel execution.
    allStats{1}.LLH(iter,:) = ctLLH;    allStats{1}.AUC(iter,:) = ctAUC;    allStats{1}.Models(iter,:) = ctModel;
    allStats{2}.LLH(iter,:) = csLLH;    allStats{2}.AUC(iter,:) = csAUC;    allStats{2}.Models(iter,:) = csModel;
    allStats{3}.LLH(iter,:) = rtLLH;    allStats{3}.AUC(iter,:) = rtAUC;    allStats{3}.Models(iter,:) = rtModel;
    allStats{4}.LLH(iter,:) = rsLLH;    allStats{4}.AUC(iter,:) = rsAUC;    allStats{4}.Models(iter,:) = rsModel;
    
    disp(['====> Iteration = ', int2str(iter), ' completed in ', int2str(toc(ticID_iter)), ' seconds.']);
    
    
    sendmail('daiwei89@gmail.com', 'Mail from MATLAB', ['Iteration ', int2str(iter), ' is done in ', int2str(toc(ticID_iter)), ' seconds.']);
    
end

matlabpool close;


%% Step 2: Compute stats.

for istat = 1 : nStorages
    
    % Compute LLH
    allStats{istat}.meanLLH = mean(allStats{istat}.LLH,1);
    allStats{istat}.stdDevLLH = std(allStats{istat}.LLH,0,1);
    allStats{istat}.stdErrLLH = allStats{istat}.stdDevLLH / sqrt(nIterations);

    % Compute AUC
    allStats{istat}.meanAUC = mean(allStats{istat}.AUC,1);
    allStats{istat}.stdDevAUC = std(allStats{istat}.AUC,0,1);
    allStats{istat}.stdErrAUC = allStats{istat}.stdDevAUC / sqrt(nIterations);
    
end

%% Step 3: Plotting

figure()
%hold all
%nChunks = 786;
coordX = linspace(1,nChunks,nChunks);

subplot(2,1,1);
plot(coordX,fullcoreLLH);
xlabel('hours')
ylabel('LLH')
hold all;
    
subplot(2,1,2);
plot(coordX, fullcoreAUC);
xlabel('hours')
ylabel('AUC')
hold all;

for istat = 1 : nStorages
    subplot(2,1,1);
    %errorbar(coordX, allStats{istat}.meanLLH, allStats{istat}.stdErrLLH, '--x', 'LineWidth', 2);
    plot(coordX, allStats{istat}.meanLLH);
    xlabel('hours')
    ylabel('LLH')
    hold all;
    
    subplot(2,1,2);
    %errorbar(coordX, allStats{istat}.meanAUC, allStats{istat}.stdErrAUC, '--x', 'LineWidth', 2);
    plot(coordX, allStats{istat}.meanAUC);
    xlabel('hours')
    ylabel('AUC')
    legend({'Coreset(full)','Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)', 'Full Set'}, 'Location', 'Southeast')
    hold all;
end

%{
    subplot(2,1,1);
    plot(coordX, fullLLH);
    xlabel('hours')
    ylabel('LLH')
    hold all;
    
    subplot(2,1,2);
    plot(coordX, fullAUC);
    xlabel('hours')
    ylabel('AUC')
    legend({'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)', 'Full Set'}, 'Location', 'Southeast')
    hold all;

%}
%%
statMat = zeros(10, nStorages);
for istat = 1 : nStorages
    
    for period = 1:3
        start_point = (period - 1) * nChunks_real + 1;
        end_point = start_point + nChunks_real - 1;
        avgLLH = sum(allStats{istat}.meanLLH(1,start_point:end_point)) / numel(allStats{istat}.meanLLH(1,start_point:end_point));
        avgAUC = sum(allStats{istat}.meanAUC(1,start_point:end_point)) / numel(allStats{istat}.meanAUC(1,start_point:end_point));
        avgStdLLH = sum(allStats{istat}.stdDevLLH(1,start_point:end_point)) / numel(allStats{istat}.stdDevLLH(1,start_point:end_point));
        avgStdAUC = sum(allStats{istat}.stdDevAUC(1,start_point:end_point)) / numel(allStats{istat}.stdDevAUC(1,start_point:end_point));
        disp(['period = ', int2str(period), ' istat=', int2str(istat), ', avgLLH=', num2str(avgLLH), ', avgAUC=', num2str(avgAUC), ', avgStdLLH=', num2str(avgStdLLH), ', avgStdAUC=', num2str(avgStdAUC)]);
        statMat(period,istat) = avgLLH;
        statMat(period+5,istat) = avgAUC;
    end
    
    avgLLH = sum(allStats{istat}.meanLLH) / numel(allStats{istat}.meanLLH);
    avgAUC = sum(allStats{istat}.meanAUC) / numel(allStats{istat}.meanAUC);
    avgStdLLH = sum(allStats{istat}.stdDevLLH) / numel(allStats{istat}.stdDevLLH);
    avgStdAUC = sum(allStats{istat}.stdDevAUC) / numel(allStats{istat}.stdDevAUC);
    disp(['==> TOTAL: istat=', int2str(istat), ', avgLLH=', num2str(avgLLH), ', avgAUC=', num2str(avgAUC), ', avgStdLLH=', num2str(avgStdLLH), ', avgStdAUC=', num2str(avgStdAUC)]);
    statMat(4,istat) = avgLLH;
    statMat(5,istat) = avgStdLLH;
    statMat(9,istat) = avgAUC;
    statMat(10,istat) = avgStdAUC;
end
%disp(['full dataset, avgLLH=', num2str(fullLLH), ', avgAUC=', num2str(fullAUC)]);

dlmwrite('experiments/coreset_vs_onlineEM/result_Stats/run80000.csv',statMat,'delimiter',',');

%clearvars -except 'allStats' 'nChunks' 'nChunks_real' 'nStorages' 'fullLLH' 'fullAUC' 'fullModels';

