%% Step 0: Setups
import online.*
import feature.*
import parallel.*
import gmm.*

%clear;
%clc;


genericCoresetTestUtil.gmailSetup();


%load('experiments/coreset_vs_onlineEM/preprocessedMAT/7recordings_combined_262hrs');
load('experiments/coreset_vs_onlineEM/preprocessedMAT/rjc_10022010.mat');
nChunks = 3; % Keep it < 100, so that nChunks * 3 < 100
cTest_set = cTest_set(:,1:nChunks);
cTrain_set = cTrain_set(:,1:nChunks);

% repeat recordings by increasing nChunks.
nChunks_real = nChunks; 
nEpoch = 3;
nChunks = nEpoch * nChunks_real;  % run the same data multiple times.
nIterations = 3;

nStorages = 1;

% Stats: 
% 1=coresetTree(ct), 2=coresetSerial(cs), 3=randTree(rt), 4=randSerial(rs)
nStorages = 4; % number of storages.
stepStats = cell(nStorages,1);

for istat = 1 : nStorages
    stepStats{istat}.LLH = zeros(nIterations, nChunks);
    stepStats{istat}.AUC = zeros(nIterations, nChunks);
    stepStats{istat}.Models = cell(nIterations, nChunks);
    stepStats{istat}.Time = zeros(nIterations, nChunks);
end


% Test set is the same for all iterations. Do it beforehand.
nTest_perHour = size(feature.Feature.unpackFeatureVectors(cTest_set(:,1)), 2); % Half of it is synthetic earthquake
Xtest_all = zeros(17, nChunks_real * nTest_perHour);
Ltest_all = zeros(1, nChunks_real * nTest_perHour); % test labels (row vector)
for t = 1:nChunks_real
    % Unpack test set and test labels
    istart_test = (t - 1) * nTest_perHour + 1;
    iend_test = istart_test + nTest_perHour - 1;
    Xtest_all(:,istart_test:iend_test) = feature.Feature.unpackFeatureVectors(cTest_set(:,t));
    Ltest_all(:,istart_test:iend_test) = feature.Feature.unpackLabels(cTest_set(:,t))';
end


%% Step 1: Full Dataset
%ticID_full = tic;
%[fullLLH, fullAUC, fullModels] = genericCoresetTestUtil.fullDatasetAccuracyRun(nChunks, nChunks_real, cTrain_set, testSet_mat, TrueLabels);
%disp(['full dataset completed in ', int2str(toc(ticID_full)), ' seconds.']);

%% Step 2: Storages

for iter = 1:nIterations
    
    ticID_iter = tic;

    stepEM = AccuracyStepTestModule();
    
    stepLLH = zeros(1, nChunks);      stepAUC = zeros(1, nChunks);      stepModel = cell(1, nChunks);   stepTime = zeros(1, nChunks);
    
    % Computation heavy loop.
    for t = 1:nChunks

        ticID_chunk = tic;
        
        t_rmd = rem(t, nChunks_real);
        if t_rmd == 0, t_rmd = nChunks_real; end
        
        % Get training sets.
        Xtrain = Feature.unpackFeatureVectors(cTrain_set(:,t_rmd));
        Wtrain = ones(1, size(Xtrain,2));
        
        % Produce test set on fly rather than beforehand for memory
        % efficiency.
        Xtest = genericCoresetTestUtil.repData(Xtest_all, nChunks_real, t, nTest_perHour);
        Ltest = genericCoresetTestUtil.repData(Ltest_all, nChunks_real, t, nTest_perHour);
        
        % Actual accuracy calculation
        [stepLLH(1,t), stepAUC(1,t), stepModel{t}, stepTime(1,t)] = stepEM.stepAccuracyMetrics(Xtrain, Wtrain, Xtest, Ltest, 6);
        
        % Display some info.
        disp(['==> t = ', int2str(t), ' completed in ', int2str(toc(ticID_chunk)), ' seconds.']);
    end
    
    % Retrieve the results from parallel execution.
    stepStats{1}.LLH(iter,:) = stepLLH;    stepStats{1}.AUC(iter,:) = stepAUC;    stepStats{1}.Models(iter,:) = stepModel;   stepStats{1}.Time(iter,:) = stepTime;
    
    msg = ['====> Iteration = ', int2str(iter), ' completed in ', int2str(toc(ticID_iter)), ' seconds.'];
    disp(msg);
    sendmail('daiwei89@gmail.com', 'Mail from MATLAB', msg);
    
end


%% Step 2: Compute All Stats.

for istat = 1 : nStorages
    
    % Compute LLH
    stepStats{istat}.meanLLH = mean(stepStats{istat}.LLH,1);
    stepStats{istat}.stdDevLLH = std(stepStats{istat}.LLH,0,1);
    stepStats{istat}.stdErrLLH = stepStats{istat}.stdDevLLH / sqrt(nIterations);

    % Compute AUC
    stepStats{istat}.meanAUC = mean(stepStats{istat}.AUC,1);
    stepStats{istat}.stdDevAUC = std(stepStats{istat}.AUC,0,1);
    stepStats{istat}.stdErrAUC = stepStats{istat}.stdDevAUC / sqrt(nIterations);
    
    % Compute Time
    stepStats{istat}.meanTime = mean(stepStats{istat}.Time,1);
    stepStats{istat}.stdDevTime = std(stepStats{istat}.Time,0,1);
    stepStats{istat}.stdErrTime = stepStats{istat}.stdDevTime / sqrt(nIterations);
    
    % Compute cumulative mean LLH and AUC and Time
    stepStats{istat}.cumMeanLLH = zeros(1, nChunks);
    stepStats{istat}.cumMeanAUC = zeros(1, nChunks);
    stepStats{istat}.cumMeanTime = zeros(1, nChunks);
    % The first one is trivial
    stepStats{istat}.cumMeanLLH(1) = stepStats{istat}.meanLLH(1);
    stepStats{istat}.cumMeanAUC(1) = stepStats{istat}.meanAUC(1);
    stepStats{istat}.cumMeanTime(1) = stepStats{istat}.meanTime(1);
    for t = 2:nChunks
        stepStats{istat}.cumMeanLLH(t) = (stepStats{istat}.cumMeanLLH(t-1) * (t-1) + stepStats{istat}.meanLLH(t)) / t;
        stepStats{istat}.cumMeanAUC(t) = (stepStats{istat}.cumMeanAUC(t-1) * (t-1) + stepStats{istat}.meanAUC(t)) / t;
        stepStats{istat}.cumMeanTime(t) = (stepStats{istat}.cumMeanTime(t-1) * (t-1) + stepStats{istat}.meanTime(t)) / t;
    end
    
    % Compute cumulative mean LLH and AUC by EPOCH
    LLH_cum_epoch = zeros(nEpoch, nChunks_real);
    AUC_cum_epoch = zeros(nEpoch, nChunks_real);
    Time_cum_epoch = zeros(nEpoch, nChunks_real);

    for iepoch = 1:nEpoch
        t_start = (iepoch - 1) * nChunks_real + 1;
        t_end = t_start + nChunks_real - 1;
        LLH_cum_epoch(iepoch, 1) = stepStats{istat}.meanLLH(t_start);
        AUC_cum_epoch(iepoch, 1) = stepStats{istat}.meanAUC(t_start);
        Time_cum_epoch(iepoch, 1) = stepStats{istat}.meanTime(t_start);
        for t = t_start + 1 : t_end
            %disp(int2str(t));
            t_1 = rem(t-1, nChunks_real);
            t_0 = rem(t, nChunks_real);
            if t_0 == 0, t_0 = nChunks_real; end
            if t_1 == 0, error('something strange in stat calculation'); end
            LLH_cum_epoch(iepoch, t_0) = (LLH_cum_epoch(iepoch, t_1) * t_1 + stepStats{istat}.meanLLH(t)) / t_0;
            AUC_cum_epoch(iepoch, t_0) = (AUC_cum_epoch(iepoch, t_1) * t_1 + stepStats{istat}.meanAUC(t)) / t_0;
            Time_cum_epoch(iepoch, t_0) = (Time_cum_epoch(iepoch, t_1) * t_1 + stepStats{istat}.meanTime(t)) / t_0;
        end
    end
    
    stepStats{istat}.cumMeanLLH_epoch = LLH_cum_epoch;
    stepStats{istat}.cumMeanAUC_epoch = AUC_cum_epoch;
    stepStats{istat}.cumMeanTime_epoch = Time_cum_epoch;
end


%% Step 3: Plot Cumulative meanLLH and meanAUC.

% You are only allow to set 3 colors. Come on, Matlab, that's it?
set(0,'DefaultAxesLineStyleOrder',{'-o',':s','--+','-.d'})
set(0,'DefaultAxesColorOrder',[0 0 1; 0 1 0; 1 0 0])

figure()
coordX = linspace(1,nChunks,nChunks);

names = {'Stepwise EM'};

for istat = 1 : nStorages
    subplot(2,1,1);
    %errorbar(coordX, stepStats{istat}.cumMeanLLH, stepStats{istat}.stdErrLLH, 'LineWidth', 1);
    plot(coordX, stepStats{istat}.cumMeanLLH);
    xlabel('hours')
    ylabel('cumulative average LLH')
    title('Four storages');
    hold all;
    
    subplot(2,1,2);
    %errorbar(coordX, stepStats{istat}.cumMeanAUC, stepStats{istat}.stdErrAUC, 'LineWidth', 1);
    plot(coordX, stepStats{istat}.cumMeanAUC);
    xlabel('hours')
    ylabel('cumulative average AUC')
    title('Four storages');
    hold all;
end
legend(names, 'Location', 'Southeast')

hold off;

%% Step 4: Plot Cumulative meanLLH by EPOCH
figure()
coordX = linspace(1,nChunks_real, nChunks_real);
names = {'Stepwise EM'};
for istat = 1 : nStorages
    subplot(2,2,istat);
    
    for iepoch = 1:nEpoch
        %errorbar(coordX, stepStats{istat}.meanLLH, stepStats{istat}.stdErrLLH, '--x', 'LineWidth', 2);
        plot(coordX, stepStats{istat}.cumMeanLLH_epoch(iepoch,:));
        xlabel('hours')
        ylabel('cumulative average LLH')
        hold all;
    end
    title(names{istat})
end
legend({'epoch 1', 'epoch 2', 'epoch 3'}, 'Location', 'Southeast');

%% Step 5: Plot Cumulative meanAUC by EPOCH

figure()
coordX = linspace(1,nChunks_real, nChunks_real);
names = {'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)'};
for istat = 1 : nStorages
    subplot(2,2,istat);
    
    for iepoch = 1:nEpoch
        %errorbar(coordX, stepStats{istat}.meanLLH, stepStats{istat}.stdErrLLH, '--x', 'LineWidth', 2);
        plot(coordX, stepStats{istat}.cumMeanAUC_epoch(iepoch,:));
        xlabel('hours')
        ylabel('cumulative average AUC')
        hold all;
    end
    title(names{istat})
end
legend({'epoch 1', 'epoch 2', 'epoch 3'}, 'Location', 'Southeast');

%% Plot Time statistics.

figure();
for istat = 1 : nStorages
    subplot(2,4,[1 4])
    coordX = linspace(1,nChunks, nChunks);
    %errorbar(coordX, stepStats{istat}.cumMeanTime, stepStats{istat}.stdErrTime, '--.', 'LineWidth', 1);
    plot(coordX, stepStats{istat}.cumMeanTime);
    xlabel('hours')
    ylabel('cumulative average Time (sec)')
    legend(names, 'Location', 'Southeast')
    title('All storages');
    hold all;
    
    subplot(2,4,istat + 4);
    coordX = linspace(1,nChunks_real, nChunks_real);
    for iepoch = 1:nEpoch
        %errorbar(coordX, stepStats{istat}.meanLLH,
        %stepStats{istat}.stdErrLLH, '--x', 'LineWidth', 2);
        plot(coordX, stepStats{istat}.cumMeanTime_epoch(iepoch,:));
        xlabel('hours')
        ylabel('cumulative average Time (sec)')
        hold all;
    end
    title(names{istat})
end
legend({'epoch 1', 'epoch 2', 'epoch 3'}, 'Location', 'SoutheastOutside');



%%
%{
%% Step 3: Plot meanLLH.

figure()
coordX = linspace(1,nChunks,nChunks);

for istat = 1 : nStorages
    subplot(2,1,1);
    %errorbar(coordX, stepStats{istat}.meanLLH, stepStats{istat}.stdErrLLH, '--x', 'LineWidth', 2);
    plot(coordX, stepStats{istat}.meanLLH);
    xlabel('hours')
    ylabel('LLH')
    hold all;
    
    subplot(2,1,2);
    %errorbar(coordX, stepStats{istat}.meanAUC, stepStats{istat}.stdErrAUC, '--x', 'LineWidth', 2);
    plot(coordX, stepStats{istat}.meanAUC);
    xlabel('hours')
    ylabel('AUC')
    legend({'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)', 'Full Set'}, 'Location', 'Southeast')
    hold all;
end

    subplot(2,1,1);
    plot(coordX, fullLLH);
    xlabel('hours')
    ylabel('LLH')
    hold all;
    
    subplot(2,1,2);
    plot(coordX, fullAUC);
    xlabel('hours')
    ylabel('AUC')
    legend({'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)', 'Full Set'}, 'Location', 'Southeast')
    hold all;


%%
for istat = 1 : nStorages
    avgLLH = sum(stepStats{istat}.meanLLH) / numel(stepStats{istat}.meanLLH);
    avgAUC = sum(stepStats{istat}.meanAUC) / numel(stepStats{istat}.meanAUC);
    avgStdLLH = sum(stepStats{istat}.stdDevLLH) / numel(stepStats{istat}.stdDevLLH);
    avgStdAUC = sum(stepStats{istat}.stdDevAUC) / numel(stepStats{istat}.stdDevAUC);
    disp(['istat=', int2str(istat), ', avgLLH=', num2str(avgLLH), ', avgAUC=', num2str(avgAUC), ', avgStdLLH=', num2str(avgStdLLH), ', avgStdAUC=', num2str(avgStdAUC)]);
end
disp(['full dataset, avgLLH=', num2str(mean(fullLLH)), ', avgAUC=', num2str(mean(fullAUC))]);


%clearvars -except 'stepStats' 'nChunks' 'nChunks_real' 'nStorages' 'fullLLH' 'fullAUC' 'fullModels', ;
%}
