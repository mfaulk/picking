%{

Descriptions:
    This script do the actual experiments to compare GMM trained by
    coresets vs. by online EM algorithm.

%}

%% Setups

% Unleash the 8 core CPU power!
import parallel.*;
openPool();

close all;


if ~exist('cTrain_set','var') || ~exist('cTest_set','var')
    load('experiments/coreset_vs_onlineEM/mf_tues_preprocessed_newversion');
end


%% Experiments on cTrain_set and cTest_set

import feature.*;
import storage.*;
import gmm.*;
import coreset_wrapper.*

%%%%%%%%%%%%%%%%%%%%%%%% Variable to be changed %%%%%%%%%%%%%%%%%%%
coreParams.coresetSize = 1000;   % small compared with chunksize
coreParams.k = 6;  % 6 Gaussians
coreParams.beta = 2 * coreParams.k;

CTS = cumulativeTreeStorage(coreParams, nChunks);
GDS = GmmDataStorage(100, 0);


AUC = zeros((nChunks - 1), 1);
cTP = cell((nChunks - 1), 1);
cFP = cell((nChunks - 1), 1);

%h = waitbar(0,'Initializing waitbar...');

%kc =KMedianCoresetAlg();% the kmeans coreset algorithm.
%delta = 0.1;
%cType = KMedianCoresetAlg.quadraticInK;

for t = 1 : (nChunks - 1) % The last chunk doesn't have a test set.
    
    %waitbar(t/(nChunks - 1),h,'Main loop...')
    
    % Convert the cell format into matrix for coreset construction.
    trainingSet_mat = Feature.unpackFeatureVectors(cTrain_set(:,t));
    %testSet_mat = Feature.unpackFeatureVectors(cTest_set(:,t+1));
    testSet_mat = Feature.unpackFeatureVectors(cTest_set(:,t));
    

    %[X, w] = CTS.addChunkData(trainingSet_mat);
    
    % Use GDS stores trainingSet_mat and return the union of all the 
    % previous data in X. w is uniform weight.
    [X, w] = GDS.fullMemoryAdd(trainingSet_mat);
    
    
    nGaussians = coreParams.k;
    nRestart = 10;
    gmFull = gmm.Gmm.fitGmmWeighted(X, w, nGaussians, nRestart);
    LLH_full = gmFull.logLikelihood(testSet_mat(:,1:288));
    
    %[X, w] = GDS.randomMemoryAdd(TrainingFeature_Mat);
    %disp(int2str(size(X,2)));
    [X, w] = CoresetUtils.computeCoresetKMedian(X, coreParams.coresetSize, coreParams.k, coreParams.beta);
    
    % learn GMM
    % gm = GaussianMixture.learnGaussianMixture(cTrainingFeatures, nGaussians, 1); % <=== Magic Number
    nGaussians = coreParams.k;
    nRestart = 10;
    %gm = gmm.Gmm.fitGmmWeighted(X, w, nGaussians, nRestart);
    gmCoreset = gmm.Gmm.fitGmmWeighted(X, w.*100, nGaussians, nRestart);
    LLH_coreset = gmCoreset.logLikelihood(testSet_mat(:,1:288));
    
    
    %params.gm = gmCoreset;

    % evaluate training data
    P = gmCoreset.evaluateProbability(testSet_mat);

    %TrueLabels = Feature.unpackLabels(cTest_set(:,t+1));
    TrueLabels = Feature.unpackLabels(cTest_set(:,t));

    Assignments = -log(P);

    % a low hack to deal with the infinite!
    infIndices = (Assignments == inf);
    Assignments(infIndices) = 1000;
    nThresh = 1000;

    Thresholds = linspace(min(Assignments), max(Assignments), nThresh);

    import roc.*;
    [tp, fp] =  myROC( TrueLabels, Assignments, Thresholds );
    areaUnderCurve = auc(fp,tp);
    %disp(num2str(areaUnderCurve))
    
    AUC(t) = areaUnderCurve;
    cTP{t} = tp;
    cFP{t} = fp;
    %models{i} = gm;
    %cThresholds{i} = Thresholds;
    %cParams{i} = params; % save C, mu, sigma
    
    %plot(cFP{t}, cTP{t});
    
    subplot(2,1,1);
    plot(fp, tp, '.-');
    grid on
    xlabel('FPR');
    ylabel('TPR');
    subplot(2,1,2);
    plot([t;t], [LLH_full; LLH_coreset], '.r');
    drawnow;
    hold on;
    
end

close(h);
