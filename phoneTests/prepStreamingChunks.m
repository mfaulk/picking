function [cTrain_set, cTest_set, nChunks] = prepStreamingChunks(acsnFile, chunksize_minute, sacDir)
%{

Descriptions:

    This function does the following things (each is a stage in a cell):

    1. Read the acsnFile with PhoneRecord class in phone package.
    2. Segment the data into 8.5 second segments with 2.5 sliding window
       using SegmentSynthesizer.segmentPhoneRecord.
    3. Divide the data into chunks of chunksize_minute minutes. Randomize 
       and choose training set (80% of the dataset) and test set (20%).
    4. Compute PCA feature vectors for training set. Store the PCA parameters
       to be applied in PCA on test set.
    5. Compute PCA feature vectors for test set.
    

    Notes: 
        1. This function prints out time for each tasks with disp.
        2. It invokes waitbar.
        

Inputs:
    acsnFile - string. The path to the file from the directory where 
               prepTimeSeriesChunks(...) is executed. 
               Ex: 'A/rjc_10022010_3.acsn'
    
    chunksize_minute - scalar. The number of minutes in a chunk. To give an
                       intuition, 60 minutes is about 1440 8.5s-segments
                       with 2.5s sliding window.

    sacDir [optional] - string. The directory containing sac file for
                        synthesizing earthquake in the testing dataset.
                        Default = 'data/sac/6-8__HN'.

Outputs:
    cTrain_set - [nTrain_per_chunk x nChunks] cell array. Each column is a
                 cell array of Feature.PcaFeature objects.

    cTest_set - [nTest_per_chunk*2 x nChunks] cell array. Each column is a
                cell array of Feature.PcaFeature objects.

    nChunks - scalar. Number of chunks.


Author: Dai Wei (2011 Summer)

%}



    %% Stage 0. Setups.
    
    import phone.*
    import parallel.*
    import check.*
    import feature.*
    import phone.*
    import sac.*
    import gmm.*
    import synthetic.*
    import roc.*
    
    % Allow parallel executions.
    openPool();

    % Since chunksize_minute is important, we display it.
    disp(['chunksize = ', int2str(chunksize_minute), '. Please confirm.']);
    
    

    %% Stage 1. Read the acsnFile with PhoneRecord class in phone package.    
    disp(['Stage 1. Read the acsnFile with PhoneRecord class in phone package.']);
    
    ticID = tic;
    phoneRecord = PhoneRecord(acsnFile);
    time_loading = toc(ticID);
    

    
    
    disp(['==> Stage 1 completed in ', int2str(time_loading), ' seconds.']);
    
    
    
    %% Stage 2. Segment the data into 8.5 second segments with 2.5 sliding window
    %  using SegmentSynthesizer.segmentPhoneRecord.
    
    disp(['Stage 2. Segment the data into 8.5 second segments']);

    ticID = tic;

    % later, I'm going to make Long-term, short-term features, so I'll make the
    % segments long enough to leave room for those computations:
    samplesPerSecond = 50; 
    ltLength = 5; % seconds
    stLength = 2.5; % seconds
    deadTime = 1; % seconds of "dead time" between short term and long term buffers
    segmentLengthSeconds = ltLength + stLength +deadTime;
    slidingSec = 2.5;

    % cPhoneSegments is a column cell array of UniformTimeSeries objects
    cPhoneSegments = SegmentSynthesizer.segmentPhoneRecord(phoneRecord, segmentLengthSeconds, samplesPerSecond, slidingSec);
    time_segmenting = toc(ticID);
    clear phoneRecord; % free up memory

    disp(['==> Stage 2 completed in ', int2str(time_segmenting), ' seconds.']);

    
    
    %% Stage 3. Divide the data (cPhoneSegments) into chunks of 
    % chunksize_minute minutes. Randomize and choose training set and 
    % test set.
    
    disp(['Stage 3. Divide the data (cPhoneSegments) into chunks']);
    
    ticID = tic;

    nSegments = size(cPhoneSegments, 1);
    chunksize = floor(chunksize_minute * 60 / 2.5); % number of segments in a chunk
    nChunks = floor(nSegments / chunksize);
    cPhoneSegments = cPhoneSegments(1: (nChunks * chunksize), :);

    % cChunks is a [chunksize x nChunks] cell array. (each column is a chunk)
    cChunks = reshape(cPhoneSegments, [chunksize nChunks]);
    clear cPhoneSegments;

    
    % Randomly select training set (80% of the chunk) and test set (20%)
    % from each chunk (column).

    % Transpose, and randomize cChunks along each row (i.e., within each chunk of data)
    % The result is in cRandchunks.
    cRowChunks = cChunks';
    m=size(cRowChunks,1);
    [dummy J]=sort(rand(size(cRowChunks)),2);
    Ilin = bsxfun(@plus,(J-1)*m,(1:m)');
    cRandchunks = cRowChunks; % copy cChunks
    cRandchunks(:)=cRandchunks(Ilin);
    clear J;
    clear cChunks;
    clear cRowChunks;
    clear dummy;
    clear Ilin;

    cRandchunks = cRandchunks'; % cRandchunks is now [chunksize x nChunks]

    % Select the 80% from each chunk as training set; 20% as test set
    nTrain_per_chunk = floor(chunksize * 4 / 5);
    nTest_per_chunk = chunksize - nTrain_per_chunk;

    cTrain_set = cRandchunks(1:nTrain_per_chunk, :);
    cTest_set = cRandchunks((nTrain_per_chunk + 1):chunksize, :);
    clear cRandchunks;

    time_randomizing = toc(ticID);
    
    disp(['==> Stage 3 completed in ', int2str(time_randomizing), ' seconds.']);
    
    
    
    %% Stage 4. Compute feature vectors for training set. Store the PCA parameters
    % to be applied in PCA on test set.  
    
    disp('Stage 4. Compute Pca features for training set.');
    
    ticID = tic;
    
    % Rehsape cTrain_set into a column vector 
    %   for GmLtStParameterSelection.computeTrainingLtStFeatures
    cTrain_set_column = reshape(cTrain_set, [(nChunks * nTrain_per_chunk) 1]);

    nFFT = 16;
    nMoms = 1;
    dimensionsRetained = 16;

    % PCA cTrain_set_column and save the PCA parameters
    [cTrain_set_column, C, mu, sigma ]= ...
        GmLtStParameterSelection.computeTrainingLtStFeatures(cTrain_set_column, ltLength, stLength, nFFT, nMoms, dimensionsRetained, 2); % 2 is the "normal label"

    % reshape cTrainingFeatures to [nTrain_per_chunk x nChunks] cell array.
    cTrain_set = reshape(cTrain_set_column, [nTrain_per_chunk nChunks]);
    clear cTrain_set_column;
    time_pca = toc(ticID);
    
    disp(['==> Stage 4 completed in ', int2str(time_pca), ' seconds.']);
    
    
    

    %% Stage 5. Compute PCA feature vectors for test set.
    
    disp('Stage 5. Compute PCA feature vectors for test set.');

    ticID = tic;
    
    % Load sac data.
    if ~exist('sacDir', 'var')
        sacDir = 'data/sac/6-8__HN';    % Use large earthquakes
    end
    cSacRecords = SacLoader.loadSacRecords(sacDir);

    
    % create synthetic data on the entire cTest_set. Preserve the no-quake data
    % in cTest_noquake_set
    cTest_quake_set = cTest_set;
    cTest_noquake_set = cTest_set;

    % cTest_set will hold 20% synthetic earthquake and 80% noquake data after PCA.
    % cTest_set_temp will be cTest_set, but don't erase cTest_set so that we
    % can rerun this cell of codes.
    cTest_set_temp = cell(nTest_per_chunk * 2, nChunks);

    % Create synthetic earthquake with cTest_set and generate corresponding labels
    % (see the sac tutorial)

    threshold = 0.3;
    peakAmplitude = 0; % this isn't used anymore

    parfor ichunk = 1:nChunks,

        cTest_quake_set_chunk = cTest_quake_set(:, ichunk);
        cTest_noquake_set_chunk = cTest_noquake_set(:, ichunk);

        % Add recorded earthquake to cTest_quake_set_chunk
        cTest_quake_set_chunk = ...
                        synthetic.SegmentSynthesizer.createOnsetSegments(cTest_quake_set_chunk, cSacRecords, ...
                        ltLength+deadTime, stLength, threshold, peakAmplitude, samplesPerSecond);


        % Apply the PCA params to cTest_noquake_set_chunk and
        % cTest_quake_set_chunk. computeTestingLtStFeatures generates
        % LtStFeatures and run PCA on it with the given PCA params.
        cTest_set_temp(:, ichunk) = ...
            gmm.GmLtStParameterSelection.computeTestingLtStFeatures(cTest_noquake_set_chunk, cTest_quake_set_chunk, ...
            ltLength, stLength, nFFT, nMoms, C, mu, sigma, dimensionsRetained);

    end
    clear cTest_quake_set_chunk;
    clear cTest_noquake_set_chunk;
    clear cTest_quake_set;
    clear cTest_noquake_set;

    time_synQuake = toc(ticID);
    
    cTest_set = cTest_set_temp;
    clear cTest_set_temp;
    

    disp(['==> Stage 5 completed in ', int2str(time_synQuake), ' seconds.']);
    
end

