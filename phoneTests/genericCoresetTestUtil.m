classdef genericCoresetTestUtil
%{

Descriptions:
    This class is a hodge-podge containing what Dai Wei thinks is a modular function but too 
    trivial to form a class by itself.
    
Author: Dai Wei (2011 Summer)
%}
    
    
    properties (Constant)
        NO_COMPRESSION = 0;
        CORESET_COMPRESSION = 1;
        RANDSUBSET_COMPRESSION = 2;
    end
    
    % ====================================================================
    
    methods (Static)
        
        % ----------------------------------------------------------------
        
        function gmailSetup()
        %{
        
        Descriptions:
            Set up gmail daiwei89.ad@gmail.com, which is just my spam email account,
            so that you can send email with the command:
            
            sendmail('you@gmail.com', 'Title', 'Content');
            
            to keep track of the progress of a long run job.
            
        %}
            
            setpref('Internet', 'E_mail', 'daiwei89.ad@gmail.com');
            setpref('Internet', 'SMTP_Username', 'daiwei89.ad@gmail.com');
            setpref('Internet', 'SMTP_Password', 'ddd951753456');
            setpref('Internet', 'SMTP_Server', 'smtp.gmail.com');
            props = java.lang.System.getProperties;
            props.setProperty('mail.smtp.auth','true');
            props.setProperty('mail.smtp.socketFactory.class', 'javax.net.ssl.SSLSocketFactory');
            props.setProperty('mail.smtp.socketFactory.port', '465'); 
        end
        
        % ----------------------------------------------------------------
        
        function [fullLLH, fullAUC, fullModels, fullX, fullW] = ...
                fullDatasetAccuracyRun(nChunks, nChunks_real, cTrain_set, cTest_set, compression_mode, memorySize)
        %{
        
        This function has to work closely with FourStorageComparison_par.m
                
        Inputs:
            nChunks_real - scalar. Number of hours (chunks).
            nChunks - scalar. Integer multiples of nChunks_real.
            cTrain_set 
                
        Outputs:
            fullLLH - [1 x nChunks] row vector. fullLLH(t) is the log-likelihood at time t.
            fullAUC - [1 x nChunks] row vector. fullAUC(t) is the Area Under Curve (ROC curve) at time t.
            fullModes - {1 x nChunks} row cell array, storing the full history fitted gmm models.
            fullX - 
            
        %}
            
            import parallel.*
            import feature.*
            import gmm.*
            
            if ~exist('compression_mode', 'var')
                compression_mode = genericCoresetTestUtil.NO_COMPRESSION;
                memorySize = Inf; % This is necessary for parfor
                
                % No need to return fullX, W if there's no compression
                fullX = [];
                fullW = [];
            elseif ~exist('memorySize', 'var')
                error('Must supply memorySize if you want to do data compression.');
            else
                % Store all the intermediate Xtrain and Wtrain
                fullX = cell(nChunks,1);
                fullW = cell(nChunks,1);
            end

            % Hardcode in k
            k = 6;
            coresetParams.k = k;
            coresetParams.beta = k*2;
            
            
            % Prepare all training set, test set in matrix format.
            nTest_perHour = size(feature.Feature.unpackFeatureVectors(cTest_set(:,1)), 2); % Half of it is synthetic earthquake
            nTrain_perHour = size(feature.Feature.unpackFeatureVectors(cTrain_set(:,1)), 2);
            Xtest_all = zeros(17, nChunks_real * nTest_perHour);
            Ltest_all = zeros(1, nChunks_real * nTest_perHour); % test labels (row vector)
            Xtrain_all = zeros(17, nChunks_real * nTrain_perHour);
            for t = 1:nChunks_real

                % Unpack test set and test labels
                istart_test = (t - 1) * nTest_perHour + 1;
                iend_test = istart_test + nTest_perHour - 1;
                Xtest_all(:,istart_test:iend_test) = feature.Feature.unpackFeatureVectors(cTest_set(:,t));
                Ltest_all(:,istart_test:iend_test) = feature.Feature.unpackLabels(cTest_set(:,t))';

                % Unpack training set.
                istart_train = (t - 1) * nTrain_perHour + 1;
                iend_train = istart_train + nTrain_perHour - 1;
                Xtrain_all(:,istart_train:iend_train) = feature.Feature.unpackFeatureVectors(cTrain_set(:,t));
            end
            

            % return values.
            fullLLH = zeros(1, nChunks);    
            fullAUC = zeros(1, nChunks);
            fullModels = cell(1, nChunks); % row cell array.
            
            openPool();
            parfor t = 1:nChunks,

                ticID_full = tic;

                % Generate training set and test set on the fly.
                Xtrain = genericCoresetTestUtil.repData(Xtrain_all, nChunks_real, t, nTrain_perHour);
                Wtrain = ones(1, t * nTrain_perHour);
                Xtest = genericCoresetTestUtil.repData(Xtest_all, nChunks_real, t, nTest_perHour);
                Ltest = genericCoresetTestUtil.repData(Ltest_all, nChunks_real, t, nTest_perHour);

                % Compress the dataset (one-time compression).
                if compression_mode == genericCoresetTestUtil.CORESET_COMPRESSION
                    % Use coreset to compress it down to memorySize
                    % datapoints
                    coresetSize = memorySize;
                    Xtrain = coreset_wrapper.CoresetUtils.computeCoresetKMedian(Xtrain, coresetSize, coresetParams.k, coresetParams.beta);
                    Wtrain = ones(1, size(Xtrain,2));
                    fullX{t} = Xtrain;
                    fullW{t} = Wtrain;
                elseif compression_mode == genericCoresetTestUtil.RANDSUBSET_COMPRESSION
                    % Use random subsampling to down sample
                    nData = size(Xtrain, 2);
                    ind = randperm(nData);
                    Xtrain = Xtrain(:,ind <= memorySize);
                    Wtrain = ones(1, size(Xtrain,2));
                    fullX{t} = Xtrain;
                    fullW{t} = Wtrain;
                end
                fprintf(1,'t = %d: Xtrain = %d, Xtest = %d \n',t, size(Xtrain,2),size(Xtest,2));
                % The actual accuracy calculation for full set.
                [fullLLH(1,t), fullAUC(1,t), fullModels{t}] = ...
                    AccuracyTestModule.basicAccuracyMetrics(Xtrain, Wtrain, Xtest, Ltest);

                %disp(['==> t = ', int2str(t), ' completed in ', int2str(toc(ticID_full)), ' seconds.']);

            end
            %matlabpool close;
        end
        
        % ----------------------------------------------------------------
        
        function f = plot17dDataNGmm(X1, w1, model, X2, w2)
        %{
    
        Descriptions:
            
            Only allow 2 dataset to be plotted to avoid over-crowdedness.
            
        Inputs:
            X1, w1 - data set [d x n] with weights [1 x n]. Will be plotted in blue.
            model - gmm object.
            X2, w2 - (optional). Same as X1, w1, but will be plotted in red.
            
        Outputs:
            f - a figure handle (gcf) for futher manipulations.
   
        %}
            
            % Sanity checks
            if ~isa(model, 'gmm.Gmm')
                error('model must be of gmm.Gmm class.');
            end
            
            if size(X1,1) ~= 17
                error('Dataset must be 17 dimensional.');
            end
            
            % Normalize the weights so that sum(w) = n.
            %nX1 = size(X1,2);
            %w1 = bsxfun(@rdivide, w1, sum(w1)) .* nX1;
            %if exist('X2','var') && exist('w2','var')
            %    nX2 = size(X2,2);
            %    w2 = bsxfun(@rdivide, w2, sum(w2)) .* nX2;
            %end
            
            f = figure();
            %{
            % assume gmm is computed from X2, w2 if they are given.
            % Otherwise use X1, w1.
            if exist('X2','var') && exist('w2','var')
                Xgmm = X2;
                Wgmm = w2;
            else
                Xgmm = X1;
                Wgmm = w1;
            end
            % Compute R (responsibility matrix) for model and (Xgmm, Wgmm)
            R = genericCoresetTestUtil.expectation(Xgmm, Wgmm, model);
            
            % Find the means of the gaussians from R.
            % gmeans is [k x d] matrix. gmeans(k,:) is the mean for the kth
            % guassian.
            weighted_Xgmm = bsxfun(@times, Xgmm, Wgmm);
            gmeans = (weighted_Xgmm * R ./ sum(Wgmm))';
            
            k = model.nGaussians;
            assert(size(gmeans,1) == k && size(gmeans, 2) == 17, ...
                sprintf('gmeans [%d x %d]\n',size(gmeans,1), size(gmeans, 2)));
            %}
            ngrids = 50; % The higher the finer the plot, but the longer it takes
            
            for iplot = 1:8
                h = subplot(2,5,iplot);
                
                d1 = 2 * iplot - 1;
                d2 = 2 * iplot;
                scatter(X1(d1,:)', X1(d2,:)', w1', 'b');
                hold on;
                
                if exist('X2','var') && exist('w2','var')
                    scatter(X2(d1,:)', X2(d2,:)', w2', 'r');
                    hold on;
                end
                
                xlabel(['component ', int2str(d1)]);
                ylabel(['component ', int2str(d2)]);
                
                % Plot the contours of gaussians.
                xlim = get(gca, 'XLim');
                ylim = get(gca, 'YLim');
                
                rangeX = linspace(xlim(1), xlim(2), ngrids);
                rangeY = linspace(ylim(1), ylim(2), ngrids);
                [X,Y] = meshgrid(rangeX, rangeY);
                
                for iguassian = 1:model.nGaussians
                
                    %gmeans(iguassian,:)'
                    %model.mu(:,iguassian)
                    %Z1 = arrayfun(@(x,y) model.evaluateProbability2D(x, y, d1, d2, iguassian, gmeans(iguassian,:)'), X, Y);
                    Z2 = arrayfun(@(x,y) model.evaluateProbability2D(x, y, d1, d2, iguassian, model.mu(:,iguassian)), X, Y);
                    %Z3 = arrayfun(@(x,y) model.evaluateProbability2D(x, y, d1, d2, iguassian, zeros(17,1)), X, Y);
                    %[C,h] = contour(X,Y,Z1, 5,'g');
                    [C,h] = contour(X,Y,Z2, 2,'g');
                    %[C,h] = contour(X,Y,Z3, 1,'k');
                    %colormap cool;
                    set(h,'ShowText','on','TextStep',get(h,'LevelStep')*2)
                    text(model.mu(d1,iguassian), model.mu(d2,iguassian), int2str(iguassian));
                    
                    hold on;
                end
            end
            
            % The last plot is dimension 1 vs dimension 17.
            subplot(2,5,9);
            scatter(X1(1,:)', X1(17,:)', w1', 'b');
            hold on;
            if exist('X2','var') && exist('w2','var')
                scatter(X2(1,:)', X2(17,:)', w2','r');
            end
            xlabel(['component 1']);
            ylabel(['component 17']);
            
            % Plot the contours of gaussians.
            xlim = get(gca, 'XLim');
            ylim = get(gca, 'YLim');

            rangeX = linspace(xlim(1), xlim(2), ngrids);
            rangeY = linspace(ylim(1), ylim(2), ngrids);
            [X,Y] = meshgrid(rangeX, rangeY);
            
            for iguassian = 1:model.nGaussians
                %Z1 = arrayfun(@(x,y) model.evaluateProbability2D(x, y, 1, 17, iguassian, gmeans(iguassian,:)'), X, Y);
                Z2 = arrayfun(@(x,y) model.evaluateProbability2D(x, y, 1, 17, iguassian, model.mu(:,iguassian)), X, Y);
                %Z3 = arrayfun(@(x,y) model.evaluateProbability2D(x, y, 1, 17, iguassian, zeros(17,1)), X, Y);
                %[C,h] = contour(X,Y,Z1, 5,'g');
                [C,h] = contour(X,Y,Z2, 2,'g');
                %[C,h] = contour(X,Y,Z3, 1,'k');
                set(h,'ShowText','on','TextStep',get(h,'LevelStep')*2)
                text(model.mu(1,iguassian), model.mu(17,iguassian), int2str(iguassian));
                
                colormap cool;
                hold on;
            end
            
            subplot(2,5,10);
            s(1) = {'nth  | weight'};
            for iguassian = 1:model.nGaussians
                s(iguassian + 1) = {sprintf('g%d, %.4f', iguassian, model.weight(iguassian))};
            end
            text(0.3,0.8,s);
        end
        
        
        
        % ----------------------------------------------------------------
        
        function return_mat = repData(seed_mat, nChunks_real, t, nData_per_hour)
        %{
            
        Descriptions:
            This help to duplicate dataset (represented as [d x n] matrix) 
            horizontally with given parameter. This is used when we pass 
            the dataset multiple times.
        
        Inputs:
            
        Outputs:
            return_mat - [d x t * nData_per_hour] matrix.
        
        %}
            
            nth_repeat = floor(t / nChunks_real); 
            if nth_repeat > 0
                return_mat = repmat(seed_mat, 1, nth_repeat);
            else
                return_mat = [];
            end

            n_rmd_hour = rem(t, nChunks_real);
            if n_rmd_hour > 0
                return_mat = [return_mat, seed_mat(:,1:n_rmd_hour * nData_per_hour)];
            end

            assert(size(return_mat,2) == t * nData_per_hour, ['failed to produce dataset for t =', int2str(t)]);
        end
        
        % ----------------------------------------------------------------
        
        function [R, llh] = expectation(X, weights, model)
        %{
            
        Descriptions:
            This is the same function as in wemgm_class, but static. Once 
            that function is made static, this function can go away.
            
        Inputs:
            X - d x n data matrix
            weights - [1 x n] vector of nonnegative weights
            model - struct of GMM. See the output of maximization function.

        Outputs:
            R - [n x k] responsibility matrix.
            llh - log-likelihood. See documentation for wemgm above.

        %}
            %assert(size(X,2) == size(weights,1), ...
            %    sprintf('X (%d points) does not match weights (%d points).\n', size(X,2), size(weights,1)));
            
            import gmm.*;
            mu = model.mu;
            Sigma = model.Sigma;
            w = model.weight';

            n = size(X,2);
            k = size(mu,2);

            % Log of responsibility matrix R
            logR = zeros(n,k);

            for i = 1:k
                logR(:,i) = genericCoresetTestUtil.loggausspdf(X,mu(:,i),Sigma(:,:,i));
            end

            % w * R is addition in log.
            logR = bsxfun(@plus,logR,log(w));
            T = genericCoresetTestUtil.logsumexp(logR,2);      % T ([n x 1] matrix). T(i) is the normalization
                                        %   constant for each data point.
            llh = (weights*T)/sum(weights);     % loglikelihood
            logR = bsxfun(@minus,logR,T);   % normalize each data point.
            R = exp(logR);
        end
        
        % ----------------------------------------------------------------
        
        function y = loggausspdf(X, mu, Sigma)
        %{

        Inputs:
            X - See documentation for wemgm above.

        Outputs:
            y - [1 x n] matrix representing log(N(X|mu, Sigma)), that is, the 
                probability of the log of normal distribution with 
                [mean, deviation] = [mu, Sigma] at each data point of X.

        %}
            import gmm.*;
            d = size(X,1);
            X = bsxfun(@minus,X,mu);

            % Cholesky Factorization. Return value p suppress none-PD error, 
            %   which we show in (if p ~= 0). R = Sigma^(1/2)
            [R,p]= chol(Sigma);
            if p ~= 0
                error('ERROR: Sigma is not PD.');
            end
            q = sum((R'\X).^2,1);  % quadratic term (M distance)
            c = d*log(2*pi)+2*sum(log(diag(R)));   % normalization constant
            y = -(c+q)/2;
        end
        
        % ----------------------------------------------------------------
        
        function s = logsumexp(x, dim)
        % Compute log(sum(exp(x),dim)) while avoiding numerical underflow.
        %   By default dim = 1 (columns).
        % Written by Michael Chen (sth4nth@gmail.com).
            if nargin == 1, 
                % Determine which dimension sum will use
                dim = find(size(x)~=1,1);
                if isempty(dim), dim = 1; end
            end

            % subtract the largest in each column
            y = max(x,[],dim);
            x = bsxfun(@minus,x,y);
            s = y + log(sum(exp(x),dim));
            i = find(~isfinite(y));
            if ~isempty(i)
                s(i) = y(i);
            end
        end

        % ----------------------------------------------------------------
    end
    
    % ====================================================================
    
    %{
    methods (Static, Access = private)
        
        % ----------------------------------------------------------------
        
        function plotGuassians(Xgmm, Wgmm, model)
        %{
        
        Descriptions:
            Helper for plot17dDataNGmm. Make contour plot on the current plot.
            
        %}
            
            for iguassian = 1:model.nGaussians
                
                Z = arrayfun(@(x,y) model.evaluateProbability2D(x, y, 1, 17, iguassian, gmeans(iguassian,:)), X, Y);
                [C,h] = contour(X,Y,Z, 2);
                colormap cool;
                hold all;
            end
        end
        
    end
    %}
    % ====================================================================
    
end

