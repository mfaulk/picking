%% Step 0: Setups
import online.*
import feature.*
import parallel.*
import gmm.*

clear;
clc;

% Set up email
genericCoresetTestUtil.gmailSetup();


load('experiments/coreset_vs_onlineEM/preprocessedMAT/7recordings_combined_262hrs');
%load('experiments/coreset_vs_onlineEM/preprocessedMAT/rjc_10022010.mat');
nChunks = 60; % Keep it < 100, so that nChunks * 3 < 100
cTest_set = cTest_set(:,1:nChunks);
cTrain_set = cTrain_set(:,1:nChunks);
nIterations = 3;




%% Step 1: Compute LLH on unradomized dataset, i.e., chronologically ordered, (a) without compression (b)
%% with Coreset compression. (c) with random subsampling compression
ticID_iter = tic;
memorySize = 1000; % The compression data can store at most memorySize datapoints.


% a: No compression (full dataset).
disp('************ FullDatasetStats_noRand **************');
[FullDatasetStats_noRand.LLH, FullDatasetStats_noRand.AUC, FullDatasetStats_noRand.Models] = ...
    genericCoresetTestUtil.fullDatasetAccuracyRun(nChunks, nChunks, cTrain_set, cTest_set);

% b: Coreset compression (CoresetComp) to 40000 data points.
disp('************ CoresetCompStats_noRand **************');
[CoresetCompStats_noRand.LLH, CoresetCompStats_noRand.AUC, CoresetCompStats_noRand.Models, CoresetX_noRand, CoresetW_noRand] = ...
    genericCoresetTestUtil.fullDatasetAccuracyRun(nChunks, nChunks, cTrain_set, cTest_set, genericCoresetTestUtil.CORESET_COMPRESSION, memorySize);

% c: Random subsampling compression (RandsubComp) to 40000 data points.
disp('************ RandsubCompStats_noRand **************');
[RandsubCompStats_noRand.LLH, RandsubCompStats_noRand.AUC, RandsubCompStats_noRand.Models] = ...
    genericCoresetTestUtil.fullDatasetAccuracyRun(nChunks, nChunks, cTrain_set, cTest_set, genericCoresetTestUtil.RANDSUBSET_COMPRESSION, memorySize);


msg = ['+++> iter = 0 (no randomization full dataset)', ' completed in ', int2str(toc(ticID_iter)), ' seconds.'];
disp(msg);
sendmail('daiwei89@gmail.com', 'Mail from MATLAB', msg);

%{
%% Step 2: Compute LLH on 3 randomized dataset (a) without compression (b)
%% with Coreset compression. (c) with random subsampling compression

% Remember the permutations.
randInds_all = zeros(nIterations,nChunks);

% Will store the stats for full dataset which is randomized (nIterations runs).
FullDatasetStats_Rand.LLH = zeros(nIterations, nChunks);
FullDatasetStats_Rand.AUC = zeros(nIterations, nChunks);
FullDatasetStats_Rand.Models = cell(nIterations, nChunks);

% Will store the calculated coreset history.
CoresetX_Rand = cell(nIterations,1);
CoresetW_Rand = cell(nIterations,1);

for iter = 1:nIterations % loop through each k.
    
    ticID_iter = tic;

    randInds = randperm(nChunks);
    randInds_all(iter,:) = randInds;
    
    % Randomize the test and training set.
    cTrain_set_rand = cTrain_set(:,randInds);
    cTest_set_rand = cTest_set(:,randInds);
    
    % a: No compression (full dataset).
    fprintf(1, '************ FullDatasetStats_Rand (iter=%d) **************\n', iter);
    [FullDatasetStats_Rand.LLH(iter,:), FullDatasetStats_Rand.AUC(iter,:), models] = ...
        genericCoresetTestUtil.fullDatasetAccuracyRun(nChunks, nChunks, cTrain_set_rand, cTest_set_rand);
    for t = 1:nChunks, FullDatasetStats_Rand.Models{iter,t} = models{t}; end

    % b: Coreset compression (CoresetComp) to 40000 data points.
    fprintf(1, '************ CoresetCompStats_Rand (iter=%d) **************\n', iter);
    [CoresetCompStats_Rand.LLH(iter,:), CoresetCompStats_Rand.AUC(iter,:), models, CoresetX_Rand{iter}, CoresetW_Rand{iter}] = ...
        genericCoresetTestUtil.fullDatasetAccuracyRun(nChunks, nChunks, cTrain_set_rand, cTest_set_rand, genericCoresetTestUtil.CORESET_COMPRESSION, memorySize);
    for t = 1:nChunks, CoresetCompStats_Rand.Models{iter,t} = models{t}; end
    
    % c: Random subsampling compression (RandsubComp) to 40000 data points.
    fprintf(1, '************ RandsubCompStats_Rand (iter=%d) **************\n', iter);
    [RandsubCompStats_Rand.LLH(iter,:), RandsubCompStats_Rand.AUC(iter,:), models] = ...
        genericCoresetTestUtil.fullDatasetAccuracyRun(nChunks, nChunks, cTrain_set_rand, cTest_set_rand, genericCoresetTestUtil.RANDSUBSET_COMPRESSION, memorySize);
    for t = 1:nChunks, RandsubCompStats_Rand.Models{iter,t} = models{t}; end
    
    msg = ['+++> randomized iter = ', int2str(iter), ' completed in ', int2str(toc(ticID_iter)), ' seconds.'];
    disp(msg);
    sendmail('daiwei89@gmail.com', 'Mail from MATLAB', msg);
end
%}
return;

%% POST PRODUCTION:

%% Consolidate the data

% Unrandomized runs
%{ 
    1 = Full Dataset; 
    2 = Coreset Compression; 
    3 = Random sampling compression.
%}
UnRandStats = cell(1,3);
UnRandStats{1} = FullDatasetStats_noRand;
UnRandStats{2} = CoresetCompStats_noRand;
UnRandStats{3} = RandsubCompStats_noRand;


%{
% Randomized runs
RandStats = cell(1,3);
RandStats{1} = FullDatasetStats_Rand;
RandStats{2} = CoresetCompStats_Rand;
RandStats{3} = RandsubCompStats_Rand;
%}

nCompressionModes = 3;


%% Compute cumulative LLH and AUC

% Unrandomized Data
for istat = 1 : nCompressionModes
    
    % Compute cumulative (mean) LLH and AUC
    UnRandStats{istat}.cumMeanLLH = zeros(1, nChunks);
    UnRandStats{istat}.cumMeanAUC = zeros(1, nChunks);

    % The first one is just the raw data.
    UnRandStats{istat}.cumMeanLLH(1) = UnRandStats{istat}.LLH(1);
    UnRandStats{istat}.cumMeanAUC(1) = UnRandStats{istat}.AUC(1);
    for t = 2:nChunks
        UnRandStats{istat}.cumMeanLLH(t) = (UnRandStats{istat}.cumMeanLLH(t-1) * (t-1) + UnRandStats{istat}.LLH(t)) / t;
        UnRandStats{istat}.cumMeanAUC(t) = (UnRandStats{istat}.cumMeanAUC(t-1) * (t-1) + UnRandStats{istat}.AUC(t)) / t;
    end
end


%{
% Randomized data
for istat = 1 : nCompressionModes
    
    % Compute LLH
    RandStats{istat}.meanLLH = mean(RandStats{istat}.LLH,1);
    RandStats{istat}.stdDevLLH = std(RandStats{istat}.LLH,0,1);
    RandStats{istat}.stdErrLLH = RandStats{istat}.stdDevLLH / sqrt(nIterations);

    % Compute AUC
    RandStats{istat}.meanAUC = mean(RandStats{istat}.AUC,1);
    RandStats{istat}.stdDevAUC = std(RandStats{istat}.AUC,0,1);
    RandStats{istat}.stdErrAUC = RandStats{istat}.stdDevAUC / sqrt(nIterations);
    
    % Compute cumulative mean LLH and AUC and Time
    RandStats{istat}.cumMeanLLH = zeros(1, nChunks);
    RandStats{istat}.cumMeanAUC = zeros(1, nChunks);
    
    % The first one is just the raw data.
    RandStats{istat}.cumMeanLLH(1) = RandStats{istat}.meanLLH(1);
    RandStats{istat}.cumMeanAUC(1) = RandStats{istat}.meanAUC(1);
    for t = 2:nChunks
        RandStats{istat}.cumMeanLLH(t) = (RandStats{istat}.cumMeanLLH(t-1) * (t-1) + RandStats{istat}.meanLLH(t)) / t;
        RandStats{istat}.cumMeanAUC(t) = (RandStats{istat}.cumMeanAUC(t-1) * (t-1) + RandStats{istat}.meanAUC(t)) / t;
    end
end
%}

%% Plot cumulative LLH

% You are only allow to set 3 colors. Come on, Matlab, that's it?
set(0,'DefaultAxesLineStyleOrder',{'-o',':s','--+','-.d'})
set(0,'DefaultAxesColorOrder',[0 0 1; 0 1 0; 1 0 0])

figure()
coordX = linspace(1,nChunks,nChunks);

    
for istat = 1 : nCompressionModes
    subplot(2,1,1);
    plot(coordX, UnRandStats{istat}.cumMeanLLH);
    xlabel('hours')
    ylabel('cumulative average LLH')
    title('Unrandomized (1 run)');
    legend({'Full Dataset', 'Coreset Compression (1000 points)', 'Random Subsample (1000 points)'}, 'Location', 'Southeast')
    hold all;
    
    %{
    subplot(2,1,2);
    plot(coordX, RandStats{istat}.cumMeanLLH);
    xlabel('hours')
    ylabel('cumulative average LLH')
    title('Randomized (3 runs)');
    legend({'Full Dataset', 'Coreset Compression (1000 points)', 'Random Subsample (1000 points)'}, 'Location', 'Southeast')
    hold all;
    %}
end
hold off;



%% Plot cumulative AUC

% You are only allow to set 3 colors. Come on, Matlab, that's it?
set(0,'DefaultAxesLineStyleOrder',{'-o',':s','--+','-.d'})
set(0,'DefaultAxesColorOrder',[0 0 1; 0 1 0; 1 0 0])

figure()
coordX = linspace(1,nChunks,nChunks);
names = {'Full Dataset', 'Coreset Compression (1000 points)', 'Random Subsample (1000 points)'};

for istat = 1 : nCompressionModes
    subplot(2,1,1);
    plot(coordX, UnRandStats{istat}.cumMeanAUC);
    xlabel('hours')
    ylabel('cumulative average AUC')
    title('Unrandomized (1 run)');
    legend(names, 'Location', 'Southeast')
    hold all;
    
    %{
    subplot(2,1,2);
    plot(coordX, RandStats{istat}.cumMeanAUC);
    xlabel('hours')
    ylabel('cumulative average AUC')
    title('Randomized (3 runs)');
    legend(names, 'Location', 'Southeast')
    hold all;
    %}
end
hold off;

%% Plot cumulative LLH (for separate runs)

figure()
coordX = linspace(1,nChunks,nChunks);
names = {'Full Dataset', 'Coreset Compression (40000 points)', 'Random Subsample (40000 points)'};

for istat = 1 : nCompressionModes
    subplot(3,1,istat);
    for iter = 1 : nIterations
        
        cumLLH = zeros(1,nChunks);
        cumAUC = zeros(1,nChunks);
        
        % The first one is just the raw data.
        cumLLH(1) = RandStats{istat}.LLH(iter, 1);
        cumAUC(1) = RandStats{istat}.AUC(iter, 1);
        for t = 2:nChunks
            cumLLH(t) = (cumLLH(t-1) * (t-1) + RandStats{istat}.LLH(iter, t)) / t;
            cumAUC(t) = (cumAUC(t-1) * (t-1) + RandStats{istat}.AUC(iter, t)) / t;
        end
        plot(coordX, cumLLH);
        %legend({'Full Dataset', 'Coreset Compression (40000 points)', 'Random Subsample (40000 points)'}, 'Location', 'Southeast')
        hold all;
    end
    xlabel('hours')
    ylabel('cumulative average LLH')
    title(names{istat});
end

legend({'run 1', 'run 2', 'run 3'},'Location', 'Southeast');
hold off;

%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END OF PROGRAM %%%%%%%%%%%%%%%%%%%%%%%%%%%%




%{



k = 6;
coresetParams.k = k;
coresetParams.beta = k*2;
nIterations = 1;


openPool();
parfor istat = 1 : nStorages
    allStats{istat}.LLH = zeros(nIterations, nChunks);
    allStats{istat}.AUC = zeros(nIterations, nChunks);
    allStats{istat}.Models = cell(nIterations, nChunks);
end


% Test set is the same for all iterations. Do it beforehand.
testSet_mat = cell(nChunks, 1);
TrueLabels = cell(nChunks, 1);
parfor t = 1:nChunks
    
    t_rmd = rem(t, nChunks_real) + 1;
    
    testSet_mat{t} = feature.Feature.unpackFeatureVectors(cTest_set(:,t_rmd));
    TrueLabels{t} = feature.Feature.unpackLabels(cTest_set(:,t_rmd))';
end


%% Step 1: Full Dataset
%ticID_full = tic;
%[fullLLH, fullAUC, fullModels] = genericCoresetTestUtil.fullDatasetAccuracyRun(nChunks, nChunks_real, cTrain_set, testSet_mat, TrueLabels);
%disp(['full dataset completed in ', int2str(toc(ticID_full)), ' seconds.']);

%% Step 2: Storages

% Divide runs into batches to circumvent the memory limitation.
assert(rem(nChunks_real, 2) == 0, 'nChunks_real has to be multiple of 2');
batchSize = nChunks_real / 2;
nBatches = nChunks / batchSize;



for iter = 1:nIterations
    
    % Clear the storages from previous iteration.
    coresetTree.clear();
    coresetSerial.clear();
    randTree.clear();
    randSerial.clear();
    
    ticID_iter = tic;
    
    % hack for parallel.
    ctLLH = zeros(1, nChunks);      ctAUC = zeros(1, nChunks);      ctModel = cell(1, nChunks);
    csLLH = zeros(1, nChunks);      csAUC = zeros(1, nChunks);      csModel = cell(1, nChunks);
    rtLLH = zeros(1, nChunks);      rtAUC = zeros(1, nChunks);      rtModel = cell(1, nChunks);
    rsLLH = zeros(1, nChunks);      rsAUC = zeros(1, nChunks);      rsModel = cell(1, nChunks);
    
    %
    Xct = cell(nChunks,1);    wct = cell(nChunks,1);
    Xcs = cell(nChunks,1);    wcs = cell(nChunks,1);
    Xrt = cell(nChunks,1);    wrt = cell(nChunks,1);
    Xrs = cell(nChunks,1);    wrs = cell(nChunks,1);
    
    % Use batch to circumvent the memory limiation.
    for ibatch = 1:nBatches
        
        ticID_batch = tic;
        
        % Only cover a small range of t.
        tlow = batchSize * (ibatch - 1) + 1;
        thigh = tlow + batchSize - 1;

        

        ticID_StorageTime = tic;


        disp(['===== Preparing cell data =====']);

        for t = tlow:thigh

            disp(['===== t = ' int2str(t), ' =====']);

            t_rmd = rem(t, nChunks_real) + 1;

            % Get training and testing sets.
            trainingSet_mat = Feature.unpackFeatureVectors(cTrain_set(:,t_rmd));

            % Store trainingSet_mat into 5 storages.
            [Xct{t}, wct{t}] = coresetTree.addChunkDataTree(trainingSet_mat);
            [Xcs{t}, wcs{t}] = coresetSerial.addChunkDataSerial(trainingSet_mat);
            [Xrt{t}, wrt{t}] = randTree.addChunkDataTree(trainingSet_mat);
            [Xrs{t}, wrs{t}] = randSerial.addChunkDataSerial(trainingSet_mat);
        end


        disp(['Storage time = ', int2str(toc(ticID_StorageTime)), ' seconds.']);

        parfor t = tlow:thigh

            ticID_chunk = tic;

            [ctLLH(1,t),   ctAUC(1,t), ctModel{t}] = AccuracyTestModule.basicAccuracyMetrics(Xct{t}, wct{t}, testSet_mat{t}, TrueLabels{t});
            [csLLH(1,t),   csAUC(1,t), csModel{t}] = AccuracyTestModule.basicAccuracyMetrics(Xcs{t}, wcs{t}, testSet_mat{t}, TrueLabels{t});
            [rtLLH(1,t),   rtAUC(1,t), rtModel{t}] = AccuracyTestModule.basicAccuracyMetrics(Xrt{t}, wrt{t}, testSet_mat{t}, TrueLabels{t});
            [rsLLH(1,t),   rsAUC(1,t), rsModel{t}] = AccuracyTestModule.basicAccuracyMetrics(Xrs{t}, wrs{t}, testSet_mat{t}, TrueLabels{t});
            
            % Display some info.
            disp(['===== t = ' int2str(t), ' =====']);
            
            disp(['Dataset Size: Coreset(Tree)=', int2str(size(Xct{t},2)), ...
                              ', Coreset(Serial)=', int2str(size(Xcs{t},2)), ...
                              ', Random Subset(Tree)=', int2str(size(Xcs{t},2)), ...
                              ', Random Subset(Serial)=', int2str(size(Xcs{t},2))]);
            
            disp(['==> t = ', int2str(t), ' completed in ', int2str(toc(ticID_chunk)), ' seconds.']);
        end
        
        
        disp(['====> batch = ', int2str(ibatch), ' completed in ', int2str(toc(ticID_batch)), ' seconds.']);
        
    end % for ibatch = 1:nBatches
    
    
    
    % Retrieve the results from parallel execution.
    allStats{1}.LLH(iter,:) = ctLLH;    allStats{1}.AUC(iter,:) = ctAUC;    allStats{1}.Models(iter,:) = ctModel;
    allStats{2}.LLH(iter,:) = csLLH;    allStats{2}.AUC(iter,:) = csAUC;    allStats{2}.Models(iter,:) = csModel;
    allStats{3}.LLH(iter,:) = rtLLH;    allStats{3}.AUC(iter,:) = rtAUC;    allStats{3}.Models(iter,:) = rtModel;
    allStats{4}.LLH(iter,:) = rsLLH;    allStats{4}.AUC(iter,:) = rsAUC;    allStats{4}.Models(iter,:) = rsModel;
    
    disp(['====> Iteration = ', int2str(iter), ' completed in ', int2str(toc(ticID_iter)), ' seconds.']);
    
    
    sendmail('daiwei89@gmail.com', 'Mail from MATLAB', ['Iteration ', int2str(iter), ' is done in ', int2str(toc(ticID_iter)), ' seconds.']);
    
end

matlabpool close;


%% Step 2: Compute stats.

for istat = 1 : nStorages
    
    % Compute LLH
    allStats{istat}.meanLLH = mean(allStats{istat}.LLH,1);
    allStats{istat}.stdDevLLH = std(allStats{istat}.LLH,0,1);
    allStats{istat}.stdErrLLH = allStats{istat}.stdDevLLH / sqrt(nIterations);

    % Compute AUC
    allStats{istat}.meanAUC = mean(allStats{istat}.AUC,1);
    allStats{istat}.stdDevAUC = std(allStats{istat}.AUC,0,1);
    allStats{istat}.stdErrAUC = allStats{istat}.stdDevAUC / sqrt(nIterations);
    
end

%% Step 3: Plotting

figure()
%hold all
%nChunks = 786;
coordX = linspace(1,nChunks,nChunks);

for istat = 1 : nStorages
    subplot(2,1,1);
    %errorbar(coordX, allStats{istat}.meanLLH, allStats{istat}.stdErrLLH, '--x', 'LineWidth', 2);
    plot(coordX, allStats{istat}.meanLLH);
    xlabel('hours')
    ylabel('LLH')
    hold all;
    
    subplot(2,1,2);
    %errorbar(coordX, allStats{istat}.meanAUC, allStats{istat}.stdErrAUC, '--x', 'LineWidth', 2);
    plot(coordX, allStats{istat}.meanAUC);
    xlabel('hours')
    ylabel('AUC')
    legend({'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)', 'Full Set'}, 'Location', 'Southeast')
    hold all;
end

    subplot(2,1,1);
    plot(coordX, fullLLH);
    xlabel('hours')
    ylabel('LLH')
    hold all;
    
    subplot(2,1,2);
    plot(coordX, fullAUC);
    xlabel('hours')
    ylabel('AUC')
    legend({'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)', 'Full Set'}, 'Location', 'Southeast')
    hold all;


%%
for istat = 1 : nStorages
    avgLLH = sum(allStats{istat}.meanLLH) / numel(allStats{istat}.meanLLH);
    avgAUC = sum(allStats{istat}.meanAUC) / numel(allStats{istat}.meanAUC);
    avgStdLLH = sum(allStats{istat}.stdDevLLH) / numel(allStats{istat}.stdDevLLH);
    avgStdAUC = sum(allStats{istat}.stdDevAUC) / numel(allStats{istat}.stdDevAUC);
    disp(['istat=', int2str(istat), ', avgLLH=', num2str(avgLLH), ', avgAUC=', num2str(avgAUC), ', avgStdLLH=', num2str(avgStdLLH), ', avgStdAUC=', num2str(avgStdAUC)]);
end
disp(['full dataset, avgLLH=', num2str(mean(fullLLH)), ', avgAUC=', num2str(mean(fullAUC))]);


%clearvars -except 'allStats' 'nChunks' 'nChunks_real' 'nStorages' 'fullLLH' 'fullAUC' 'fullModels', ;

%}