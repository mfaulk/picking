%% Step 0: Setups
import online.*
import feature.*
import parallel.*


%clear;
clc;




if ~exist('cTrain_set','var') || ~exist('cTest_set','var') || ~exist('nChunks','var')
    clear;
    %load('experiments/coreset_vs_onlineEM/preprocessedMAT/7recordings_combined_262hrs');
    load('experiments/coreset_vs_onlineEM/preprocessedMAT/7recordings_262_random');
    %load('experiments/coreset_vs_onlineEM/preprocessedMAT/rjc_10022010.mat');
end

% repeat recordings by increasing nChunks.

cTrain_set = randTrain;
cTest_set = randTest;

nChunks = 262

nChunks_real = nChunks;
nChunks = 3 * nChunks_real;


k = 6;
coresetParams.k = k;
coresetParams.beta = k*2;
nIterations = 3;

allStats.LLH = zeros(nIterations, nChunks);
allStats.AUC = zeros(nIterations, nChunks);

%% Step 1: 

% Test set is the same for all iterations. Do it beforehand.
testSet_mat = cell(nChunks, 1);
TrueLabels = cell(nChunks, 1);



% Main loop.
for iter = 1:nIterations
             
    % Prepare cells of training data
    ctLLH = zeros(1, nChunks);      ctAUC = zeros(1, nChunks);
    Xct = cell(nChunks,1);    wct = cell(nChunks,1);
 
    ticID_StorageTime = tic;


    disp(['===== Preparing cell data =====']);

    test = AccuracyStepTestModule();
    
    for t = 1:nChunks
        disp(['===== t = ' int2str(t), ' =====']);
        % Find the position in our data (since we repeat)
        t_rmd = rem(t, nChunks_real) + 1;
    
        %Prepare testing set
        testSet_mat = feature.Feature.unpackFeatureVectors(cTest_set(:,t_rmd));
        TrueLabels = feature.Feature.unpackLabels(cTest_set(:,t_rmd))';        

        % Prepare training set
        trainingSet_mat = Feature.unpackFeatureVectors(cTrain_set(:,t_rmd));
        Xct =trainingSet_mat;
        wct = ones(1,size(trainingSet_mat,2));
        
        %Calculate the metrics
        [ctLLH(1,t),   ctAUC(1,t)] = test.stepAccuracyMetrics(Xct, wct, testSet_mat, TrueLabels, k);
         
    end
        
    
    
    
    % Retrieve the results
    allStats.LLH(iter,:) = ctLLH;    allStats.AUC(iter,:) = ctAUC;
    
    
end


%% Step 2: Compute stats.

    
% Compute LLH
allStats.meanLLH = mean(allStats.LLH,1);
allStats.stdDevLLH = std(allStats.LLH,0,1);
allStats.stdErrLLH = allStats.stdDevLLH / sqrt(nIterations);

% Compute AUC
allStats.meanAUC = mean(allStats.AUC,1);
allStats.stdDevAUC = std(allStats.AUC,0,1);
allStats.stdErrAUC = allStats.stdDevAUC / sqrt(nIterations);
    
% Remove .LLH and .AUC fields.
%allStats = rmfield(allStats, 'LLH');
%allStats = rmfield(allStats, 'AUC');

%% Step 3: Plotting

figure()
%hold all
%nChunks= 786;
coordX = linspace(1,nChunks,nChunks);

subplot(2,1,1);
plot(coordX, allStats.meanLLH, '--x', 'LineWidth', 2);
xlabel('hours')
ylabel('LLH')
hold all;
    
subplot(2,1,2);
plot(coordX, allStats.meanAUC, '--x', 'LineWidth', 2);
xlabel('hours')
ylabel('AUC')
legend({''})
hold all;



