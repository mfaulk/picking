%% Load up the file
clear;
clc;
close all;

import phone.*;

% Unleash the 8 core CPU power!
import parallel.*;
openPool();

h = waitbar(0,'Initializing waitbar...');
waitbar(0,h,'Loading file...')

acsnFile = 'A/rjc_10022010_3.acsn';
%acsnFile = 'A/mf.tues.acsn';
%acsnDir = 'A';

ticID = tic;
phoneRecord = PhoneRecord(acsnFile);
time_loading = toc(ticID);



%% Break up into feature vectors

import check.*
import feature.*
import phone.*
import sac.*
import gmm.*
import synthetic.*
import parallel.*
import roc.*

%%%%%%%%%%%%%%%%%%%%%%%% Variable to be changed %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Variable to be changed %%%%%%%%%%%%%%%%%%%
chunksize_minute = 5;     % every hour is a chunk

disp(['chunksize = ', int2str(chunksize_minute), '. Please confirm.']);

%model = GmLtStParameterSelection();

%% Segmenting

waitbar(0.25,h,'Segmenting file...')

ticID = tic;

% later, I'm going to make Long-term, short-term features, so I'll make the
% segments long enough to leave room for those computations:
samplesPerSecond = 50; 
ltLength = 5; % seconds
stLength = 2.5; % seconds
deadTime = 1; % seconds of "dead time" between short term and long term buffers
segmentLengthSeconds = ltLength + stLength +deadTime;
slidingSec = 2.5;

% cPhoneSegments is a column cell array of UniformTimeSeries objects
cPhoneSegments = SegmentSynthesizer.segmentPhoneRecord(phoneRecord, segmentLengthSeconds, samplesPerSecond, slidingSec);
%cPhoneSegments = SegmentSynthesizer.segmentPhoneRecord(phoneRecord, segmentLengthSeconds, samplesPerSecond);
time_segmenting = toc(ticID);
clear phoneRecord; % free up memory

disp(['Finished segmenting. Time: ', int2str(time_segmenting), ' seconds.']);

%% Chop cPhoneSegments into chunks of chunksize_minute minutes of data.
% This is only for the 39 mins data 'A/rjc_10022010_3.acsn'. Bigger chunks
% should be considered for 'A/mf.tues.acsn'

ticID = tic;

nSegments = size(cPhoneSegments, 1);


chunksize = floor(chunksize_minute * 60 / 2.5);
nChunks = floor(nSegments / chunksize);
cPhoneSegments = cPhoneSegments(1: (nChunks * chunksize), :);

% cChunks is a [chunksize x nChunks] cell array. (each column is a chunk)
cChunks = reshape(cPhoneSegments, [chunksize nChunks]);
clear cPhoneSegments;

%% Randomly select training set (80% of the chunk, or 96 data) and test set (the remaining 24 data points) from each chunk of 120 data points.

% randomize cChunks along each row (i.e., within each chunk of data)
% The result is in cRandchunks
cRowChunks = cChunks';
m=size(cRowChunks,1);
[dummy J]=sort(rand(size(cRowChunks)),2);
clear dummy;
Ilin = bsxfun(@plus,(J-1)*m,(1:m)');
clear J;
cRandchunks = cRowChunks; % copy cChunks
clear cChunks;
clear cRowChunks;
cRandchunks(:)=cRandchunks(Ilin);
clear Ilin;

cRandchunks = cRandchunks'; % cRandchunks is now [chunksize x nChunks]

% Select the 80% from each chunk as training set; 20% as test set
nTrain_per_chunk = floor(chunksize * 4 / 5);
nTest_per_chunk = chunksize - nTrain_per_chunk;

cTrain_set = cRandchunks(1:nTrain_per_chunk, :);
cTest_set = cRandchunks((nTrain_per_chunk + 1):chunksize, :);
clear cRandchunks;




% Rehsape cTrain_set into a column vector 
%   for GmLtStParameterSelection.computeTrainingLtStFeatures
cTrain_set_column = reshape(cTrain_set, [(nChunks * nTrain_per_chunk) 1]);


time_randomizing = toc(ticID);

%% Preparing Training Dataset

waitbar(0.5,h,'Preparing Training Set...')

ticID = tic;


nFFT = 16;
nMoms = 1;
dimensionsRetained = 16;

% PCA cTrain_set_column and save the PCA parameters
[cTrain_set_column, C, mu, sigma ]= ...
    GmLtStParameterSelection.computeTrainingLtStFeatures(cTrain_set_column, ltLength, stLength, nFFT, nMoms, dimensionsRetained, 2); % 2 is the "normal label"

params.C = C;
params.mu = mu;
params.sigma = sigma;

clear C;
clear mu;
clear sigma;

% reshape cTrainingFeatures to [nchunks x chunksize] cell array.
cTrain_set = reshape(cTrain_set_column,[nTrain_per_chunk nChunks]);
clear cTrain_set_column;
time_pca = toc(ticID);

disp(['Finished training set prep/PCA. Time: ', int2str(time_pca), ' seconds.']);

%% Prepare Test data set

waitbar(0.75,h,'Preparing Test Set...')

ticID = tic;

% create synthetic data on the entire cTest_set. Preserve the no-quake data
% in cTest_noquake_set
cTest_quake_set = cTest_set;
cTest_noquake_set = cTest_set;

% cTest_set will hold 20% synthetic earthquake and 80% noquake data after PCA.
% cTest_set_temp will be cTest_set, but don't erase cTest_set so that we
% can rerun this cell of codes.
cTest_set_temp = cell(nTest_per_chunk * 2, nChunks);
                        
% Create synthetic earthquake with cTest_set and generate corresponding labels
% (see the sac tutorial)

import sac.*


sacDir = 'data/sac/6-8__HN';    % Use large earthquakes
cSacRecords = SacLoader.loadSacRecords(sacDir);

threshold = 0.3;
peakAmplitude = 0; % this isn't used anymore

parfor ichunk = 1:nChunks,

    cTest_quake_set_chunk = cTest_quake_set(:, ichunk);
    cTest_noquake_set_chunk = cTest_noquake_set(:, ichunk);
    
    % Add recorded earthquake to cTest_quake_set_chunk
    cTest_quake_set_chunk = ...
                    synthetic.SegmentSynthesizer.createOnsetSegments(cTest_quake_set_chunk, cSacRecords, ...
                    ltLength+deadTime, stLength, threshold, peakAmplitude, samplesPerSecond);


    % Apply the PCA params to cTest_noquake_set_chunk and
    % cTest_quake_set_chunk. computeTestingLtStFeatures generates
    % LtStFeatures and run PCA on it with the given PCA params.
    cTest_set_temp(:, ichunk) = ...
        gmm.GmLtStParameterSelection.computeTestingLtStFeatures(cTest_noquake_set_chunk, cTest_quake_set_chunk, ...
        ltLength, stLength, nFFT, nMoms, params.C, params.mu, params.sigma, dimensionsRetained);

end
clear cTest_quake_set_chunk;
clear cTest_noquake_set_chunk;
clear cTest_quake_set;
clear cTest_noquake_set;

time_synQuake = toc(ticID);

close(h);

disp(['Finished data set prep (synthesizing earthquake). Time: ', int2str(time_synQuake), ' seconds.']);

%% Finish up

cTest_set = cTest_set_temp;
clear cTest_set_temp;

%% Experiments on cTrain_set and cTest_set are in coreset_vs_onlineEM_exp

            
 