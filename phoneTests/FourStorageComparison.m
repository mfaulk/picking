
%% Step 0: Setups
import online.*
import feature.*
import parallel.*

if ~exist('cTrain_set','var') || ~exist('cTest_set','var') || ~exist('nChunks','var')
    clear;
    load('experiments/coreset_vs_onlineEM/mat_combined');
end

openpool();


k = 6;
coresetParams.k = k;
coresetParams.beta = k*2;
nIterations = 2;

% Initialize 4 storages.
coresetTree = coresetOnline(AbstractOnlineStorage.USE_TREE, coresetParams);
coresetSerial = coresetOnline(AbstractOnlineStorage.USE_SERIAL, coresetParams);
randTree = randSubsetOnline(AbstractOnlineStorage.USE_TREE);
randSerial = randSubsetOnline(AbstractOnlineStorage.USE_SERIAL);
fullStorage = randSubsetOnline(AbstractOnlineStorage.USE_SERIAL, Inf); % Infinite memory size.

% Stats: 1=coresetTree(ct), 2=coresetSerial(cs), 3=randTree(rt),
% 4=randSerial(rs), 5=fullStorage(full)
nStorages = 5; % number of storages.
allStats = cell(nStorages,1);
for istat = 1 : nStorages
    allStats{istat}.LLH = zeros(nIterations, nChunks);
    allStats{istat}.AUC = zeros(nIterations, nChunks);
end

%% Step 1: 

for iter = 1:nIterations
    
    % Clear the storages from previous iteration.
    coresetTree.clear();
    coresetSerial.clear();
    randTree.clear();
    randSerial.clear();
    fullStorage.clear();
    
    ticID_iter = tic;
    
    Xct = cell(nChunks,1);  wct = cell(nChunks,1);
    Xcs = cell(nChunks,1);  wcs = cell(nChunks,1);
    Xrt = cell(nChunks,1);  wrt = cell(nChunks,1);
    Xrs = cell(nChunks,1);  wrs = cell(nChunks,1);
    Xfull = cell(nChunks,1);  wfull = cell(nChunks,1);
    
    for t = 1:nChunks

        disp(['===== t = ' int2str(t), ' =====']);
        ticID_chunk = tic;

        % Get training and testing sets.
        trainingSet_mat = Feature.unpackFeatureVectors(cTrain_set(:,t));
        testSet_mat = feature.Feature.unpackFeatureVectors(cTest_set(:,t));
        TrueLabels = Feature.unpackLabels(cTest_set(:,t))';

        % Store trainingSet_mat into 5 storages.
        [Xct{t}, wct{t}] = coresetTree.addChunkDataTree(trainingSet_mat);
        [Xcs, wcs] = coresetSerial.addChunkDataSerial(trainingSet_mat);
        [Xrt, wrt] = randTree.addChunkDataTree(trainingSet_mat);
        [Xrs, wrs] = randSerial.addChunkDataSerial(trainingSet_mat);
        [Xfull, wfull] = fullStorage.addChunkDataSerial(trainingSet_mat);
        
    end
    
    
    parfor t = 1:nChunks
        
        [allStats{1}.LLH(iter,t), allStats{1}.AUC(iter,t)] = AccuracyTestModule.basicAccuracyMetrics(Xct, wct, testSet_mat, TrueLabels);
        [allStats{2}.LLH(iter,t), allStats{2}.AUC(iter,t)] = AccuracyTestModule.basicAccuracyMetrics(Xcs, wcs, testSet_mat, TrueLabels);
        [allStats{3}.LLH(iter,t), allStats{3}.AUC(iter,t)] = AccuracyTestModule.basicAccuracyMetrics(Xrt, wrt, testSet_mat, TrueLabels);
        [allStats{4}.LLH(iter,t), allStats{4}.AUC(iter,t)] = AccuracyTestModule.basicAccuracyMetrics(Xrs, wrs, testSet_mat, TrueLabels);
        [allStats{5}.LLH(iter,t), allStats{5}.AUC(iter,t)] = AccuracyTestModule.basicAccuracyMetrics(Xfull, wfull, testSet_mat, TrueLabels);

        % Display some info.
        disp(['Dataset Size: Coreset(Tree)=', int2str(size(Xct,2)), ...
                          ', Coreset(Serial)=', int2str(size(Xcs,2)), ...
                          ', Random Subset(Tree)=', int2str(size(Xcs,2)), ...
                          ', Random Subset(Serial)=', int2str(size(Xcs,2)), ...
                          ', Full dataset=', int2str(size(Xcs,2))]);
        disp(['==> t = ', int2str(t), 'completed in ', int2str(toc(ticID_chunk)), ' seconds.']);
    end
    
    disp(['==> Iteration = ', int2str(iter), 'completed in ', int2str(toc(ticID_iter)), ' seconds.']);
    
end

%% Step 2: Compute stats.

for istat = 1 : nStorages
    
    % Compute LLH
    allStats{istat}.meanLLH = mean(allStats{istat}.LLH,1);
    allStats{istat}.stdDevLLH = std(allStats{istat}.LLH,0,1);
    allStats{istat}.stdErrLLH = allStats{istat}.stdDevLLH / sqrt(nIterations);

    % Compute AUC
    allStats{istat}.meanAUC = mean(allStats{istat}.AUC,1);
    allStats{istat}.stdDevAUC = std(allStats{istat}.AUC,0,1);
    allStats{istat}.stdErrAUC = allStats{istat}.stdDevAUC / sqrt(nIterations);
    
    % Remove .LLH and .AUC fields.
    %allStats{istat} = rmfield(allStats{istat}, 'LLH');
    %allStats{istat} = rmfield(allStats{istat}, 'AUC');
end

%% Step 3: Plotting

figure()
%hold all
coordX = linspace(1,nChunks,nChunks);

for istat = 1 : nStorages
    subplot(2,1,1);
    errorbar(coordX, allStats{istat}.meanLLH, allStats{istat}.stdErrLLH, '--x', 'LineWidth', 2);
    xlabel('hours')
    ylabel('LLH')
    hold all;
    
    subplot(2,1,2);
    errorbar(coordX, allStats{istat}.meanAUC, allStats{istat}.stdErrAUC, '--x', 'LineWidth', 2);
    xlabel('hours')
    ylabel('AUC')
    legend({'Coreset(Tree)', 'Coreset(Serial)', 'Random Subset(Tree)', 'Random Subset(Serial)', 'Full Set'}, 'Location', 'Southeast')
    hold all;
end



