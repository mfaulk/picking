classdef coresetOnline < online.AbstractTreeStorage & online.AbstractSerialStorage
%{

Descriptions:
    This class compress data by constructing coresets. See Danny 
    Feldman's NIPS 2011 paper for details.
    
Author: Dai Wei (2011)
    
%}
    
    properties
        
        coreParams      % Struct containing parameters of coreset 
                        % construction: 
                        %   1.coreParams.k (scalar, number of Gaussians). 
                        %   2.coreParam.beta (scalar, usually 2*k).
                        %
                        %   coreParams.coresetSize (scalar, number of 
                        %       data points in the coreset sample) is no
                        %       longer needed.
                                      
    end
    
    
    
    % ====================================================================

    
    
    methods
        
        function co = coresetOnline(useTree, coreParams, memorySize)
            %{
                Constructor.
            
            Inputs:
                coreParams - see docs.
                useTree - onlineStorage.USE_TREE or
                          onlineStorage.USE_SERIAL.
                memory (optional) - scalar. Total number of data points allowed in
                         storage. Default is 40000 because 2000 * 20 layers
                         in the tree.
            
            %}
            
            import online.*
            
            if ~exist('memorySize', 'var')
                memorySize = 40000;
            end
            
            % Give 0 memory to the storage super class that we don't use.
            memorySizeTree = 0;
            memorySizeSerial = 0;
            
            if useTree == AbstractOnlineStorage.USE_TREE
                memorySizeTree = memorySize;
            else
                memorySizeSerial = memorySize;
            end
            
            co@online.AbstractTreeStorage(memorySizeTree);
            
            % Use deval = 1 default for now.
            co@online.AbstractSerialStorage(memorySizeSerial);
            
            co.coreParams = coreParams;
        end
        
        
        % ----------------------------------------------------------------
        
        function compressedSet = compressSet(obj, D, compressedSize)
        
            import coreset_wrapper.*;
            
            Ndata = size(D.X, 2);
            if Ndata <= compressedSize
                compressedSet = D;
                return;
            end
            
            % Try a few large sampling size.
            sampleSizes = [16; 10; 8; 4; 2; 1] .* compressedSize;
            
            for isize = 1 : size(sampleSizes, 1)
                
                sampleSize = sampleSizes(isize);
            
                [compressedSet.X, compressedSet.w] = ... 
                    CoresetUtils.computeCoresetKMedian(D.X, ...
                        sampleSize, obj.coreParams.k, ...
                        obj.coreParams.beta, D.w);
                
                rsultSize = size(compressedSet.X, 2);
                
                if rsultSize <= compressedSize
                    break;
                end
            end
                
            % Normalize the weights, since we may use different
            % sampleSize.
            compressedSet.w = bsxfun(@rdivide, compressedSet.w, sum(compressedSet.w));
        end        
        
        % ----------------------------------------------------------------
        
        
        function clear(obj)
        %{
            Clear the necessary fields.
        %}
            clear@online.AbstractTreeStorage(obj);
            clear@online.AbstractSerialStorage(obj);
           
        end
        
        
        %{
        function mergedSet = mergeSets(obj, S1, S2, compressedSize, wratio)
            %{
            
            Descriptions:
                Merge and compress coresets A and B to merged. A, B, merged
                are all of Coreset Struct defined in the beginning.
            
            Inputs:
                S1, S2 - [d x n1], [d x n2] data matrix.
                wratio - scalar (optional). weight for S1 relative to S2.
                         Default to 1.
            
            Outputs:
                merged - coreset datatype. See definitions.
            
            %}
            
            import coreset_wrapper.*;
            
            if ~exist('wratio','var')
                wratio = 1;
            end
            
            % Normalize the weights for S1, S2.
            normWeightS1 = bsxfun(@rdivide, S1.w, sum(S1.w));
            normWeightS2 = bsxfun(@rdivide, S2.w, sum(S2.w));
            
            % Apply wratio
            normWeightS1 = normWeightS1.*wratio;
            
            % Convert it into PointFunctionSet
            C1 = CoresetUtils.Xw2PointFunctionSet(S1.X, normWeightS1);
            C2 = CoresetUtils.Xw2PointFunctionSet(S2.X, normWeightS2);
            
            [mergedSet.X, mergedSet.w] = CoresetUtils.mergeCoresets(C1, C2, ...
                compressedSize, obj.coreParams.k, obj.coreParams.beta);
        
        end
            
        % ----------------------------------------------------------------
        %}
    end
    
end

