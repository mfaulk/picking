classdef AbstractTreeStorage < online.AbstractOnlineStorage
%{ 

Descriptions:
    This class represents objects that store datasets in a (balanced) tree.
    The object receives a streams a "data chunks," which can be data
    recorded in, say, 1 hours. Upon receiving the data, it compresses it
    with the abstract method inherited from AbstractOnlineStorage. The
    object may receive up to nChunksMax chunks of data.
    
    Furthermore, the object keeps a column cell array (treeLayers) 
    of length depth. Entry i of the array is the
    dataset at level i (the dataset computed from a raw data is level 1), 
    which is obtained by merging two dataset on level (i-1). 
    

    
Definitions:
    Coreset datatype - FunctionPointSet.
    
Note that this class is abstract because it does not supply implementation
for abstract functions (compress and merge) in AbstractOnlineStorage class.
    
    
Author: Dai Wei (2011)
    
%}
    
    properties %(Access = private)
        
        nChunksMax      % Scalar. The maximum number of data chunks it can 
                        % received. Initialized in constructor.  
                        
        depth = 10      % number of depths to keep. 2^14 = 16384 hours = 1.87 years.
        
        treeLayers      % [depth x 1] column cell array. Cell of coresets. 
                        % The coreset computed from raw 
                        % data is at depth 1; the coreset computed by
                        % merging two coresets at depth 1 is at depth 2,
                        % etc. 'null' means that there is no coreset at 
                        % that layer
                        
        setSize         % scalar. # of data points in one set at a treeLayer.
     
    end
    
    
    % ====================================================================
    
    methods
        
        % ----------------------------------------------------------------
        
        function ts = AbstractTreeStorage(memorySize)
            %{
            
            Descriptions:
                Constructor.        
            
            Inputs: 
                nChunksMax - See properties above.
               
            %}
            
            if nargin == 0
                memorySize = 0;
            end
            
            if memorySize > 0
                ts.nChunksMax = 2 ^ ts.depth - 1;
                ts.setSize = floor(memorySize / ts.depth);
                ts.treeLayers = cell(ts.depth, 1);
            else
                ts.nChunksMax = 0;
            end
        end
        
        % ----------------------------------------------------------------
        
        function [X, w, time] = addChunkDataTree(obj, dataset)
            %{
            
            Descriptions:
                Compress dataset and add it to treeLayers using 
                the helper function.
            
            Inputs:
                dataset - [d x n] matrix. That is, n data points in d
                          dimensions.
            
            Outputs:
                X, w - The X-w representation of obj.currentSet. See 
                      docs in AbstractOnlineStorage.
                time - scalar. Execution time.
            
            %}
            
            if obj.nChunksMax == 0
                error('No memory for tree storage.');
            end
            
            assert(obj.nChunks <= obj.nChunksMax, 'exceeds capacity.');
            obj.nChunks = obj.nChunks + 1;

            % Dimension initialization / check
            if obj.Ndim == 0
                obj.Ndim = size(dataset,1);
            elseif obj.Ndim ~= size(dataset,1)
                error('Input data has different dimensions.');
            end
            
            ticID = tic;
            
            D.X = dataset;
            D.w = ones(1,size(dataset,2));
            
            % Note that if setSize > size(dataset, 2), then this line
            % will not do anything.
            compressedSet = obj.compressSet(D, obj.setSize);

            % The compressed set computed on a raw dataset is at depth 1.
            obj.addDataHelper(compressedSet, 1);
            
            % Compute currentSet by merging and compressing everything
            % from treeLayers
            obj.currentSet = [];
            for idepth = 1:size(obj.treeLayers, 1)
                obj.currentSet = obj.mergeSets(...
                        obj.currentSet, obj.treeLayers{idepth}, obj.setSize);
            end
            
            
            X = obj.currentSet.X;
            w = obj.currentSet.w;
            
            time = toc(ticID);
        end
        
        
        % ----------------------------------------------------------------
        
        
        function clear(obj)
        %{
            Clear the necessary fields.
        %}
            if obj.nChunksMax > 0
                clear@online.AbstractOnlineStorage(obj);
                obj.treeLayers = cell(obj.depth, 1);
            end            
        end
        
        % ----------------------------------------------------------------
        
    end
        
    
    
    
    % ====================================================================
    
    
    
    methods (Access = private)
        
        function addDataHelper(obj, set_to_add, depth)
            %{
            
            Descriptions:
                This is a private function that helps adding a set (set_to_add) to 
                treeLayers recursivly. If treeLayers{depth} is
                occupied, then merge set_to_add with treeLayers{depth} and
                call addDataHelper with (depth + 1).
            
            Inputs:
                set_to_add - struct with .X and .w fields. It's the
                             dataset pending to be added to treeLayers.
            
                depth - scalar. depth is the index for treeLayers cell
                               array. In recursion it's incremented until
                               we find treeLayers(depth, 1) empty.]
            
            Outputs:
                none.
            
            %}
            
            
            if isempty(obj.treeLayers{depth})
                % Base case.
                % There's no dataset at depth index. Just add it.
                obj.treeLayers{depth} = set_to_add;
            else
                % Recursve case.
                % Merge set_to_add with treeLayers{depth}
                mergedSet = obj.mergeSets(set_to_add, ...
                                    obj.treeLayers{depth}, obj.setSize);
                
                % Remove the dataset that has been merged
                obj.treeLayers{depth} = [];
                
                addDataHelper(obj, mergedSet, depth + 1);
            end
        end
        
        % ----------------------------------------------------------------
    end
        

    
    
end