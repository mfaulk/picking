classdef AbstractSerialStorage < online.AbstractOnlineStorage
%{ 

Descriptions:

    
Definitions:
    Coreset datatype - FunctionPointSet.
    
    
Note that this class is abstract because it does not supply implementation
for abstract functions (compress and merge) in AbstractOnlineStorage class.
    
Author: Dai Wei (2011)
    
%}
    
    properties (Access = private)
        
        memorySize      % scalar. This is the allocated size for this 
                        % storage and we require memory > 0 to ensure 
                        % the storage is in enabled.
                        
    end
    
    
    % ====================================================================
    
    methods
        
        % ----------------------------------------------------------------
        
        function ss = AbstractSerialStorage(memorySize)
            %{
            
            Descriptions:
                Constructor.        
            
            Inputs: 
                memorySize - See properties above.
               
            %}
            
            if ~exist('memorySize', 'var')
                memorySize = 0;
            end
            
            ss.memorySize = memorySize;
        end
                
        % ----------------------------------------------------------------
        
        
        function [X, w, time] = addChunkDataSerial(obj, dataset)
            %{
            
            Descriptions:
                Construct the coreset and add it to treeLayers using 
                the helper function.
            
            Inputs:
                dataset - [d x n] matrix. That is, n data points in d
                          dimensions.
            
            Outputs:
                X, w - The X-w representation of the current coreset.
            
            %}
            
            if obj.memorySize == 0
                error('No memory for serial storage.');
            end
            
            obj.nChunks = obj.nChunks + 1;
            
            % Dimension initialization / check
            if obj.Ndim == 0
                obj.Ndim = size(dataset,1);
            elseif obj.Ndim ~= size(dataset,1)
                error('Input data has different dimensions.');
            end
            
            ticID = tic;
            
            % Don't compress if there's enough room for the entire dataset.
            % Otherwise compress only the existing data in memory.
            
            Ndata = size(dataset,2);
            
            D.X = dataset;
            D.w = ones(1,Ndata);
            
            obj.currentSet = obj.mergeSets(obj.currentSet, D, obj.memorySize);

            X = obj.currentSet.X;
            w = obj.currentSet.w;
            
            time = toc(ticID);
        end
        
        
        % ----------------------------------------------------------------
        
        
        function clear(obj)
        %{
            Clear the necessary fields.
        %}
            if obj.memorySize > 0
                clear@online.AbstractOnlineStorage(obj);
            end            
        end
        
        
        % ----------------------------------------------------------------
        
    end        
        
end