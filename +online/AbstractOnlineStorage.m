classdef AbstractOnlineStorage < handle
%{

Descriptions:
    Provide very basic fields that both tree storage and iterative
    serial storage need.
    
Author: Dai Wei (2011)
    
%}
    % ====================================================================
    
    properties (Constant)
        USE_TREE = 1;
        USE_SERIAL = 2;
    end
    
    % ====================================================================
    
    properties (Access = protected)
    %{
    
        These are fields shared by both storage. protected access to allow
        subclass to directly access it.
        
    %}
                        
        nChunks         % scalar. Number of chunks of data we have  
                        % collected together so far.
                        

        
        
        currentSet      % currentSet.X = [d x n], currentSet.w = [1 x n]
                        % The compressed set computed based on the data
                        % given so far. For serialStorage, its size could
                        % be as large as memory. For tree storage, it is an
                        % "incorrect" merge of all sets at all layer of the
                        % tree. Note that currentSet is only temporary, and
                        % it won't affect the storage array.
                        
                        
        Ndim = 0        % scalar. dimension of the dataset. Need to be consistent.
                        % initialized when the first chunk is added.
        
    end
    
    % ====================================================================
    
    methods
        
        function aos = AbstractOnlineStorage()
            aos.clear();
        end
        
        
        % ----------------------------------------------------------------
        

        function [X, w] = getCurrentSet(obj)
            %{
            
            Descriptions:
                Getter for the current coreset.
            
            %}
            
            if obj.nChunks == 0
                error('You have not given me any data yet!');
            end
            
            X = obj.currentSet.X;
            w = obj.currentSet.w;
            
        end
        
        
        % ----------------------------------------------------------------
        
        
        function mergedSet = mergeSets(obj, S1, S2, finalSize)
        %{
            Descriptions:
                Simple merge by concatenation.
            
            mergedSet - struct. mergedSet.X and mergedSet.w
        %}
        
            if isempty(S1)
                mergedSet = S2;
                return;
            elseif isempty(S2)
                mergedSet = S1;
                return;
            end
            
            if size(S1.X, 1) ~= size(S2.X, 1) && ...
                    size(S1.X, 1) ~= 0 && size(S2.X, 1) ~= 0
                error('merging data of different dimensions.');
            end
            mergedSet.X = [S1.X, S2.X];
            mergedSet.w = [S1.w, S2.w];
            
            % Ensure that the merged set is within the size limit.
            if size(mergedSet.X, 2) > finalSize
                mergedSet = obj.compressSet(mergedSet, finalSize);
            end
            
        end
        
        % ----------------------------------------------------------------
        
        
        function clear(obj)
        %{
            Clear all the fields.
        %}
            obj.Ndim = 0;
            obj.nChunks = 0;
            obj.currentSet.X = [];
            obj.currentSet.w = [];
        end
        
        % ----------------------------------------------------------------
        
    end
    
    
    % ====================================================================
    
    
    
    methods (Abstract)
        %{
            D - D.X and D.w matrix representing the dataset.
            compressedSet - struct. compressedSet.X and compressedSet.w
        %}
        compressedSet = compressSet(obj, D, compressedSize)  
        
    end
    
end

