classdef randSubsetOnline < online.AbstractTreeStorage & online.AbstractSerialStorage
%{

Descriptions:
    This class compress data by taking random sampling.
    
Author: Dai Wei (2011)
    
%}
    
    properties
                                      
    end
    
    
    
    % ====================================================================

    
    
    methods
        
        function rso = randSubsetOnline(useTree, memorySize)
            %{
                Constructor.
            
            Inputs:
                deval - scalar. see properties.
                useTree - onlineStorage.USE_TREE or
                          onlineStorage.USE_SERIAL.
                memory (optional) - scalar. Total number of data points allowed in
                         storage. Default is 40000 because 2000 * 20 layers
                         in the tree (AbstractTreeStorage).
            
            %}
            
            import online.*
            
            if ~exist('memorySize', 'var')
                memorySize = 40000;
            end
            
            % Give 0 memory to the storage super class that we don't use.
            memorySizeTree = 0;
            memorySizeSerial = 0;
            
            if useTree == AbstractOnlineStorage.USE_TREE
                memorySizeTree = memorySize;
            else
                memorySizeSerial = memorySize;
            end
            
            rso@online.AbstractTreeStorage(memorySizeTree);
            rso@online.AbstractSerialStorage(memorySizeSerial);
            
        end
        
        
        % ----------------------------------------------------------------
        
        function compressedSet = compressSet(obj, D, compressedSize)
        
            
            Ndata = size(D.X, 2);
            
            if Ndata < compressedSize
                compressedSet = D;
                return;
            end
            
            % Subsample
            ind = randperm(Ndata);
            ind = ind(1:compressedSize);
            
            compressedSet.X = D.X(:,ind);
            compressedSet.w = D.w(:,ind);
        end        
        
        % ----------------------------------------------------------------
        
        
        function clear(obj)
        %{
            Clear the necessary fields.
        %}
            clear@online.AbstractTreeStorage(obj);
            clear@online.AbstractSerialStorage(obj);
           
        end
        
        
        % ----------------------------------------------------------------
        
        %{
        function mergedSet = mergeSets(obj, S1, S2)
            %{
            
            Descriptions:
                Merge and compress coresets A and B to merged. A, B, merged
                are all of Coreset Struct defined in the beginning.
            
            Inputs:
                S1, S2 - [d x n1], [d x n2] data matrix.
                wratio - scalar (optional). weight for S1 relative to S2.
                         Default to 1.
            
            Outputs:
                merged - coreset datatype. See definitions.
            
            %}
            
            import coreset_wrapper.*;
            
            if ~exist('wratio','var')
                wratio = 1;
            end
            
            % Weights are always 1.
            
            
            % Merge by concatenation.
            obj.NcurrentSet = obj.NcurrentSet + Ndata;
            obj.currentSet.X = [obj.current.X, dataset];
            obj.currentSet.w = [obj.current.w, ones(1,Ndata)];
        
        end
            
        % ----------------------------------------------------------------
        %}
    end
    
end

