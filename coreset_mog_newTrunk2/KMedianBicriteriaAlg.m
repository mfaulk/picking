classdef KMedianBicriteriaAlg < AbstractBicriteriaAlg    
	% Entry point class called by the coreset algorithms
	
    methods
        
        %function [center vals indexes] = robust(obj, F, gamma, epsilon, beta)
        function robustClusters = robust(obj, F, gamma, beta, nIteration)
            %this has to change - couldn't implement overloading normally
            if isa(obj.robustAlg, 'AbstractPtasAlg')
                %[center, vals, indexes] 
                robustClusters = obj.robustAlg.compute(F, epsilon, obj.delta/log(obj.originalFsize), beta, nIteration);
            else
                %[center, vals, indexes] 
                robustClusters = obj.robustAlg.compute(F, gamma, beta, nIteration);
            end
        end
    end
end % class