classdef Excel < handle
    % Writing data to Excel file.
    % Initialize file using constructor or openFile().
    % Add rows using addRows() function.
    % When number of rows exceeds rowsCacheSize, then the ram is written
    % (flush) to the file.
    % Flush will also be called when the number of columns in the input
    % of a call to addRows() is different the the previous rows.
    % Call closeFile() when finish, in order to flush last rows.
    % Call flush to force writing to disk
    % You can switch/add current file/sheet/row using the Set..() functions
    % Use .xls in file names. You can also specify path.
    % In current version, rows in memory must be of the same size each.
    % Otherwise, use flush() between matrices of different column numbers
    
    % Example:
    % report=Excel('myfile.xls', 'mysheet'); % use current directory
    % report.addRows ({'Header A', 'Header B'});
    % report.flush
    % report.addRows([2 3; 4 5]);
    % report.addRows([6 7]);
    % report.flush
    % report.addRows([8 9 10]);
    % report.closeFile();
    properties
        % current workfile name . Type=string
        fileName;
        
        % current sheet name in excel file
        sheetName;
        
        % current row in the sheet
        nRow;

        %  fulsh data to file when the number of rows exceeds rowsCacheSize
        % 
        rowsCacheSize=1;
        
        % data to write as excel rows
        rowsCache;
    end % properties
    
    methods

        % constructor. See openFile.
        function obj=Excel(varargin)
            obj.openFile(varargin);
            obj.initCache();
        end % constructor
        
        % set current place to write data. See defaults values in code
        % below.
        % fileName - string, include .xls and path. 
        % sheetName - name of sheet to use (add if not exists)
        % toDeleteExistingFile - delete file if exists
        % toDeleteExistingSheet - same for sheet
        % nRow - current row to write in spreadsheet (first=1)
        % rows - what to write (matrix)
        % rowsCacheSize - when to write to disk (how much memory to use)
        function openFile(obj, varargin)
                    [fileName, sheetName, toDeleteExistingFile, toDeleteExistingSheet, nRow, rows] = ...
                        Utils.complete({'temp.xls', 'Sheet1', 1,1,1,[]},varargin);
                    obj.setFileName(fileName, toDeleteExistingFile);
                    obj.setSheetName(sheetName);
                    obj.setRowNumber (nRow);
                    if not(isempty(rows))
                        obj.addRows(rows);
                    end
                    
        end % function setAll
        
        % set current file. Default sheetname is 
        % filename - string. 
        % toDeleteExistingFile - boolean. default =1 (yes).
        function setFileName(obj, fileName, toDeleteExistingFile)
            if ~exist('toDeleteExistingFile','var')
                toDeleteExistingFile = true;
            end % if toDelete...
            obj.fileName = fileName;
            if toDeleteExistingFile & exist(fileName,'file')
                  delete(fileName);
            end % if 
        end % function setFileName
        
        % set current sheetname. 
        % sheetName - string. 
        function setSheetName(obj, sheetName)
            obj.sheetName=sheetName;
        end % function setSheet
        
        % first row (header) is one.
        function setRowNumber(obj, nRow)
            obj.nRow=nRow;
        end % function setRow

        % Add rows to current excel sheet. 
        % rows are matrix that should be the same as previous matrices in
        % cache.
        % The data will be written to disk only after calling to flush, 
        % or closing file, or writing more current data exceeds dataSize
        function addRows(obj, rows)
            if strcmp(class(rows),'double')
                rows=mat2cell(rows,ones(1,size(rows,1)),ones(1,size(rows,2)));
            end
            %l=size(obj.rowsCache);
            if length(obj.rowsCache)>0  && size(rows,2)~= size(obj.rowsCache,2)
                obj.flush();
            end
            obj.rowsCache=[obj.rowsCache ;rows];
            if size(obj.rowsCache,1)>=obj.rowsCacheSize
                obj.flush();
            end % if size..
        end % function addRows
        

        % write data to file
        function flush(obj)
             dataPlacement=['A' num2str(obj.nRow)];
             warning off MATLAB:xlswrite:AddSheet;
                obj.setReadOnly(false);
             [status, message]=xlswrite(obj.fileName,obj.rowsCache, obj.sheetName, dataPlacement);
                 obj.setReadOnly(true);
                     
             if status ~=1 % problem
                warning(message.message)
             else
                 obj.setRowNumber(obj.nRow+size(obj.rowsCache,1));
                 obj.initCache;
             end % if 
        end % flush
        
    function initCache(obj)
        obj.rowsCache={};
    end
        % write data to file
        function closeFile(obj)
             if not(isempty(obj.rowsCache))
                 obj.flush();
             end
             obj.setReadOnly(true);
        end % closeFile
        function setReadOnly(obj, on)
             if exist(obj.fileName, 'file')
                    if on
                        fileattrib(obj.fileName,'-w');
                    else
                        fileattrib(obj.fileName,'+w');
                    end % if on
             end % if exists
        end % setReadOnly
    end % methods

    methods (Static)
        function example();
            report=Excel('c:\temp\myfile.xls', 'mysheet'); % use current directory
            report.addRows ({'Header A', 'Header B'});
            report.addRows ({'Header C', 'Header D'});
            report.addRows ([4 5 6]);
            report.flush;
            report.addRows([2 3; 4 5]);
            report.addRows([6 7]);
            report.flush
            report.addRows([10 11]);
            report.addRows([8 9]);
            report.closeFile();
        end % example
    end % Static Methods
end % class
    

