classdef AbstractPtasAlg < AbstractRobustAlg
    %UNTITLED5 Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = AbstractPtasAlg()
			% gamma here is an alias of medianFraction defined in class AbstractBicriteriaAlg
			% Since in ptas algorithm we consider the whole set of points instead of a fraction of it. gamma = 1.
            obj.gamma = 1;
        end
        
        %function [mincenter vals indexes mincost]=compute (obj, F, eps, delta, beta)
        function robustClusters = compute(obj, F, beta, nIteration, varargin)
            %[mincenter vals indexes mincost]
            robustClusters = compute@AbstractRobustAlg(obj, F, obj.gamma, beta, nIteration);
        end
    end
    
end