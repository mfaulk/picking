classdef BicriteriaConstantDeltaAlg < AbstractBicriteriaAlg
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = BicriteriaConstantDeltaAlg(varargin)
                obj = obj@AbstractBicriteriaAlg(varargin{:});
        end
        
        function center=robust(obj,F, epsilon, beta,nIteration)
            %this has to change - couldn't implement overloading normally
            if isa(obj.robustAlg,'AbstractPtasAlg')
                center = obj.robustAlg.compute(F,beta,nIteration,epsilon,obj.delta/log(obj.originalFsize));
            else
                center = obj.robustAlg.compute(F,gamma,epsilon,obj.delta/log (obj.originalFsize), beta);
            end
        end
    end
end