classdef Exception < MException
    properties
        mystack; % since MException.stack is readonly
    end
    
    methods
        function obj=Exception (exception)
            obj = obj@MException(exception.identifier, exception.message);
            obj.mystack=exception.stack;
%           obj.exception=exception;
        end
        function extendedMsg=print(obj)
            %stack2msg=obj.stack
            %msg1=sprintf('%s \n',obj.message);
            f={obj.mystack.file};
            files=sprintf('%s\n', f{:});
            s={obj.mystack.line};
            lines=sprintf('%d,',s{:});
            msg=sprintf('%s\n', obj.message);
            extendedMsg=[msg 'Files: ' files 'Lines:' lines];
        end
    end
    methods (Static)
        function report=example2()
            try
                Exception.example;
                report='';
            catch ex
                e=Exception(ex);
                report=e.print();
            end
        end
        function example()
                df
        end
    end
    
end

