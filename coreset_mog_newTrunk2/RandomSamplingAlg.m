classdef RandomSamplingAlg
    %RANDSAMWITHREPETITION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function [S, randIndexes] = compute(obj,M,sampleSize,repetition)
            if repetition == 0
                [S, randIndexes] = obj.computeWithoutRepetition(M,sampleSize);
            else
                [S, randIndexes] = obj.computeWithRepetition(M,sampleSize);
            end
        end
        
        function [S, randIndexes] = computeWithoutRepetition(obj,M,sampleSize)
            randIndexes = randsample(size(M,1),sampleSize);
            S = M(randIndexes,:);
        end
        
        function [S, randIndexes] = computeWithRepetition(obj,M,sampleSize)
            randIndexes = randsample(size(M,1),sampleSize,true);
            S = M(randIndexes,:);
%              S=M(:,ceil(rand(1,sampleSize)*size(M,2)));
        end
    end
    
end

