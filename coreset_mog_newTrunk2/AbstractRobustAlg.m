classdef AbstractRobustAlg < AbstractUsingCoresetAlg
%--------------------------------------------------------------------------
%This class calculates the robust median of a given set of points (with
%weights). The "compute" function is the main function which fulfils the
%function.
%
%The basic idea of the current implementation is very simple. In every
%iteration it randomly samples \beta (given as a parameter) points as the
%centers (robust median) with probability proportional to their respective
%weights. Then for every point, it calculates the distance between the
%point and its nearest center among the \beta centers. Then it sums up all
%the distances (with outliers uncounted). If it obtains smaller value, this
%set of centers will replace the previously found set of centers.
%--------------------------------------------------------------------------

    properties (SetAccess = protected)
        % median fraction. Alias to medianFraction defined in AbstractBicriteriaAlg
         gamma
        
        % Type = AbstractEpsApprox
		% Set by default
        % In the current implementation, it is unused.
        epsApproxAlg = RandomEpsApproxAlg(1);
    end
    
    methods
        % Constructor
        function obj = AbstractRobustAlg()
		    % It is meaningless to let gamma = 0. The only purpose here is
		    % to define gamma as a number instead of a matrix or anything
		    % else. Can be ignored.
            obj.gamma = 0;
        end
        
        % compute \beta (gamma,eps)-robust median with probability at least 1-delta
        % In this function, we hide our implementations in
        % AbstractBicriteriaAlg.m where we keep original F the same and
        % only manipulate currentIndexes. From this class' point of view, its
        % job is still to compute the robust median given a specific point
        % set "F".
        function robustClusters = compute(obj, F, gamma,  beta, nIteration, varargin)
            % Initialize the number of centers we are going to sample.
            % This is mainly to deal with the case where F.size < \beta. 
            nCenters=min(beta, F.size);

            mincost = inf;           

             if (gamma==1)
                mIndexes = 1:F.size;
             end

            for i=1:nIteration
				% Generate a sample set of points with size nCenters from F as the robust medians.
                center = obj.computeWithoutCoreset(F, nCenters); 
				
				% Calculate the minimum sum of distances of points in F to the nearest points in center.
                [tempDistances, tempCentersIndexes] = F.Eval(center);

                % If gamma < 1, we will only take the gamma fraction of
                % points into account. The rest part is taken as outliers.
                if (gamma < 1)
                     % Figure out m such that the sum of weights of index
                     % Indexes[1...m] is equal to ceil(F.sumW * gamma));
                     mIndexes = OrderStatisticBySortAlg.compute(tempDistances, F.sumW * gamma, F.W.matrix);
                     cost = sum(tempDistances(mIndexes));
                else
                   cost = sum(tempDistances);
                end
                 
                 if or(i==1,cost<mincost)
                     optDistances = tempDistances(mIndexes);
                     optCentersIndexes = tempCentersIndexes(mIndexes);
                     optCenters = center; %center.M.m;
                     optIndexes = mIndexes;
                     mincost=cost;				
                 end % if 
            end % for
            
            % create the vector
            robustClusters = ClusterVector();
            robustClusters.initWithFunctionSet(F, optIndexes, optDistances, optCenters, optCentersIndexes);
        end % function robustCluster
    end

    methods % may be overridden by subclasses
        % currently we only use the simplest randomized algorithm: Return a
        % subset of points in G with size sampleSize as the medians. The points are
        % sampled according to their weights in G.
        function center = computeWithoutCoreset(obj, G, sampleSize)
            % Initialize a non-uniform sampler
            nonUniformSampler = NonUniformSamplingAlg(sampleSize);
            [~,indexes] = nonUniformSampler.sample(G.W.matrix);
            center = G.subset(indexes);
        end
    end
end