classdef BicriteriaDecreasingDeltaAlg < AbstractBicriteriaAlg
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
         function obj = BicriteriaDecreasingDeltaAlg(delta, beta, varargin)
                obj = obj@AbstractBicriteriaAlg(delta, beta, varargin);
         end
        
        function center=robust(obj,F, gamma, epsilon, beta)
            %this has to change - couldn't implement overloading normally
            if isa(obj.robustAlg,'AbstractPtasAlg')
               center =obj.robustAlg.compute(F,epsilon,(obj.delta/2)^obj.nIteration, beta);
            else
                center =obj.robustAlg.compute(F,gamma,epsilon,(obj.delta/2)^obj.nIteration, beta);
            end
        end
    end
end

