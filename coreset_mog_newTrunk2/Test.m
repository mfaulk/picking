classdef Test < handle
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
%        testFields;
        testFields={};
        reportFields;
        IsTextReport=true;
        toExcel=false;  % Dai Wei
        fileName='c:\temp\report.xls';
        excel;
        ExcelReport;
        sheetName = 'TestsResult';
        currentConfig=0;
        configs;
    end % properties

    methods
        function vec=getReportVec(obj, fieldName, vecSize)
            vec=cell(1,vecSize);
            for i=1:vecSize
                vec{i}=[fieldName '(' num2str(i) ')']; 
            end % for
        end % setReportField

        
        function result=getReportFields(obj)
            result={};
        end
        function obj=setTestFields (obj,varargin)
            obj.testFields=[obj.testFields  varargin];
        end % setFieldValue

       function textReport=runCartesianProduct(obj)
                    %testArray = cartesianProdArray (obj);
                    textReport=obj.runTests();
        end % runCartesianProduct

%         function testArray=cartesianProdArray (obj)
%             obj.configs=obj.makeCartesianProduct();
%             testArray=obj.testParams2Array (obj.configs);
%         end % cartesianProdArray

%          function testArray=testParams2Array(obj, configs)
%               arraySize=0;
%               testArray=cell(1,configs.nCols);
%               for c=configs.m
%                 test=obj;
%                 for fieldNo=1:length(obj.testFields)/2 
%                     fieldName=obj.testFields{2*fieldNo-1};
% %                    fieldVec=obj.(fieldName);
%                     fieldVec=obj.testFields{2*fieldNo};
%                     fieldValue=fieldVec(c(fieldNo));
%                     test=Utils.setFieldValue(test, fieldName, fieldValue);
%                 end % for fieldName
%                 arraySize=arraySize+1;
%                 testArray{arraySize}=test;
%               end % for c
%             end % function testParams

         function obj=initTest(obj)
             obj.currentConfig=0;
             obj.configs=obj.makeCartesianProduct();
         end
         function [obj test]=nextTest(obj)
             current =obj.currentConfig;
             if current < obj.configs.nCols
                current=current+1;
                obj.currentConfig=current;
                c=obj.configs.m(:,current);
                test=obj;
                for fieldNo=1:length(obj.testFields)/2 
                    fieldName=obj.testFields{2*fieldNo-1};
%                    fieldVec=obj.(fieldName);
                    fieldVec=obj.testFields{2*fieldNo};
                    fieldValue=fieldVec(c(fieldNo));
                    test=Utils.setFieldValue(test, fieldName, fieldValue);
                end % for fieldName
             else
                 test=[];
             end % if
          end % function nextTest

          function configs=makeCartesianProduct(obj)
            nFields=length(obj.testFields)/2;
            sizeCell=cell(1,nFields);
            for (fieldNo=1:nFields)
               valuesVec=obj.testFields{2*fieldNo};
               sizeCell{fieldNo}=1:length(valuesVec);
            end
            configs=Utils.setProd(sizeCell)';   
          end % makeCartesian
        
        function textReport=runTests(obj)
            tic;
            obj=obj.open();
            obj=initTest(obj);
            obj.print(obj.getTitle);
            [obj test]=obj.nextTest();
            while not (isempty(test))
                 test=test.run();
                 reportLine=test.report2Line();
                 obj.print(reportLine);
                 disp(['test no' num2str(obj.currentConfig)]);
                 [obj test]=obj.nextTest();
             end % while
            obj.close();   
            textReport=[];
            toc;
        end % runTests
        function print(obj, reportLine)
            if obj.toExcel
                obj.excel.addRows (reportLine);
            end % if
        end % print(reportLine)
        function obj=open(obj)
            if obj.toExcel
                 obj.excel=Excel(obj.fileName, obj.sheetName); % use current directory
            end % if 
        end
        function close(obj)
            if obj.toExcel
                obj.excel.closeFile();
            end
        end
        function reportLine=makeReportLine(obj)
           reportLine=cell(1,length(obj.reportFields));
           for i=1:length(obj.reportFields)
                fieldValue = Utils.getFieldValue(obj, obj.reportFields{i});
                reportLine{i}=fieldValue;
           end % for
        end
        
        function reportLine = report2Line (obj)
            reportLine=obj.makeReportLine();
        end
        function text=getTitle(obj)
            obj=obj.setReportFields();
            text=obj.reportFields;
        end % getTitle
%         function obj=clearReportFields(obj)
%            for i=1:length(obj.reportFields)
%                 obj= Utils.setFieldValue(obj, obj.reportFields{i}, ' ');
%            end
%         end
    function obj=setReportFields(obj)
    end % setReportFields
    end % methods
    methods (Abstract)
        report=run(obj);
    end % abstract methodsobj
end

