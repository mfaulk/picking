classdef docMatrix < Matrix
    % A matrix of size n-by-d. Used to store a set P of n points in R^d.
    %each point refers to a document, i.e docID vector n-by-1
    
    
    properties
        docID
    end
    
    methods
        %constructor docMatrix is a matrix class with additive metadata 
        %needed as properties(currently only docID)
        function obj=docMatrix(docID,varargin)
            obj=obj@Matrix(varargin{:});
            obj.docID = docID;
        end
    end
    
end

