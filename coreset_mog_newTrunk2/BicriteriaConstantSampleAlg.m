classdef BicriteriaConstantSampleAlg < AbstractBicriteriaAlg
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    methods
        function obj = BicriteriaConstantSampleAlg(varargin)
            obj.beta = varargin{1};
            obj.robustNIteration = varargin{2};
            obj.robustAlg = varargin{3};
            obj.ptasAlg = varargin{4};
            obj.orderStatisticsAlg = varargin{5};
        end
        
        function center=robust(obj,F, medianFraction, beta,nIteration)
            %There's no overloading in matlab:
            if isa(obj.robustAlg,'AbstractPtasAlg')
                center = obj.robustAlg.compute(F,beta,nIteration);
            else
                center = obj.robustAlg.compute(F,gamma,beta,nIteration);
            end
        end
    end
end