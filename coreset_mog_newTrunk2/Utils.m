classdef Utils
% statuc global functions that are used by all classes.
    properties
    end
    
    methods (Access=public, Static);
        function S=fieldName2Struct(fieldName)
                 fieldName=['.' fieldName];
                 [s e]=stok(fieldName, '.()');
                 nTokens=length(s);
                 fieldNames=cell(1,nTokens);
                 for i=1:nTokens
                     fieldNames{i}=fieldName(s(i):e(i));
                 end
                 c={};
                 %s(nTokens+1)=' ';
                 for i=1:nTokens
                      if strcmp(fieldName(s(i)-1),'.')
                        c{2*i-1}='.';
                        c{2*i}=fieldName(s(i):e(i));
                      elseif strcmp(fieldName(s(i)-1),'(')
                        c{2*i-1}='()';
                        c{2*i}={str2num(fieldName(s(i):e(i)))};
                      end % if
                 end
                 try
                 S = substruct(c{:});
                 catch
                     disp(1);
                 end
        end % fieldName2struct        
        function fieldValue=getFieldValue(obj, fieldName)
                S=Utils.fieldName2Struct(fieldName);
                 try
                     fieldValue=subsref(obj,S);
                 catch exception
                     fieldName
                     throw(exception)
                 end % try
        end % getFieldValue
        
        function obj=setFieldValue(obj, fieldName, fieldValue)
                 S=Utils.fieldName2Struct(fieldName);
                 try
                    subsasgn(obj, S, fieldValue);
                 catch exception
                     fieldName
                     fieldValue
                     throw(exception)
                 end % try
        end
        
        function C= setProd(cellArray)
            C = Matrix(setprod(cellArray));
        end
        function error=ratio(opt,approx, absolute)
            if nargin<3
                absolute=true;
            end
            if absolute
                error=abs(approx)/opt;
            else
                error=approx/opt;
            end
            error= Utils.numericCheck (opt,approx,error);
        end
        function error= numericCheck (opt,approx,error)
            if (abs(error)<100*eps)
               error=0;
           end
            if(and(opt < 100*eps,approx < 100*eps))
                error = 0;
            end
        end
        function error = relativeError (opt, approx, absolute)
            if nargin<3
                absolute=true;
            end
            % return a number between 0 to 1, represents the relative error of
            % approximating approx by opt
            if absolute
                error=abs(approx-opt)/opt;
            else
                error=(approx-opt)/opt;
            end
            error= Utils.numericCheck (opt,approx,error);
        end % function relativeError
        
        function varargout =complete(optargs, varin)
            % Some function that requires 2 inputs and has some optional inputs.
            % only want 3 nonempty optional inputs at most
            varin=varin{1};
            numvarargs = find(~cellfun('isempty',varin));
            if length(numvarargs) > length(optargs)
                error(['requires at most'  length(optargs) 'optional inputs']);
            end
            % now put these defaults into the valuesToUse cell array,
            % and overwrite the ones specified in varargin.
            optargs(numvarargs) = varin(numvarargs);
            % Place optional args in memorable variable names
            varargout = optargs;
        end
        function ownerObj=completeProps(ownerObj, optargs, varin)
               s=[];
               for i=1:length(varin)
                   s=char(strcat(s, ' ownerObj.', varin(i)));
               end
               b=['[' s ']=Utils.complete(optargs,{{', s,'}});'];
               eval(b);
        end
      end % methods
    
end

