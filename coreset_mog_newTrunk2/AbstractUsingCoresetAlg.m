classdef AbstractUsingCoresetAlg  
    %UNTITLED3 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
    end
    
    methods
        function result=abstractCompute(F) % assume parameters of coresetAlg and UsincoresetAlg are set
	        if coresetAlg~=[]
                G=coresetAlg.compute(F);
            else
                G=F;
            end
		    compute(G);
        end
    end
    methods(Abstract)
    	result=compute(obj,F,gamma,eps,delta);
    end
    
end

