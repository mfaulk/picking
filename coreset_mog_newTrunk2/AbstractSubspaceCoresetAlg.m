classdef AbstractSubspaceCoresetAlg < AbstractCoresetAlg
% Abstract class for algorithms that construct coresets that 
% approximate distances to a query subspace.
    properties
        j % the dimension of the optimal subspace
        isProjection % whether the coreset approximates projections on the 
                     % subspaces, instead of distances
        epsilon % error bound
    end
    
    methods 
        function set.epsilon(obj, epsilon)
          setEps (obj,epsilon);            
        end % function set
        
        function unitedCoreset=computeUsingCluster(obj, cluster)
            G=cluster.getFunctionSet().M;
%             obj.j=cluster.functions.j;       
            center = cluster.centers{1};
            unitedCoreset = obj.compute(G, center);
            unitedCoreset = PointFunctionSet(unitedCoreset);
        end % computeUsingClusters
        
        % set defaults value for the properties
        function setDefaults(obj)
            Utils.completeProps(obj, {false, 1},{'isProjection', 'j'});
        end       
        
        % Compute the subspace that minimizes the cost function to the
        % coreset C
        function opt=computeOpt(obj, C)
            C=Matrix(C.matrix);
            opt=C.getOptSqSubspace(obj.j);
        end
        
        % Compute the cost for a query subspace on a given set P
        function cost=computeCost(obj, P, subspace)
            P=Matrix(P.matrix);
            if obj.isProjection
                 cost=subspace.sumSqProjections(P);
            else cost=subspace.sumSqDistances(P);
            end % if
        end
        
        % Compute the minimum cost for P over all query subspaces.
        % P might be a coreset or the original set
        function optCost=computeOptCost(obj, P)
            % Any coreset should support matrix property.
            % note: should be implemented as typecast
%             MP=Matrix(P.matrix); - why do we need this row (writer:
%             Danny, commenter: Shachar)
            if obj.isProjection
                optCost=sum(P.getOptSqProjections(obj.j));
            else 
                optCost=sum(P.getOptSqDistances(obj.j));
            end  % if
        end % function optCost=computeOptCost
    end % methods
end % classdef AbstractSubspaceCoresetAlg

