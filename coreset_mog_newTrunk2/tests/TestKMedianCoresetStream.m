classdef TestKMedianCoresetStream < TestKMedianCoreset
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % stream data
        leafSize        
        DS=DataStream();
    end
    
    methods
        function obj=setReportFields(obj)
            obj.reportFields = {'DS.batchSize' , 'n' 'kc.k' 'kc.t' 'kc.bicriteriaAlg.beta' ...
                'leafSize' 'tryNo' 'energy' 'Cenergy' 'CEnergyVsEnery'};

        end
        % Test random stream
        function main1(obj)

            [allP StreamCoreset]=Stream.testKMedianStream(obj.kc, obj.leafSize, obj.n, 2);
            [ct, t, ce, e] = compareCoresetKMeans(allP, StreamCoreset, obj.kc.k, inf, inf);
                        
            obj.CTimeKmeansOnCoreset=ct;
            obj.TimeKmeansWithoutCoreset=t;
            % obj.CTimeTotalKmeansOnCoreset=obj.CTimeConstruction+obj.CTimeKmeansOnCoreset;
            
            obj.Cenergy=ce;
            obj.energy=e;
        end

        % DataStrream testing
        function main2(obj)
            C=obj.DS.computeCoreset(Matrix(obj.inputMatrix), obj.kc);

            % may not compute the CKmeans for large n.
            % [ct, t, ce, e] = compareCoresetKMeans(allP, StreamCoreset, obj.kc.k, inf, inf);
                      
            obj.CTimeKmeansOnCoreset = tic;
            opts = statset('MaxIter', obj.nIterationsWithCoreset);
            [~, CCenters] = Ckmeans(C.M.matrix, obj.kc.k, C.W.matrix,...
            'emptyaction','singleton','options', opts);
            obj.CTimeKmeansOnCoreset=toc(obj.CTimeKmeansOnCoreset);
            % obj.CTimeTotalKmeansOnCoreset=obj.CTimeConstruction+obj.CTimeKmeansOnCoreset;
            % end of timer for coreset kmedian
            
            % Compare the centers.
            W(1:obj.n,1)=1;
            CDist = TestKMedianCoreset.distfun(obj.inputMatrix, CCenters, W, 'sqeuclidean', 0);
            [Cd, Cidx] = min(CDist, [], 2);
            CsumDist = accumarray(Cidx,Cd,[obj.kc.k,1]);
            obj.Cenergy = sum(CsumDist);

            obj.TimeKmeansWithoutCoreset=0;
            % obj.CTimeTotalKmeansOnCoreset=obj.CTimeConstruction+obj.CTimeKmeansOnCoreset;
            obj.energy=0;
        end
        
        function obj=run(obj)
            obj=obj.setReportFields();
            % [StreamCoreset]=KMedianStream(calg, lsize, n, d, iterations)
            % To compare the stream in memory
            % obj.main1();
            obj.main2();
        end 
        
    end % methods
    
    methods (Static)
        function testStream(n,d,k,update,batchSize, leafSize)
            dataMatrix=rand(n,d);
            T=TestKMedianCoresetStream();
            % Windows 7 does not have c:\temp directory
            % T.fileName='c:\temp\report.xls';
            T.fileName='../report_stream.xls';
            T.inputMatrix=dataMatrix;
            T=T.setTestFields('kc.bicriteriaAlg.update', update);
            T=T.setTestFields('n', n);
            T=T.setTestFields('kc.k', k);
            T=T.setTestFields('kc.coresetType', [KMedianCoresetAlg.linearInK]);
            T=T.setTestFields('kc.t', [ 1000]);
            T=T.setTestFields('kc.bicriteriaAlg.beta', k);
            T=T.setTestFields('tryNo', 1);
            T=T.setTestFields('nIterationsWithCoreset', inf);
            T=T.setTestFields('nIterationsWithoutCoreset', inf);
            T=T.setTestFields('leafSize', leafSize);
            T=T.setTestFields('DS.batchSize',batchSize);
            
            T.runCartesianProduct();
        end % test
    end % methods (static)
end % class