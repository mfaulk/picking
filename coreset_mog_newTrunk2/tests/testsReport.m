function [ output_args ] = testsReport( input_args )
function simpleStream(t)
G=bvgraph('C:\installs\bvgraph-1.2\data\wb-cs.stanford');
filename='C:\pagerank\stanford3.xls';
delete(filename);
%[optxAll,~,~,~] = bvpagerank(G);
n=size(G,1);
m=25;
chunk=ceil(n/t);
header={'IsAdaptiveNodes' 'Node no','PageRank score','relative error from final PageRank score','log Error' 'Improvement in this iteration', ...
    'Iteration no','Graph size','Sum Of Errors','Time'};
G=double(sparse(G));
[U D V]=svds(G,2);
[optxAll,~,~,~] =mypagerank(G,m,[],[], []);

currentRow=1;
sheet=1;
sheetName=['cnr' num2str(1)];
xlswrite(filename, header,  sheetName, 'A1');
   %
for i=chunk:chunk:n
 newRow=rep(i, G, currentRow, sheet, sheetName, optxAll, m, filename);
 currentRow=newRow;
end
  rep(n, G, currentRow, sheet, sheetName, optxAll, m, filename)
    % Output matrix report:
% size read | iteration | node | page Rank | final Page Rank | error
end
 
function newRow =rep(i, G, currentRow, sheet, sheetName, optxAll, m, filename)
    currentG=G(1:i,1:i);
    indexes=AdaptiveSampling(optxAll(1:i),m);
    [~,~,~,myreport] = mypagerank(currentG,m,optxAll(1:m), optxAll(indexes), indexes);
    dataPlacement = ['A' num2str(currentRow+1)];
    xlswrite(filename, num2cell(myreport), sheetName, dataPlacement);
    newRow=currentRow+size(myreport,1);
    
%    if currentRow+size(report,1)>65000
%        currentRow=1;
%        sheet=sheet+1;
%        sheetName=['cnr' num2str(sheet)];
%        xlswrite(filename, header,  sheetName, 'A1');
%    end
    % Output matrix report:
% size read | iteration | node | page Rank | final Page Rank | error
end

end

