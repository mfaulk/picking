%create random data
n=200;
d=1e5;
nzPerPoint = 1e3;
numOfChunks = 75;
allP=Matrix([]);
for i=1:max(numOfChunks);
    allP.merge(Matrix('sparse',n,d,nzPerPoint));
end

j=10;
iterations=1;
leafSizes= [1000];
chunks=[allP.n/leafSizes];
lowDimSampleVals = [100];
originaSampleVals = 80;
BCsampleSize = [60];

a = LowDimOriginCoresetAlg();
a.originCoresetAlg.sampleSize = originaSampleVals;
a.j=j;
a.isProjection=false;

b  = BicriteriaConstantSampleAlg(BCsampleSize,1,SpannedSubspaceAlg(),SpannedSubspaceAlg(),OrderStatisticBySortAlg());

check = LowDimOriginCoresetAlg();
check.j=j;
Time = zeros(size(lowDimSampleVals,2),size(BCsampleSize,2),size(leafSizes,2),iterations);
Memory = zeros(chunks,size(lowDimSampleVals,2),size(BCsampleSize,2),size(leafSizes,2),iterations);
 USERVIEW = memory;
initialMemory = USERVIEW.MemUsedMATLAB;


for iter=1:iterations
    for leafSizeIndex = 1:size(leafSizes,2)
        leafSize = leafSizes(leafSizeIndex);
        for BCsampleIndex = 1:size(BCsampleSize,2)
            %change BC.beta
                for lowDimSampleIndex = 1:size(lowDimSampleVals,2)
                    a.lowDimSampleSize=lowDimSampleVals(lowDimSampleIndex);
                    a.bicriteriaAlg = b;
                    s=[];
                    s=Stream(a,leafSize);
                    for i=1:chunks
                        P =PointFunctionSet(Matrix(allP.m(((i-1)*allP.n/chunks+1:i*allP.n/chunks),:)));
                        start = tic;
                        s.addPointSet(P);
                        tempTime = toc(start);
                        Time(lowDimSampleIndex,BCsampleIndex,leafSizeIndex,iter) = Time(lowDimSampleIndex,BCsampleIndex,leafSizeIndex,iter) + tempTime;
                        USERVIEW = memory;
                        Memory(i,lowDimSampleIndex,BCsampleIndex,leafSizeIndex,iter) = USERVIEW.MemUsedMATLAB;
                        i
                    end % for
                    allC=s.getUnifiedCoreset();
%                     save testStream2_parameterFeel.mat
%                     [errorCostPUsingOptC(lowDimSampleIndex,BCsampleIndex,leafSizeIndex,iter), ~]=check.isGoodCoreset(allP,allC);
                    disp(strcat('finished sampleSize:' , int2str(lowDimSampleVals(lowDimSampleIndex))));
                end %lowDim
             disp(strcat('finished BCsampleSize:' , int2str(BCsampleSize(BCsampleIndex))));
        end %BC
         disp(strcat('finished leafSize:' , int2str(leafSize)));
    end %leafSize
    disp(strcat('finished Iteration:' , int2str(iter)));
end %iterations