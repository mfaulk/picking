clc;
d=2;
j=10;
r=1000;
for i=1:10
    j=j+1;
    k=i;
    X=CoordinatesMat(randperm(r),:);
    Wgt=randn(r,1);
    tic;
    [idx, C, sumD, D] = weightedkmeans(X, k, Wgt)
 
    A(j)=toc;
    Q(j)=sum(sumD);
    
end
figure(1);
plot(A);
figure(2);
plot(Q);