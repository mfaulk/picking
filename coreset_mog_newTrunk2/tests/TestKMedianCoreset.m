classdef TestKMedianCoreset < Test
    %UNTITLED Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        inputMatrix % input points
        n % number of points from the matrix

        energy
        Cenergy
        tryNo;  % Number of tries for the same group of parameters.
        
        kc =KMedianCoresetAlg();% the kmeans coreset algorithm.
        CTimeConstruction;
        TimeKmeansWithoutCoreset        ;
        CTimeKmeansOnCoreset;
        CTimeTotalKmeansOnCoreset;
        
        nIterationsWithCoreset = inf;
        nIterationsWithoutCoreset = inf;
    end

   properties (Dependent)
        TimeVsCTime;
        CEnergyVsEnery;        
   end % properties (Dependent)
    
   methods
        function result=get.TimeVsCTime(obj)
             result=obj.TimeKmeansWithoutCoreset/obj.CTimeTotalKmeansOnCoreset;
        end % get.RatioCtimeVsTime()
        function result=get.CEnergyVsEnery(obj)
            result=obj.Cenergy/obj.energy;
        end % RatioCEnergyVsEnery 
        function obj=setReportFields(obj)
            obj.reportFields = {'kc.bicriteriaAlg.update', 'n', 'kc.k', 'kc.t' 'kc.bicriteriaAlg.beta' ...
               'CTimeConstruction', 'TimeKmeansWithoutCoreset', ...
        'CTimeKmeansOnCoreset', 'CTimeTotalKmeansOnCoreset',...
        'energy' 'Cenergy', 'tryNo', 'TimeVsCTime', 'CEnergyVsEnery',...
        'kc.report.bicriteriaCost', 'kc.report.bicriteriaTime', 'kc.report.bicriteriaSize',...
        'nIterationsWithCoreset', 'nIterationsWithoutCoreset', 'kc.coresetType', 'kc.weightsFactor', 'kc.warningNegative'};
        end

        function main(obj)
            % Create matrix.
            mat=obj.inputMatrix;
             M=Matrix(mat);
%             M.m=mat;
%             M.matrix=mat;

            % Create a Pointfunctionset from input matrix.
            P=PointFunctionSet(M);
            Kc=obj.kc;
            
            % Compute CORESET
            % timer for coreset kmedian
            % kc.setParameters(1, 0.1, t, 0.1);
            obj.CTimeConstruction=tic;
            Kc.computeCoreset(P);
            obj.CTimeConstruction=toc(obj.CTimeConstruction);
            disp(['Overall construction time is: ' num2str(obj.CTimeConstruction)]);
            C=Kc.coreset;

            % compare the results 
%             [ct, t, ce, e]=TestKMedianCoreset.compareCoresetKMeans(P, C, obj.kc.k, ...
%                 obj.nIterationsWithCoreset, obj.nIterationsWithoutCoreset);
% 
%             obj.CTimeKmeansOnCoreset=ct;
%             obj.CTimeTotalKmeansOnCoreset=obj.CTimeConstruction+obj.CTimeKmeansOnCoreset;
%             obj.TimeKmeansWithoutCoreset=t;
%             obj.Cenergy=ce;
%             obj.energy=e;
%             
        end% function main   

        function obj=run(obj)
            obj=obj.setReportFields();
            obj.main();
        end 
        
    end % methods
    
    methods (Static)
          
        function D = distfun(X, C, weight, dist, iter)
                %DISTFUN Calculate point to cluster centroid distances.
                [n,p] = size(X);
                D = zeros(n,size(C,1));
                nclusts = size(C,1);

                switch dist
                case 'sqeuclidean'
                    for i = 1:nclusts
                        D(:,i) = (X(:,1) - C(i,1)).^2;
                        for j = 2:p
                            D(:,i) = D(:,i) + (X(:,j) - C(i,j)).^2;
                        end
                        % D(:,i) = sum((X - C(repmat(i,n,1),:)).^2, 2);
                    end
                    D=bsxfun(@times,D,weight);
                case 'cityblock'
                    for i = 1:nclusts
                        D(:,i) = abs(X(:,1) - C(i,1));
                        for j = 2:p
                            D(:,i) = D(:,i) + abs(X(:,j) - C(i,j));
                        end
                        % D(:,i) = sum(abs(X - C(repmat(i,n,1),:)), 2);
                    end
                case {'cosine','correlation'}
                    % The points are normalized, centroids are not, so normalize them
                    normC = sqrt(sum(C.^2, 2));
                    if any(normC < eps(class(normC))) % small relative to unit-length data points
                        error('stats:kmeans:ZeroCentroid', ...
                              'Zero cluster centroid created at iteration %d.',iter);
                    end

                    for i = 1:nclusts
                        D(:,i) = max(1 - X * (C(i,:)./normC(i))', 0);
                    end
                case 'hamming'
                    for i = 1:nclusts
                        D(:,i) = abs(X(:,1) - C(i,1));
                        for j = 2:p
                            D(:,i) = D(:,i) + abs(X(:,j) - C(i,j));
                        end
                        D(:,i) = D(:,i) / p;
                        % D(:,i) = sum(abs(X - C(repmat(i,n,1),:)), 2) / p;
                    end
                end

        end

    end
    
    methods (Static)
        function test(n,d,k, update)
                %load('c:\coresets\data\Localization1.mat', 'Localization1');
                %dataMatrix=Localization1;
            dataMatrix=rand(n,d);
            T=TestKMedianCoreset();
            % Windows 7 does not have c:\temp directory
            % T.fileName='c:\temp\report.xls';
            T.fileName='../report_static.xls';
            T.inputMatrix=dataMatrix;
            T=T.setTestFields('n', n);
            T=T.setTestFields('kc.k', k );
            T=T.setTestFields('kc.coresetType', [KMedianCoresetAlg.linearInK]);% ]);
            T=T.setTestFields('kc.t', [1000]);
            T=T.setTestFields('kc.bicriteriaAlg.beta', k);
            T=T.setTestFields('tryNo', 1);
            T=T.setTestFields('kc.bicriteriaAlg.update', update);
            T=T.setTestFields('nIterationsWithCoreset', inf);
            T=T.setTestFields('nIterationsWithoutCoreset', inf);
            T.runCartesianProduct();
        end % test
        
    end % methods (static)
end % class