classdef RandomEpsApproxAlg
    %RANDOMEPSAPPROXALG Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        % sampling with = 1 , without = 0
        repetition
        
        % sampleSize
    end
    
    
    
    methods
        
        function obj = RandomEpsApproxAlg(repetition)
            obj.repetition = repetition;
        end
        
        function G = compute(obj,F,epsilon,delta)
            % calculate sample size according to delta and epsilon
            sampleSize = round(log(1/delta)/(epsilon^2));
            if sampleSize > F.size(1)
                %disp('Warning - sample Size is bigger than FunctionSet Size');
                sampleSize = F.size(1);
            end
            G = F.randomSubset(sampleSize,obj.repetition);
        end
    end
    
end