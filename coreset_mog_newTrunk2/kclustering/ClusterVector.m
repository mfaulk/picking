classdef ClusterVector < handle    
%--------------------------------------------------------------------------
%This class is a general implementation of the set of clusters. Although it
%is designed for the bicriteria clustering algorithm at the very beginning,
%in fact it is a general representation of any set of clusters by any
%clustering algorithms. In order to make the class self-contained, the
%major attributes we need to maintain for any set of clusters are:
%   - The set of points to be clustered
%       - Corresponds to the member variable "F";
%
%   - The set of centers in the clusters (we use the indexes of the centers to name the clusters)
%       - Corresponds to the member variable "centers";
%       - Member variable "centerCounter" records the number of centers in
%       the clusters (or the number of non-zero elements in centers);
%
%   - The indexes of points which are already clustered (in some applications we may not want to cluster the whole set)
%       - Corresponds to the member variable "pointIndexes";
%       - Member variable "pointCounter" records the number of clustered
%       points (or the number of non-zero elements in pointIndexes);
%
%   - The index of the cluster(center) a point belongs to
%       - Corresponds to the member variable "centerIndexes";
%
%   - The distance of a point to its cluster center 
%       - Corresponds to the member variable "distances";
%
%Here is an example:
%  
%Initially, suppose F contains 10 points. Naturally, the indexes of the 10
%points are from 1 to 10. In the following I shall use the index of every
%point as its name. After running some clustering algorithm (not restricted
%to the bicriteria algorithm), suppose we have the following centers:
%   centers: point 2, point 5, point 7, point 10. 
%
% For each center, it defines a cluster with some points in F associated
% with the center. Suppose we have the following clusters ("dist" means the
% distance from the point to its center):
%   Cluster 1: center: point 2;
%              points in the cluster: point 2(dist: 0), point 3(dist: 0.4),
%              point 6(dist: 0.9), point 8(dist: 1.0);
% 
% 
%   Cluster 2: center: point 5; 
%              points in the cluster: point 1(dist: 0.3), point 4(dist:
%              0.7), point 5(dist: 0);
% 
%   
%   Cluster 3: center: point 7;
%              points in the cluster: point 7(dist: 0), point 9(dist: 0.4);
% 
%   
%   Cluster 4: center: point 10;
%              points in the cluster: point 10(dist: 0);
% 
% 
%For this case, the member variables in the class looks as follows: 
%   pointIndexes: [2, 3, 6, 8, 1, 4, 5, 7, 9, 10]
%       Short explanation: we rearrange the points in F according to
%       clusters and put them into pointIndexes from Cluster 1 to Cluster
%       4. Every value in pointIndexes corresponds to the original index of
%       the point in F. (Recall that we use the indexes in F as the name
%       for every point)
% 
%   distances: [0, 0.4, 0.9, 1.0, 0.3, 0.7, 0, 0, 0.4, 0]
%       Short explanation: the distances vector holds a one-to-one
%       correspondence with the pointIndexes vector, i.e., distances[i] (1
%       <= i <= 10) is the distance between point pointIndexes[i] to its
%       corresponding center.
% 
%   centerIndexes: [1, 1, 1, 1, 2, 2, 2, 3, 3, 4]
%       Short explanation: the centerIndexes vector also holds a one-to-one
%       correspondence with the pointIndexes vector, i.e., centerIndexes[i]
%       (1 <= i <= 10) is the index of center/cluster the point
%       pointIndexes[i] belongs to. Note that here we change the indexes
%       for centers. For example, although the center of Cluster 1 is point
%       2, in the record of centerIndexes, I take "point 2" as "center 1".
%       In order to be able to fetch the "real index" of a center, we
%       provide the following "centers" member variable.
% 
%   centers: centers[i] (1 \leq i \leq 4) refers to the coordinates of
%       "center i". For example, center[2] refers to the coordinates of
%       "center 2", which is in fact the coordinates of point 5.
% 
%   F: F is still the intact set of points. F[i] (1 \leq i \leq 10) refers
%       to the coordinates of point i.
% 
%   pointCounter: since we have 10 points clustered, pointCounter = 10.
% 
%   centerCounter: since we have 4 centers corresponding to 4 clusters,
%       centerCounter = 4.
%
%--------------------------------------------------------------------------

    properties (SetAccess = private)
        % To understand better of the meaning of all the following member
        % variables, please refer to the above example.        
        
        pointIndexes; % Record the indexes of points in the original point set F which have been clustered.
        distances; % distances[i]: distance between pointIndexes[i] to its corresponding center.
        centerIndexes; % centerIndexes[i]: the center index/cluster index of point pointIndexes[i]. 
                       % Note that the center index is different from the point index in F. 
                       % We shall use the following member variable
                       % "centers" to connect the two.

        % Type =  MATLAB matrix
        centers; % centers[i]: the coordinates of "center i". 
                 % Here the center index changes consecutively starting from 1. 
                 % It is different from the point indexes in "F".
    end % private properties
    
    properties
        % Type = PointFunctionSet
        F; % F.M.m[i]: the coordinates of "point i";
           % F.W.m[i]: the weight of "point i". 
        
        pointCounter; % Record the number of points in the clusters.
        centerCounter; % Record the number of centers/clusters.
    end
    
    properties (Dependent)
         % Type = integer
         % Number of points to be clustered
         n;
         
         % Type = integer
         % Point dimension
         d;   
    end
     
    methods
        function result=getFunctionSet(obj)
            result = obj.F.subset(obj.pointIndexes);
        end %get the entire Clustered functionSet
        
        function result=getSumWeights(obj)
            G=obj.F.subset(obj.pointIndexes(1:obj.pointCounter)); 
            result=G.sumW;
        end %get sum of weights of a set of points in F w.r.t pointIndexes
        
        function result=getDistances(obj)
            result=obj.distances(1:obj.pointCounter);
        end % get the distances vector 

        function result=getPointIndexes(obj)
            result=obj.pointIndexes(1:obj.pointCounter);
        end % get the pointIndexes vector

        function result=getCenterIndexes(obj, pointIndexes)
            if nargin>1
                result=obj.centerIndexes(pointIndexes);                
            else
                result=obj.centerIndexes(1:obj.pointCounter);                
            end
        end % get the centerIndexes vector

        function result=getCenters(obj,indexes)
            if nargin >1
                result = obj.centers.subset(indexes);
            else
                result=obj.centers.subset(1:obj.centerCounter);
            end
                
        end % get the centers
        
        % Initialize a new cluster vector
        % It is only called in the cluster vector where you want to store
        % the final results.
        % For temporary cluster vectors, you don't need to call it as it
        % costs much to clear and initialize
        function init(obj, centerSize, intNandD)
            if (centerSize > 0)
%                 obj.centers = zeros(centerSize, obj.d);
            end
            
            if (intNandD == true)
                obj.clear();
                % Since we can know in prior the size of all the following
                % vectors. It is faster to allocate enough space initially.
                obj.distances = zeros(obj.n, 1);      
                obj.centerIndexes = zeros(obj.n, 1);
                obj.pointIndexes = zeros(obj.n,1);
            end
        end

        % initial the cluster vector with a given set of clusters with
        % centers
        function initWithFunctionSet(obj, F, pointIndexes, distances, centers, centerIndexes)
            obj.F = F;
            obj.pointIndexes = pointIndexes;
            obj.distances=distances;
            obj.centerIndexes=centerIndexes;
            obj.pointCounter = length(pointIndexes);
            %centers & centerCounter
            if isa(centers,'PointFunctionSet')
                obj.centers = centers;
                obj.centerCounter = centers.size;
            elseif isa(centers,'Subspace')
                obj.centers = {centers};
                obj.centerCounter = size(centers,1);
            elseif isa(centers,'cell')
                obj.centers = centers;
                obj.centerCounter = size(centers,1);
            end
        end % initWithFunctionSet
        
        % Obtain the smallest partitionFraction of the current total number of  
        % points with their associated clusters
        function result = smallest(obj, partitionFraction)           
            % Calculate the index m such that the sum of weights of index
            % mIndexes is at least the "partitionFraction" of the total weights.
            dists=obj.distances(1:obj.pointCounter);
            G=obj.F.subset(obj.pointIndexes(1:obj.pointCounter)); 
            mIndexes = ...
                OrderStatisticBySortAlg.compute(dists, G.sumW * partitionFraction, G.W.matrix, obj.centerCounter);
            m = length(mIndexes);
             
            % Assign values to the attributes of the resulted cluster
            % vector
            result = ClusterVector();
            result.pointCounter = m;
            result.centerCounter = obj.centerCounter;
            result.distances = obj.distances(mIndexes);
            result.centerIndexes = obj.centerIndexes(mIndexes);
            result.pointIndexes = obj.pointIndexes(mIndexes);
            result.centers = obj.centers;
            result.F=obj.F;
        end
        
        % Append the specific cluster vector to the current cluster vector
        function union(obj, clusterVec, currentIndexes)
            % Use intermediate variables to speedup
            opC = obj.pointCounter;
            ocC = obj.centerCounter;
            cvpC = clusterVec.pointCounter;
            cvcC = clusterVec.centerCounter;
            
            % Append "distances" to the "distances" vector of the object
            obj.distances(opC + 1: opC + cvpC) = clusterVec.distances;
            % Append "centerIndexes" to the "centerIndexes" of the object
            obj.centerIndexes(opC + 1: opC + cvpC) = clusterVec.centerIndexes + ocC;
            
            % Append "centers" to the "centers" of the object
%            clusterVec.centers.M.matrix
            if isempty(obj.centers)
                %initialize
                if isa(clusterVec.centers,'PointFunctionSet')
                    obj.centers = PointFunctionSet();
                elseif isa(clusterVec.centers,'cell');
                    obj.centers = {};
                end
            end
            if isa(obj.centers,'double') %shouldn't happen
                obj.centers(ocC + 1: ocC + cvcC,:) = clusterVec.centers;
                warning('centers property in ClusterVector is of type double');
            elseif isa(obj.centers,'PointFunctionSet') %each point is a center
                %use the commented for faster calculation when size is
                %known in advance
%                 obj.centers.M.m(ocC + 1: ocC + cvcC,:) = clusterVec.centers.M.m;
%                 obj.centers.W.m(ocC + 1: ocC + cvcC,:) = clusterVec.centers.W.m;
                obj.centers.merge(clusterVec.centers);
            elseif isa(obj.centers,'cell') %subspace centers in cells
                %use the commented for faster calculation when size is
                %known in advance
                obj.centers{ocC + 1: ocC + cvcC} = clusterVec.centers{:};
            end
                
                
            
            % Append "pointIndexes" to the "pointIndexes" of the object
            obj.pointIndexes(opC + 1: opC + cvpC) = currentIndexes(clusterVec.pointIndexes);
            
            % Update obj.pointCounter and obj.centerCounter
            obj.pointCounter = opC + cvpC;
            obj.centerCounter = ocC + cvcC;
        end
        
        %this function shouldn't be here
        % Recompute the point distances to the nearest centers
        function dists = updateNearestCenters(obj)
            G = obj.F.subset(obj.pointIndexes(1:obj.pointCounter));
            [obj.distances, obj.centerIndexes] = NearestCenter.compute(G.M.matrix, G.W.matrix, obj.centers);
            dists = Matrix(obj.distances);
        end
        
        % Return a specific cluster i
        % center: the center of cluster i
        % clusterIndexes: indexes of points in the original F that belongs
        % to cluster i
        function [center clusterIndexes] = getClusterIndexes(obj, i)
              if (i > obj.centerCounter)
                  disp('Cluster number exceeded!');
              else
                 iclusterIndexes = (obj.centerIndexes == i);
                 center = obj.centers(i,:);
                 clusterIndexes = obj.pointIndexes(iclusterIndexes);
              end
        end
          
        %Return a specific cluster (subset of this obj)
        %currently works only for centers of type cell
        function [subClusterVec] = getCluster(obj,index)
            if isa(obj.centers,'cell')
               subCenter = obj.centers(index); 
               iclusterIndexes = (obj.centerIndexes == index);
               subPointIndexes = obj.pointIndexes(iclusterIndexes);
               subDistances = obj.distances(subPointIndexes);
               subCenterIndexes = ones(size(subPointIndexes));
               subClusterVec = ClusterVector();
               subClusterVec.initWithFunctionSet(obj.F,subPointIndexes,subDistances,subCenter,subCenterIndexes);
            end
            
        end
       
        % Clean the object. Release memory.
        function clear(obj)
            obj.pointCounter = 0;
            obj.centerCounter = 0;
            clear obj.distances;
            clear obj.pointIndexes;
            clear obj.centerIndexes;
            clear obj.centers;
            clear obj.F;
        end
        
        function result = get.n(obj)
            result = obj.F.size;
        end
        
        function result = get.d(obj)
            result = obj.F.dim;
        end
    end
end
