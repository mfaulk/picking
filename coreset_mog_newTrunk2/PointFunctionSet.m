classdef PointFunctionSet < AbstractFunctionSet
%--------------------------------------------------------------------------
%This class represents a set of weighted points where each point is a
%function.
%--------------------------------------------------------------------------
    
    properties
        % Weights of the points. Vector of n elements.
        % NOTE: W is n-by-1 vector.
        % Type = Matrix
        W; 
        
        % Type =  real
        sumW;
    end
    
    methods
        % Constructor
        % Points is a Matrix object (or empty set)
        function obj = PointFunctionSet(Points, varargin)
            if nargin == 0
                obj.M = Matrix();
                obj.W = Matrix();
                obj.sumW = 0;
            end
            if nargin == 1
                obj.M = Points;
                obj.W = Matrix(ones(obj.size, 1));
                % We calculate the sum of weights here. In the following we
                % can refer to the property directly without calling the
                % sum function again. 
                obj.sumW = obj.size;
            end
            if nargin > 1
                obj.M = Points;
                obj.W = varargin{1};
                obj.sumW = sum(obj.W.matrix);
            end
        end
               
        % Compute squared distance from obj (set of points) to centers
        % Either set of points or cell of subspaces        
		% Both obj and centers are PointFunctionSet type.
        % We also return the indices that reach the minimum.
        function [sqDistances indexes] = Eval(obj, centers)
            if isa(centers,'PointFunctionSet')
                N=NearestCenterAlg();
                [sqDistances indexes] = N.compute(obj.M.matrix, obj.W.matrix, centers.M.matrix);
            elseif isa(centers,'Subspace')
                % Compute squared distance from point in Matrix to subspace
                sqDistances=[];
                for i=1:size(centers,2)
                    sqDistances(:,i) = obj.M.sqDistances(centers(:,i));
                end
                sqDistances = min(sqDistances,[],2);

                %indexes is created to match for the abstract Eval, in clusters
                %there are indexes for the nearest pointFunctionSet
                indexes = ones(size(sqDistances,1),size(sqDistances,2));
            elseif isa(centers,'cell')
                warning('multiple subspace distances is not supported');
            end
        end
        
        % Return the subset of matrix M consists of rows indexed by indexes
        function G = subset(obj, indexes)
          G = PointFunctionSet(obj.M.getRows(indexes), obj.W.getRows(indexes));
        end
        
        % Return a random subset of obj with size sampleSize
        function  S = randomSubset(obj, sampleSize, repetition)
          [randM, randIndex] = obj.M.sampleRows(sampleSize,repetition);  
          S = PointFunctionSet(randM, obj.W.getRows(randIndex));
        end
        
        % Remove certain subset of points according to the indexes
        function result = remove(obj, mIndexes)
            rest = 1:obj.size;
            rest(mIndexes)=[];
            result = obj.subset(rest);
        end
        
        % Merge two PointFunctionSets
        function obj = merge(obj, P2)
            obj.M.merge(P2.M);
            obj.W.merge(P2.W);
%             % can not use merge function from Matrix since we need to check
%             % dimensions
%             if obj.M.nCols==0
%                 % obj.dim = P2.dim;
%                 obj.M.matrix=[obj.M.matrix; P2.M.matrix];
%                 obj.W.matrix=[obj.W.matrix; P2.W.matrix];
%                 obj.sumW = sum(obj.W.matrix);
%                 % obj.size = obj.M.nRows;
%             elseif P2.M.nCols== 0
%                 % Do nothing
%             elseif obj.M.nCols == P2.M.nCols
%                 %
%                 obj.M.matrix=[obj.M.matrix; P2.M.matrix];
%                 obj.W.matrix=[obj.W.matrix; P2.W.matrix];
%                 obj.sumW = sum(obj.W.matrix);
%                 % obj.size = obj.M.nRows;
%             else
%                 warning('Cannot merge pointsets with different in dimensions');
%             end
        end %end merge
        
        function clear(obj)
           obj.M.clear();
           obj.W.clear();
           obj.sumW = 0;
        end
      
        function subspace = getSpannedSubspace(obj)
            subspace = obj.M.getSpannedSubspace();
        end
        function result = getRows(obj,indexes)
            result=PointFunctionSet();
            result.M=obj.M.getRows(indexes);
            result.W=obj.W;
        end

    end
end