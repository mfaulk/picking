% j-dimensional subspace of R^d.
% Represented as a d-by-j matrix whose columns are orthogonal unit vectors
% that span the subspace.
classdef Subspace < Matrix
    properties (Dependent)
        j % dimension of the subspace (number of columns in the matrix)
    end
    
    properties
        %the origin point of an affine subspace (default = 0)
        originPoint = 0; 
    end
    methods
        function result=get.j(obj)
            result=obj.nCols;
        end
        
        function setOriginPoint(obj,inpOrigin)
            obj.originPoint = inpOrigin;
        end
        
        % constructors.
        % Subspace(S) - where S is a Matrix that represents a subspace,
        % copy the subspace. default is to orthogonalize the matrix
         %Subspace(S,orth) - orth = true to orthogonalize
        % Subspace(d,j) - random j-subspace in R^d, default is to
        % orthogonalize the matrix
        function obj=Subspace(varargin)
            if nargin==1 % copy constructor
                orthogonalize = true;
            else
               orthogonalize = varargin{2};
            end
            if isa(varargin{1},'Matrix')
                if ~orthogonalize
                    obj.matrix = varargin{1}.matrix;
                else
                %obj.matrix=varargin{1}.matrix; - instead take only orthogonal subspace
                [Q] = varargin{1}.orth();
                obj.matrix = Q.matrix;
                end    
            else d=varargin{1};
                 j=varargin{2};
                 if j==0 % zero-subspace is the origin
                    obj.matrix=zeros(d,0);
                 else
                    % M = RANDORTHMAT(d,j)
                    % generates a random d x j real matrix whose columns are orthogonal .
                    if (j>d)
                        error('j should not be larger than d');
                    end
                    tol=1e-6;
                    obj.matrix = zeros(d,j); % prealloc

                    % gram-schmidt on random column vectors
                    vi = randn(d,1);  

                    % the n-dimensional normal distribution has spherical symmetry, which implies
                    % that after normalization the drawn vectors would be uniformly distributed on the
                    % n-dimensional unit sphere.
                    obj.matrix(:,1) = vi ./ norm(vi);

                    for i=2:j
                      nrm = 0;
                      while nrm<tol
                        vi = randn(d,1);
                        vi = vi -  obj.matrix(:,1:i-1)  * ( obj.matrix(:,1:i-1).' * vi )  ;
                        nrm = norm(vi);
                      end
                      obj.matrix(:,i) = vi ./ nrm;
                    end %i
                end % if j==0
            end % if isa matrix            
         end  % Subspace(d,j)

         %ToDo: Adapt to affine subspace
         function [sumSqProjections ]=sqProjections(obj, P)
            % Compute the sum of squared distances from the origin to the projection
            % points of P on Subspace.
            projections=P*obj;
            sumSqProjections=projections.sumOfSquaredEntries();
         end        
        
        % Compute the sum of squared distances from the subspace to P
        function [sumSqDists ]=sumSqDistances(obj, P)
            sumSqDists = sum(obj.sqDistances(P));
            %faster implementation:
            %sumSqDists = norm(Pm,'fro')^2-(norm(A,'fro'))^2;
        end % function [sumSqDists]
        
        function [sqDists projP]=sqDistances(obj, P)
            if norm(obj.originPoint)~=0
                %Shift P and obj to Origin
                shiftedP = Matrix(obj.shiftToOrigin(P.m));
                shiftedObj = Subspace(Matrix(obj.shiftToOrigin(obj.m')'));
            else
                shiftedP = P;
                shiftedObj = obj;
            end

            projP = Matrix(shiftedP.m*shiftedObj.m); %rotate P onto  obj
            sqProjDists = projP.squaredRows; %Square of the projection of each point to the origin
            sqDists = shiftedP.squaredRows - sqProjDists;

            
            % projP=Matrix(P.m*obj.m); 
            % sqProjDists=projP.squaredRows; %Square of the projection of each point to the origin
            % sqDists=P.squaredRows-sqProjDists; 
            %don't do the following!!!:
            %sqDists(sqDists<0)=0; %cutting negative values resulted from rounding error
        end % function [sumSqDists]
        
        
        function [projectedPoint] = project(obj,inputPoint)
            %input: point
            %output: the point on obj closest to inputPoint (project
            %inpPoint on obj)
            %shifting the point and subspace in case of affine subspace
            %(origin is not 0)
            if norm(obj.originPoint) ~= 0
                shiftedP = obj.shiftToOrigin(inputPoint.m);
                shiftedObj = Subspace(Matrix(obj.shiftToOrigin(obj.m')'));
            else
                shiftedP = inputPoint.m;
                shiftedObj = obj;
            end
            shiftedProjP = shiftedP*shiftedObj.m*shiftedObj.m';
            %shifting the project point back to regular axis
            projectedPoint = obj.shiftFromOrigin(shiftedProjP);
        end
        
        function [shiftedP] = shiftToOrigin(obj,P)
            %input: P
            %output:shifted P , P shifted for originPoint to be
            %zero
            %matching sizes
            if norm(obj.originPoint) ~= 0
                s1 = size(P,1)/1;
                s2 = size(P,2)/size(obj.originPoint,2);
                shiftedP = P - repmat(obj.originPoint,s1,s2);
            else
                shiftedP = P;
            end
        end
        
        function [P] = shiftFromOrigin(obj,shiftedP)
            %input: shifted P , P shifted for originPoint to be
            %zero
            %output: shift back
            if norm(obj.originPoint) ~= 0
                s1 = size(shiftedP,1)/1;
                s2 = size(shiftedP,2)/size(obj.originPoint,2);
                P = shiftedP + repmat(obj.originPoint,s1,s2);
            else
                P = shiftedP;
            end
        end
                
    end % methods 
    
end % class Subspace

