% Load the data from the 16-bit phidgets, gathered by Minghei during the 1
% hour Milikan test.
%
% 1 count = 2.5 / 2^15 g
% 250 samples per second

%% Load the data, segment, and save the segments

countsToG = 2.5 / (2^15);
countsToMs2 = countsToG * 9.8;

sampleRate = 250;

% make segments for LtSt features:

% ltLength = 5; %seconds
% stLength = 2.5; % seconds
% deadTime = 1; % seconds between short-term and long-term buffers
% 
% segmentLength = ltLength + stLength + deadTime;

segmentLength = 2.5;

targetSampleRate = 50; % data will be resampled to this rate.
%

import phidget.*

% load the data and make TimeSeries objects

xFiles = {'01-165386-x.txt'  '02-165386-x.txt' '03-165386-x.txt' '04-165386-x.txt' '05-165386-x.txt' ...
    '01-165574-x.txt' '02-165574-x.txt' '03-165574-x.txt' '04-165574-x.txt' '05-165574-x.txt'};

yFiles = {'01-165386-y.txt' '02-165386-y.txt' '03-165386-y.txt' '04-165386-y.txt' '05-165386-y.txt' ...
    '01-165574-y.txt' '02-165574-y.txt' '03-165574-y.txt' '04-165574-y.txt' '05-165574-y.txt'};

zFiles = {'01-165386-z.txt' '02-165386-z.txt' '03-165386-z.txt' '04-165386-z.txt' '05-165386-z.txt' ...
    '01-165574-z.txt' '02-165574-z.txt' '03-165574-z.txt' '04-165574-z.txt' '05-165574-z.txt'};


numRecords = length(xFiles);
rootDir = 'data/phidgets/';

cPhidgetTrainingSegments = {};
cPhidgetTestingSegments = {};

for i=[1,2,6,7]
   xFile = [rootDir xFiles{i}];
   yFile = [rootDir yFiles{i}];
   zFile = [rootDir zFiles{i}];
   
   uTS = loadPhidgetFiles(xFile, yFile, zFile, sampleRate, countsToMs2);
   newSegments = uTS.segment(segmentLength, targetSampleRate);
   cPhidgetTrainingSegments = [cPhidgetTrainingSegments ; newSegments];
end

for i=[4,5,9,10]
   xFile = [rootDir xFiles{i}];
   yFile = [rootDir yFiles{i}];
   zFile = [rootDir zFiles{i}];
   
   uTS = loadPhidgetFiles(xFile, yFile, zFile, sampleRate, countsToMs2);
   newSegments = uTS.segment(segmentLength, targetSampleRate);
   cPhidgetTestingSegments = [cPhidgetTestingSegments ; newSegments];
end

%%

save('*cache/Oct29/phidgetTrainingSegments.mat', 'cPhidgetTrainingSegments');
save('*cache/Oct29/phidgetTestingSegments.mat','cPhidgetTestingSegments');

%save('*cache/Oct25/phidgetTrainingSegments_ltst.mat', 'cPhidgetTrainingSegments');
%save('*cache/Oct25/phidgetTestingSegments_ltst.mat', 'cPhidgetTestingSegments');

