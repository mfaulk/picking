function [ Y ] = normalize( X)
%normalize Normalize a matrix to be zero mean and unit std dev
%   
% X - (dxn) matrix.
%

% NOTE: if a dimension has zero variance along some component, this will
% lead to numerical problems. Possible solution: discard dimensions that
% have zero variance.

% NOTE: X must be of single or double precision.

% mean of each row
Xmean = mean(X,2);
XstdDev = std(X,0,2);

Y = bsxfun(@rdivide, bsxfun(@minus, X, Xmean), XstdDev);

end



% trainMean = mean(trainSet);
% trainStdDev = std(trainSet);
% 
% % identify dimensions with zero variance, and discard
% positiveVarIndices = find(trainStdDev ~= 0);
% 
% %
% trainSet = trainSet(:,positiveVarIndices);
% trainMean = trainMean(positiveVarIndices);
% trainStdDev = trainStdDev(positiveVarIndices);
% 
% testSet = testSet(positiveVarIndices);
% 
% trainSet = bsxfun(@rdivide, bsxfun(@minus, trainSet, trainMean), trainStdDev);
% testSet =  bsxfun(@rdivide, bsxfun(@minus, testSet, trainMean), trainStdDev);