%% Load the MNIST principal component vectors
load pcs.mat
first_pcs = pcs(:,1:50);
%% Gets the cluster means
res_str = 'results'; %the name of the results structure
K = eval([res_str '.K']);
m = eval([res_str '.posterior.m']);
IMAGE_SIZE = 28;
PCA = 1;
num_cols = 10;
num_rows = ceil((K-1)/10);

%% Display the means
figure;

IMAGE_SIZE = 28;
num_cols = 10;
num_rows = ceil((K-1)/10);

panel = zeros(IMAGE_SIZE*num_rows,IMAGE_SIZE*num_cols);

for t = 1:K-1
  cluster_mean = m(:,t);
  
  projected_vector = first_pcs*cluster_mean + mv';
  the_image = reshape(projected_vector,IMAGE_SIZE,IMAGE_SIZE)';
  the_image(the_image < 0) = 0;
  the_image = the_image/norm(the_image(:));
  
  %the_image = reshape(cluster_mean,7,7);
  
  current_row = ceil(t/num_cols);
  current_col = mod(t,num_cols);
  if current_col == 0
      current_col = num_cols;
  end
    
  panel((current_row-1)*IMAGE_SIZE+1:current_row*IMAGE_SIZE, ...
        (current_col-1)*IMAGE_SIZE+1:current_col*IMAGE_SIZE) = the_image;
  %imagesc(panel),colormap(gray);
  %current_row,current_col
  
end

imagesc(panel),colormap(gray);

hold on; axis equal; axis off;
for i = 0:num_cols %vertical lines
    plot([i*IMAGE_SIZE+.5 i*IMAGE_SIZE+.5],[+.5 num_rows*IMAGE_SIZE+.5]);
end
for i = 0:num_rows %horizontal lines
    plot([+.5 num_cols*IMAGE_SIZE+0.5],[i*IMAGE_SIZE+.5 i*IMAGE_SIZE+.5]);
end
%% Gets the clumps (in MATLAB cell mode, click this cell first then the one
%  above if you want to display the clumps) 
res_str = 'results';
data = eval([res_str '.data']);
D=50;
m = data.sum_x./repmat(data.Nc,D,1);
[foo,ind]=sort(data.Nc,'descend');
m=m(:,ind);
K=length(data.Nc)+1;
IMAGE_SIZE = 28;
PCA = 1;
num_cols = 10;
num_rows = ceil((K-1)/10);

