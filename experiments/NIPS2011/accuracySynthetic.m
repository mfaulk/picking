%% accuracySynthetic
% Compute accuracy (i.e. log likelihood) vs running time for coresets and
% random subsets of different size.
%

% Output: plot of log likelihood vs. running time.
%    Want error bars (2D error bars?)
%
% 1) Create training and testing data sets, drawn from the same
%   distribution.
% 2) For a range of parameters, fit a GMM using a core set, and using a
%   random subset of the data.
% 3) Evaluate the log likelihood of the testing data, under each model.
%   Also record runtime (runtime = corset time + EM time).
% 4) Plot log likelihood vs runtime
%%

clear all
close all
clc

%% Version 2

import gmm.*
import roc.*

if matlabpool('size')==0
    try
        matlabpool open
    catch exception
        disp('Exception opening matlab pool')
        disp(exception)
    end
end


resultDir = 'Results/20';
SAVE_RESULTS = false;

if ~SAVE_RESULTS
    disp(' === NOT SAVING RESULTS === ')
else
    mkdir(resultDir);
end


% synthetic data parameters
d = 10;
k = 10;
separation = 300;
excentricity=1.5;
b = 0.5;

nTraining = 100000;
nTesting = 100000;
%setSizes = [500, 1000, 5000, 10000, 25000];
%setSizes = 2.^(5:15);
setSizes = [32, 40, 50, 60, 70, 80, 90, 100, 125, 150, 175, 200, 300, 500, 1000, 2048, 4096];  

% coreset parameters
coresetParams.k = k;
coresetParams.beta = k*2;

%

model = GmmSphere(d, k, separation, excentricity, b);

Xtrain = model.sample(nTraining);
Xtest = model.sample(nTesting);

% figure()
% plot(Xtrain(1,:),Xtrain(2,:),'.');
% title('Xtrain, first two dimensions')


nIterations = 10;

% Experiment info
ExperimentInfo.d = d;
ExperimentInfo.k = k;
ExperimentInfo.separation = separation;
ExperimentInfo.excentricity = excentricity;
ExperimentInfo.b = b;
ExperimentInfo.nTraining = nTraining;
ExperimentInfo.nTesting = nTesting;
ExperimentInfo.setSizes = setSizes;
ExperimentInfo.coresetParams = coresetParams;
ExperimentInfo.nIterations = nIterations;
if SAVE_RESULTS
    save([resultDir '/ExperimentInfo'], 'ExperimentInfo');
end
% ------------------------------------------------------------------------

[C, S] = AccuracyUtils.accuracyAcrossSetSizeParallel...
    (Xtrain, Xtest, nIterations, coresetParams, setSizes);


ticID = tic;
nRestarts = 3;
[fullsetGmm, fullsetIters] = gmm.Gmm.fitGmmWeighted(Xtrain, ones(1, size(Xtrain,2)), k, nRestarts);
llhFullset = fullsetGmm.logLikelihood(Xtest);
%llhFullset = sum(log(fullsetGmm.evaluateProbability(Xtest)));
fullsetRuntime = toc(ticID);

% sort points by time, to avoid zig-zag plots? This is misleading in its
% own way, since the points don't necessarily get more accurate across
% time...
[C.meanTimeSorted, I] = sort(C.meanTime);
C.meanLLHSorted = C.meanLLH(I);

[S.meanTimeSorted, I] = sort(S.meanTime);
S.meanLLHSorted = S.meanLLH(I);

% log likelihood vs runtime
figure()
plot(C.meanTimeSorted, C.meanLLHSorted, '--x', S.meanTimeSorted, S.meanLLHSorted, '--o', fullsetRuntime, llhFullset, 'rs');
title('GMM-Sphere synthetic: log likelihood vs runtime')
xlabel('runtime, seconds')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/synthetic_llh_vs_runtime_errorbars'],'png');
    saveas(gcf, [resultDir '/synthetic_llh_vs_size_errorbars'],'fig');
end

% log likelihood vs runtime, with error bars
figure()
hold all
errorbar(C.meanTimeSorted, C.meanLLHSorted, C.stdErrLLH, '--x');
set(gca,'XScale','log')
errorbar(S.meanTimeSorted, S.meanLLHSorted, S.stdErrLLH, '-o');
semilogx(fullsetRuntime, llhFullset, 'rs');
title('GMM-Sphere synthetic: LLH vs runtime, with standard error')
xlabel('runtime, seconds')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/synthetic_llh_vs_runtime_errorbars'],'png');
    saveas(gcf, [resultDir '/synthetic_llh_vs_runtime_errorbars'],'fig');
end

% log likelihood vs training set size
figure()
semilogx(setSizes, C.meanLLH, '--x', setSizes, S.meanLLH, '--o', nTraining, llhFullset, 'rs');
title('GMM-Sphere synthetic: LLH vs coreset, subset size')
xlabel('training set size')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/synthetic_llh_vs_size'],'png');
    saveas(gcf, [resultDir '/synthetic_llh_vs_size'],'fig');
end

%
figure()
hold all
errorbar(setSizes, C.meanLLH, C.stdErrLLH, '--x');
set(gca,'XScale','log')
errorbar(setSizes, S.meanLLH, S.stdErrLLH, '--o');
plot(nTraining, llhFullset, 'rs');
title('GMM-Sphere Synthetic: LLH vs  coreset, subset size')
xlabel('training set size')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/synthetic_llh_vs_size_errorbars'],'png');
    saveas(gcf, [resultDir '/synthetic_llh_vs_size_errorbars'],'fig');
end

%LLH vs EM iterations
figure()
plot(C.meanIters, C.meanLLH, '--x', S.meanIters, S.meanLLH, '--o', fullsetIters, llhFullset, 'rs');
title('GMM-Sphere synthetic: log likelihood vs EM iterations')
xlabel('EM iterations')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/synthetic_llh_vs_runtime_errorbars'],'png');
    saveas(gcf, [resultDir '/synthetic_llh_vs_size_errorbars'],'fig');
end


%% Version 1

import gmm.*
import roc.*

% synthetic data parameters
d = 2;
k = 5;
separation = 30;
excentricity=1.5;
b = 0.2;

nTraining = 10000;
nTesting = 10000;
setSizes = [50, 100, 500 1000, 2500, 5000, 10000];


% coreset parameters
%coresetSize = 100;
beta = k*2;
nRestartsWithCoreset = 3; % <== magic number, for EM

% other parameters
nRestartsWithoutCoreset = 3;

% ------------------------------------------------------------------------

model = GmmSphere(d, k, separation, excentricity, b);

trainSet = model.sample(nTraining);
testSet = model.sample(nTesting);


nIterations = 50;

% ------------------------------------------------------------------------

resultDir = 'Results/02';

nSizes = length(setSizes);
coresetLikelihoods = zeros(nIterations, nSizes);
coresetRuntimes = zeros(nIterations, nSizes);

subsetLikelihoods = zeros(nIterations, nSizes);
subsetRuntimes = zeros(nIterations, nSizes);

for j=1:nSizes
    for i=1:nIterations
        setSize = setSizes(j);
        
        ticID = tic; % saving the ID allows nested timing operations
        [coresetPointsTrain, coresetWeightsTrain] = CoresetUtils.computeCoresetKMedian(trainSet, setSize, k, beta);
        
        % fit GMMs to coresets of training data, using wemgm
        coresetGmm = gmm.Gmm.fitGmmWeighted(coresetPointsTrain, coresetWeightsTrain, k, nRestartsWithCoreset);
        
        % compute likelihood ratio for test points, under coreset models
        llhCoreset = sum(log(coresetGmm.evaluateProbability(testSet)));
        
        coresetLikelihoods(i,j) = llhCoreset;
        coresetRuntimes(i,j) = toc(ticID);
        
        % --------------------------------------------------------------------
        
        % random subset
        ticID = tic;
        inds = randperm(nTraining);
        subsetInds = inds(1:setSize);
        randomTrainingSubset = trainSet(:,subsetInds);
        
        subsetGmm = gmm.Gmm.fitGmmWeighted(randomTrainingSubset, ones(1, setSize), k, nRestartsWithoutCoreset);
        
        llhSubset = sum(log(subsetGmm.evaluateProbability(testSet)));
        subsetLikelihoods(i,j) = llhSubset;
        subsetRuntimes(i,j) = toc(ticID);
    end
end

% compute mean and standard deviation. Mean and std operate on each column
% of a matrix

% should check for and throw out infs, nans

coresetLikelihoodStdDevs = std(coresetLikelihoods);
coresetLikelihoodMeans = mean(coresetLikelihoods);
coresetRuntimeMeans = mean(coresetRuntimes);

subsetLikelihoodStdDevs = std(subsetLikelihoods);
subsetLikelihoodMeans = mean(subsetLikelihoods);
subsetRuntimeMeans = mean(subsetRuntimes);

ticID = tic;
fullsetGmm = gmm.Gmm.fitGmmWeighted(trainSet, ones(1, size(trainSet,2)), k, nRestartsWithoutCoreset);
llhFullset = sum(log(fullsetGmm.evaluateProbability(testSet)));
fullsetRuntime = toc(ticID);

mkdir(resultDir);

% log likelihood vs runtime
figure()
plot(coresetRuntimeMeans, coresetLikelihoodMeans,'--x', subsetRuntimeMeans, subsetLikelihoodMeans, '--o', fullsetRuntime, llhFullset, 'rs');
title('GMM-Sphere synthetic: log likelihood vs runtime')
xlabel('runtime, seconds')
ylabel('log likelihood on test data set')
legend({'Corset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
saveas(gcf, [resultDir '/synthetic_llh_vs_runtime_errorbars'],'png');
saveas(gcf, [resultDir '/synthetic_llh_vs_size_errorbars'],'fig');

% log likelihood vs runtime, with error bars
figure()
hold all
errorbar(coresetRuntimeMeans, coresetLikelihoodMeans, coresetLikelihoodStdDevs, '--x');
errorbar(subsetRuntimeMeans, subsetLikelihoodMeans, subsetLikelihoodStdDevs, '-o');
plot(fullsetRuntime, llhFullset, 'rs');
title('GMM-Sphere synthetic: log likelihood vs runtime')
xlabel('runtime, seconds')
ylabel('log likelihood on test data set')
legend({'Corset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
saveas(gcf, [resultDir '/synthetic_llh_vs_runtime_errorbars'],'png');
saveas(gcf, [resultDir '/synthetic_llh_vs_runtime_errorbars'],'fig');

% log likelihood vs training set size
figure()
plot(setSizes, coresetLikelihoodMeans, '--x', setSizes, subsetLikelihoodMeans, '--o', nTraining, llhFullset, 'rs');
title('GMM-Sphere synthetic: log likelihood vs training set size')
xlabel('training set size')
ylabel('log likelihood on test data set')
legend({'Corset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
saveas(gcf, [resultDir '/synthetic_llh_vs_size'],'png');
saveas(gcf, [resultDir '/synthetic_llh_vs_size'],'fig');

%
figure()
hold all
errorbar(setSizes, coresetLikelihoodMeans, coresetLikelihoodStdDevs, '--x');
errorbar(setSizes, subsetLikelihoodMeans, subsetLikelihoodStdDevs, '--o');
plot(nTraining, llhFullset, 'rs');
title('GMM-Sphere Synthetic: log likelihood vs training set size')
legend({'Corset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
saveas(gcf, [resultDir '/synthetic_llh_vs_size_errorbars'],'png');
saveas(gcf, [resultDir '/synthetic_llh_vs_size_errorbars'],'fig');


