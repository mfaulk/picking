%% accuracyNN

% this script should be run within the experiments directory
if matlabpool('size')==0
    try
        matlabpool open
    catch exception
        disp('Exception opening matlab pool')
        disp(exception)
    end
end

import convenience.*
import gmm.*
import roc.*

SAVE_RESULTS = true;
resultDir = 'ResultsNN/03';
if SAVE_RESULTS
    if isdir(resultDir)
        error('resultDir exists. Choose a new result directory');
    end
    mkdir(resultDir);
else
    disp('=== NOT SAVING RESULTS === ')
end


% Load the CSN feature vector matrix. The value trainingMat contains
% "normal" data, while testingMat contains both normal and anomaly. Here,
% the goal is to evaluate each model on a held-out set of normal data. So,
% partition the training data set.

% possibly 4 tetrodes; each sample has length 38. 319209 examples.
% could concatenate all 4 38, or just take 1
% first dimension could be position, second dimension may be time.
%
fname = 'bauer_09_25_2007_20_06_40_tt_14.h5';
waveform = hdf5read(fname, '/tet_14/waveform');
[m,n,p] = size(waveform);

% create data set of only first electrode
%inputFeatures = squeeze(waveform(1,:,:));
inputFeatures = reshape(waveform, m*n, p);

nFeatures = size(inputFeatures,2);
indices = randperm(nFeatures);
disp('subsampling input features...')
Xtrain = inputFeatures(:,indices(1:100000));
Xtest = inputFeatures(:,indices(100001:200000));
clear waveform, inputFeatures;

nTraining = size(Xtrain,2);
nTesting = size(Xtest,2);

% normalize
trainingMean = mean(Xtrain,2);
trainingStdDev = std(Xtrain,0,2);

% Apply the same normalization to both the training and testing data sets
Xtrain = bsxfun(@rdivide, bsxfun(@minus, Xtrain, trainingMean), trainingStdDev);
Xtest =  bsxfun(@rdivide, bsxfun(@minus, Xtest, trainingMean), trainingStdDev);


% coreset parameters
k = 33; % <== how to choose this? there are 10 digits, so how about 10?
coresetParams.k = k;
coresetParams.beta = k*2;

%setSizes = [500, 1000, 5000, 25000];
minSize = 70;
maxSize = 1000;
nSets = 20;
setSizes = floor(10.^linspace(log10(minSize), log10(maxSize), nSets));


nIterations = 10;

if SAVE_RESULTS
    % Experiment info
    ExperimentInfo.nTraining = nTraining;
    ExperimentInfo.nTesting = nTesting;
    ExperimentInfo.setSizes = setSizes;
    ExperimentInfo.coresetParams = coresetParams;
    ExperimentInfo.nIterations = nIterations;
    save([resultDir '/ExperimentInfo'], 'ExperimentInfo');
end
% ------------------------------------------------------------------------

[C, S] = AccuracyUtils.accuracyAcrossSetSizeParallel...
    (Xtrain, Xtest, nIterations, coresetParams, setSizes);

ticID = tic;
nRestarts = 3;
fullsetGmm = gmm.Gmm.fitGmmWeighted(Xtrain, ones(1, size(Xtrain,2)), k, nRestarts);
llhFullset = fullsetGmm.logLikelihood(Xtest);
fullsetRuntime = toc(ticID);



% log likelihood vs runtime
figure()
semilogx(C.meanTime, C.meanLLH, '--x', S.meanTime, S.meanLLH, '--o', fullsetRuntime, llhFullset, 'rs');
title('NN features: LLH vs runtime')
xlabel('runtime, seconds')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/nn_features_llh_vs_runtime_errorbars'],'png');
    saveas(gcf, [resultDir '/nn_features_llh_vs_size_errorbars'],'fig');
end

% log likelihood vs runtime, with error bars
figure()
hold all
errorbar(C.meanTime, C.meanLLH, C.stdErrLLH, '--x');
set(gca,'XScale','log')
errorbar(S.meanTime, S.meanLLH, S.stdErrLLH, '-o');
plot(fullsetRuntime, llhFullset, 'rs');
title('NN features: LLH vs runtime, std Err')
xlabel('runtime, seconds')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/nn_features_llh_vs_runtime_errorbars'],'png');
    saveas(gcf, [resultDir '/nn_features_llh_vs_runtime_errorbars'],'fig');
end

% log likelihood vs training set size
figure()
semilogx(setSizes, C.meanLLH, '--x', setSizes, S.meanLLH, '--o', nTraining, llhFullset, 'rs');
title('NN features: LLH vs training set size')
xlabel('training set size')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/nn_features_llh_vs_size'],'png');
    saveas(gcf, [resultDir '/nn_features_llh_vs_size'],'fig');
end

%
figure()
hold all
errorbar(setSizes, C.meanLLH, C.stdErrLLH, '--x');
set(gca,'XScale','log')
errorbar(setSizes, S.meanLLH, S.stdErrLLH, '--o');
plot(nTraining, llhFullset, 'rs');
title('NN features: LLH vs training set size, std Err')
xlabel('training set size')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/nn_features_llh_vs_size_errorbars'],'png');
    saveas(gcf, [resultDir '/nn_features_llh_vs_size_errorbars'],'fig');
end
