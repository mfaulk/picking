%% anomalyCsn
% Fit GMMs to CSN data and perform anomaly detection
% 
%
% Author: Matt Faulkner

% ------------------------------------------------------------------------
% About the data set.
%
% The raw CSN data is in the form of three-channel accelerometer time
% series. This data is partitioned into segments of several seconds in
% length. Given a segment of time series data, a feature vector describing
% the data is computed.
%
% Synthetic earthquake data is generated by superimposing historical
% seismic accelerometer recordings (SAC files) onto the phone accelerometer
% data.
%
% For these experiments, I'm using the following saved data from the IPSN
% paper
%
% cached segments of phone accelerometer data:
% 
% load('*cache/Oct18/test01/trainingSegments'); 
% load('*cache/Oct18/test01/testingNormalSegments.mat');
% 
% and then compute feature vectors and store the feature vectors in a
% matrix. This matrix can be saved, and used with the coresets. 
% 
% Feature parameters used in the IPSN paper are specified
% in the script makeIpsnPlots.m, e.g. for LtSt features, use 
% 
% disp('Oct. 19, test04. GMM LtSt features (2.5,5), train on B, test on A');
% load('experiments/Oct19/test04/gmm_ltst_switched.mat');
% 
% to load the experimental values, and then 
% 
% >> bestParams
% bestParams = 
%     numFrequencyCoefficients: 16
%                   numMoments: 1
%           dimensionsRetained: 16
%                 numGaussians: 6
% 
% SAC data to use: 
%   data.sacRootDir = 'data/sac/5-5.5__0-40km__HN';
%

% this script should be run within the experiments directory
import gmm.*
import roc.*

if matlabpool('size')==0
    try
        matlabpool open
    catch exception
        disp('Exception opening matlab pool')
        disp(exception)
    end
end


resultDir = 'Results/03';


% Load the CSN feature vector matrix. The value trainingMat contains
% "normal" data, while testingMat contains both normal and anomaly. Here,
% the goal is to evaluate each model on a held-out set of normal data. So,
% partition the training data set.
load('CSN/csn_feature_matrix', 'trainingMat', 'testingMat', 'testingLabels', 'readme');
Xtrain = trainingMat;
Xtest = testingMat;

disp(readme)
clear trainingMat;
clear testingMat;

