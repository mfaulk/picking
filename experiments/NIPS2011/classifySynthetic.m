% Evaluate the effect on anomaly detection and hypothesis detection when
% the training data is reduced via coresets.
%
% author: Matt Faulkner

% Experiment setup
% 
% There are a variety of parameters that we'd want to sweep over. The code
% below could be refactored into functions (e.g. 'evaluateAnomalySynthetic'
% and 'evaluateHypTestSynthetic') which take in parameters. Then, it would
% be straight forward to define different experiments by looping over
% separate parameters, while holding others fixed.

%% Anomaly Detection
% Generate a training data set of "normal" data using one GMM, and a
% testing data set drawn from both the normal model, and an "anomaly" GMM.

% Fit a GMM (via EM) to the full training set of N points, as a baseline. 
% Compute a coreset of size m, and fit a GMM. 
% Also fit a GMM to a random subset of size m.

% Evaluate the test data set under each model to generate likelihoods.
% Sweep a likelihood threshold to produce ROC curves for each model.

% Repeat for various parameter settings. Possible parameters include:
%    N - (full training data set size)
%    m - (size of coreset, and random subset)
%    kTrain, kTest - number of GMM mixture components for the training and
%       testing GMM distributions
%    muTrain, muTest - 
%    sigmaTrain, sigmaTest - (how "spherical" and well-separated are the components)
%    wTrain -  the distribution of weights for the training data set, e.g.
%       exponentially decaying, uniform, maybe a heavy-tailed distribution.
%    D - problem dimensionality


%% Hypothesis Testing

% Generate training data sets:
%
% Generate training data from two different GMMs, called "A" and "B". There
% should be an equal number of data points from each class. Compute a
% coreset for each data set of the training data.

% GMM fitting:
%
% Fit a GMM to the full set of each class of training data. 
% Also fit a GMM to the coreset of each class of training data.

% Testing data:
% Draw testing data sets from GMMs A and B. Perform hypothesis testing
% using the GMMs fitted to the training data. Sweep a threshold to produce
% the "full training data" and "coreset" ROC curves.

% repeat for different parameters.

%randn('state',0);
%rand('state',0);

import gmm.*
import roc.*

d = 2;
k = 5;

separation = 30;
excentricity=1.5;
offset = 1;

nTraining = 10000;
nTesting = 10000;
coresetSize = 100;

b = 0.4;

gmmA = GmmSphere(d, k, separation, excentricity, b);
gmmB = GmmSphere(d, k, separation, excentricity, b, offset);

trainA = gmmA.sample(nTraining);
trainB = gmmB.sample(nTraining);

%plot(trainA(1,:),trainA(2,:),'.', 'Color', [0, 0.9, 0.9])
%plot(trainA(1,:),trainA(2,:),'r.');
%hold on
%plot(trainB(1,:),trainB(2,:),'b.')

% compute coresets of trainA and trainB
beta = k*2;
[coresetPointsTrainA, coresetWeightsTrainA] = CoresetUtils.computeCoresetKMedian(trainA, coresetSize, k, beta);
[coresetPointsTrainB, coresetWeightsTrainB] = CoresetUtils.computeCoresetKMedian(trainB, coresetSize, k, beta);

% fit GMMs to coresets of training data, using wemgm

nRestartsWithCoreset = 3; % <== magic number?

coresetGmmA = gmm.Gmm.fitGmmWeighted(coresetPointsTrainA, coresetWeightsTrainA, k, nRestartsWithCoreset); 
coresetGmmB = gmm.Gmm.fitGmmWeighted(coresetPointsTrainB, coresetWeightsTrainB, k, nRestartsWithCoreset);

nRestartsWithoutCoreset = 3;
fullsetGmmA = gmm.Gmm.fitGmmWeighted(trainA, ones(1, size(trainA,2)), k, nRestartsWithoutCoreset);
fullsetGmmB = gmm.Gmm.fitGmmWeighted(trainB, ones(1, size(trainB,2)), k, nRestartsWithoutCoreset);

% === testing ===

testA = gmmA.sample(nTesting);
testB = gmmB.sample(nTesting);
testPoints = [testA, testB];
TrueLabels = [2*ones(1,nTesting), ones(1,nTesting)];

% compute likelihood ratio for test points, under coreset models
llrCoreset = -log(coresetGmmA.evaluateProbability(testPoints)) + log(coresetGmmB.evaluateProbability(testPoints));
[tp, fp] =  computeROC( TrueLabels, llrCoreset);
figure()
plot(fp,tp)
title('coreset ROC')
xlabel('FPR')
ylabel('TPR')

% compute likelihood ratio for test points, under full data set models
llrFullset = -log(fullsetGmmA.evaluateProbability(testPoints)) + log(fullsetGmmB.evaluateProbability(testPoints));
[tp, fp] =  computeROC( TrueLabels, llrFullset);
figure()
plot(fp,tp)
title('full set ROC')
xlabel('FPR')
ylabel('TPR')












