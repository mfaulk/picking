%% accuracyMNIST
% Compute accuracy (i.e. log likelihood) vs running time for coresets and
% random subsets of different size.
%
% Author: Matt Faulkner

% About the data set:
% MNIST data, formatted by Ryan Gomes. This seems to be 60000 feature
% vectors, each 50 components.


% this script should be run within the experiments directory
parallel.openPool();

import convenience.*
import gmm.*
import roc.*

SAVE_RESULTS = false;
resultDir = 'Results/108';
if SAVE_RESULTS
    if isdir(resultDir)
        error('resultDir exists. Choose a new result directory');
    end
    mkdir(resultDir);
else
    disp('=== NOT SAVING RESULTS === ')
end

% Load the CSN feature vector matrix. The value trainingMat contains
% "normal" data, while testingMat contains both normal and anomaly. Here,
% the goal is to evaluate each model on a held-out set of normal data. So,
% partition the training data set.

load 'experiments/MNIST/nist_features.mat';
nFeatures = size(nist_features,2);
indices = randperm(nFeatures);
Xtrain = nist_features(:,indices(1:30000));
Xtest = nist_features(:,indices(30001:60000));
clear nist_features;

nTraining = size(Xtrain,2);
nTesting = size(Xtest,2);

%setSizes = [500, 1000, 5000, 25000];
setSizes = 2.^(5:14);

% coreset parameters
k = 10; % <== how to choose this? there are 10 digits, so how about 10?
coresetParams.k = k;
coresetParams.beta = k*2;

nIterations = 5;

if SAVE_RESULTS
    % Experiment info
    ExperimentInfo.nTraining = nTraining;
    ExperimentInfo.nTesting = nTesting;
    ExperimentInfo.setSizes = setSizes;
    ExperimentInfo.coresetParams = coresetParams;
    ExperimentInfo.nIterations = nIterations;
    save([resultDir '/ExperimentInfo'], 'ExperimentInfo');
end
% ------------------------------------------------------------------------

[C, S] = AccuracyUtils.accuracyAcrossSetSizeParallel...
    (Xtrain, Xtest, nIterations, coresetParams, setSizes);

ticID = tic;
nRestarts = 3;
fullsetGmm = gmm.Gmm.fitGmmWeighted(Xtrain, ones(1, size(Xtrain,2)), k, nRestarts);
llhFullset = fullsetGmm.logLikelihood(Xtest);
fullsetRuntime = toc(ticID);



% log likelihood vs runtime
figure()
semilogx(C.meanTime, C.meanLLH, '--x', S.meanTime, S.meanLLH, '--o', fullsetRuntime, llhFullset, 'rs');
%title('MNIST features: log likelihood vs runtime')
xlabel('runtime, seconds')
ylabel('log likelihood on test data set')
set(gca, 'FontSize', 16)
h_xlabel = get(gca,'XLabel');
set(h_xlabel,'FontSize',16); 
h_ylabel = get(gca,'YLabel');
set(h_ylabel,'FontSize',16); 
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/mnist_features_llh_vs_runtime_errorbars'],'png');
    saveas(gcf, [resultDir '/mnist_features_llh_vs_size_errorbars'],'fig');
end

% log likelihood vs runtime, with error bars
figure()
hold all
errorbar(C.meanTime, C.meanLLH, C.stdDevLLH, '--x', 'LineWidth', 2);
set(gca,'XScale','log')
errorbar(S.meanTime, S.meanLLH, S.stdDevLLH, '-o', 'LineWidth', 2);
plot(fullsetRuntime, llhFullset, 'rs');
%title('MNIST features: log likelihood vs runtime')
xlabel('runtime, seconds')
ylabel('log likelihood on test data set')
set(gca, 'FontSize', 16)
h_xlabel = get(gca,'XLabel');
set(h_xlabel,'FontSize',16); 
h_ylabel = get(gca,'YLabel');
set(h_ylabel,'FontSize',16); 
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/mnist_features_llh_vs_runtime_errorbars'],'png');
    saveas(gcf, [resultDir '/mnist_features_llh_vs_runtime_errorbars'],'fig');
end

% log likelihood vs training set size
figure()
semilogx(setSizes, C.meanLLH, '--x', setSizes, S.meanLLH, '--o', nTraining, llhFullset, 'rs');
%title('MNIST features: log likelihood vs training set size')
xlabel('training set size')
ylabel('log likelihood on test data set')
set(gca, 'FontSize', 16)
h_xlabel = get(gca,'XLabel');
set(h_xlabel,'FontSize',16); 
h_ylabel = get(gca,'YLabel');
set(h_ylabel,'FontSize',16); 
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/mnist_features_llh_vs_size'],'png');
    saveas(gcf, [resultDir '/mnist_features_llh_vs_size'],'fig');
end

%
figure()
hold all
errorbar(setSizes, C.meanLLH, C.stdDevLLH, '--x', 'LineWidth', 2);
set(gca,'XScale','log')
errorbar(setSizes, S.meanLLH, S.stdDevLLH, '--o', 'LineWidth', 2);
plot(nTraining, llhFullset, 'rs');
%title('MNIST features: log likelihood vs training set size')
xlabel('training set size')
ylabel('LLH'); %log likelihood on test data set')
set(gca, 'FontSize', 16)
h_xlabel = get(gca,'XLabel');
set(h_xlabel,'FontSize',16); 
h_ylabel = get(gca,'YLabel');
set(h_ylabel,'FontSize',16); 
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/mnist_features_llh_vs_size_errorbars'],'png');
    saveas(gcf, [resultDir '/mnist_features_llh_vs_size_errorbars'],'fig');
end


%% Skewed class distributions
%
% TODO: check the PCA implementation. 
%
% load variables test0, ... , test9, train0, ... , train9
% each row is a 28x28 image

if matlabpool('size')==0
    try
        matlabpool open
    catch exception
        disp('Exception opening matlab pool')
        disp(exception)
    end
end


load('experiments/MNIST/mnist_all')

SAVE_RESULTS = false;

resultDir = 'Results/108';
if SAVE_RESULTS
    if isdir(resultDir)
        error('resultDir exists. Choose a new result directory');
    end
    mkdir(resultDir);
else
    disp('=== NOT SAVING RESULTS ===')
end


setSizes = [30, 40, 50, 60, 70, 80, 90, 100, 125, 150, 175, 200, 250, 500, 1000, 2500, 5000 ];
%setSizes = 2.^(5:12);

% coreset parameters
k = 10; % <== how to choose this? there are 10 digits, so how about 10?
coresetParams.k = k;
coresetParams.beta = 2*k;

% number of principal components to retain
d = 100;

nIterations = 5;



trainInputs = {train0, train1, train2, train3, train4, train5, train6, train7, train8, train9};
testInputs = {test0, test1, test2, test3, test4, test5, test6, test7, test8, test9};

% Draw training data from the above training sets according to some
% distribution. 
% NOTE: this assumes that the input sets are of equal size.
classWeights = ones(1,10);
%classWeights = 0.5.^(0:9);
%classWeights = classWeights / sum(classWeights);

% ------------------------------------------------------------------------

% create training and testing sets
trainSet = [];
trainLabels = [];
testSet = [];
testLabels = [];
for i=1:10
    s = trainInputs{i};
    n = size(s,1); % number of images in this set
    w = classWeights(i);
    
    indices = randperm(n);
    trainingPointsTaken = ceil(n*w);
    trainSet = vertcat(trainSet, s(indices(1:trainingPointsTaken), :) );
    clear('indices', 's', 'n');
    
    newTrainingLabels = (i-1) * ones(trainingPointsTaken,1);
    trainLabels = vertcat(trainLabels, newTrainingLabels);
    
    % testing
    
    s = testInputs{i};
    n = size(s,1);
    indices = randperm(n);
    testingPointsTaken = ceil(n*w);
    testSet = vertcat(testSet, s(indices(1:testingPointsTaken), :) );
    
    newTestingLabels = (i-1) * ones(testingPointsTaken,1);
    testLabels = vertcat(testLabels, newTestingLabels);
    
end

% ------------------------------------------------------------------------
% Normalize and prepare input data.

% the image data is uint8. Convert to double.
trainSet = double(trainSet);
testSet = double(testSet);

% identify dimensions with zero variance, and discard
trainStdDev = std(trainSet);
positiveVarIndices = find(trainStdDev ~= 0);

trainSet = trainSet(:,positiveVarIndices);
testSet = testSet(:,positiveVarIndices);

% normalize
trainMean = mean(trainSet);
trainStdDev = std(trainSet);

trainSet = bsxfun(@rdivide, bsxfun(@minus, trainSet, trainMean), trainStdDev);
testSet =  bsxfun(@rdivide, bsxfun(@minus, testSet, trainMean), trainStdDev);

%randomly permute the trainset?
iTemp = randperm(size(trainSet,1));
trainSet = trainSet(iTemp,:);
clear iTemp
% ------------------------------------------------------------------------

% take top d principal components to get the feature set. The rows of the
% input to princomp are the datapoints.
[C, score] = princomp(trainSet); 
Xtrain = score(:,1:d)';

scoreTest = testSet*C;
Xtest = scoreTest(:,1:d)'; % transpose so each column is a data point

% ------------------------------------------------------------------------

nTraining = size(Xtrain,2);
nTesting = size(Xtest,2);

if SAVE_RESULTS
% Experiment info
ExperimentInfo.nTraining = nTraining;
ExperimentInfo.nTesting = nTesting;
ExperimentInfo.setSizes = setSizes;
ExperimentInfo.coresetParams = coresetParams;
ExperimentInfo.nIterations = nIterations;
ExperimentInfo.d = d;
ExperimentInfo.classWeights = classWeights;
save([resultDir '/ExperimentInfo'], 'ExperimentInfo');
end
% ------------------------------------------------------------------------

[C, S] = AccuracyUtils.accuracyAcrossSetSizeParallel...
    (Xtrain, Xtest, nIterations, coresetParams, setSizes);

ticID = tic;
nRestarts = 3;
fullsetGmm = gmm.Gmm.fitGmmWeighted(Xtrain, ones(1, size(Xtrain,2)), k, nRestarts);
fullsetRuntime = toc(ticID);
llhFullset = fullsetGmm.logLikelihood(Xtest);




% log likelihood vs runtime
figure()
semilogx(C.meanTime, C.meanLLH, '--x', S.meanTime, S.meanLLH, '--o', fullsetRuntime, llhFullset, 'rs');
title('MNIST features: log likelihood vs runtime')
xlabel('runtime, seconds')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/mnist_features_llh_vs_runtime_errorbars'],'png');
    saveas(gcf, [resultDir '/mnist_features_llh_vs_size_errorbars'],'fig');
end

% log likelihood vs runtime, with error bars
figure()
hold all
errorbar(C.meanTime, C.meanLLH, C.stdErrLLH, '--x');
set(gca,'XScale','log')
errorbar(S.meanTime, S.meanLLH, S.stdErrLLH, '-o');
plot(fullsetRuntime, llhFullset, 'rs');
title('MNIST features: LLH vs runtime. Standard Error.')
xlabel('runtime, seconds')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/mnist_features_llh_vs_runtime_errorbars'],'png');
    saveas(gcf, [resultDir '/mnist_features_llh_vs_runtime_errorbars'],'fig');
end

% log likelihood vs training set size
figure()
semilogx(setSizes, C.meanLLH, '--x', setSizes, S.meanLLH, '--o', nTraining, llhFullset, 'rs');
title('MNIST features: log likelihood vs training set size')
xlabel('training set size')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/mnist_features_llh_vs_size'],'png');
    saveas(gcf, [resultDir '/mnist_features_llh_vs_size'],'fig');
end

%
figure()
hold all
errorbar(setSizes, C.meanLLH, C.stdErrLLH, '--x');
set(gca,'XScale','log')
errorbar(setSizes, S.meanLLH, S.stdErrLLH, '--o');
plot(nTraining, llhFullset, 'rs');
title('MNIST features: LLH vs training set size. Standard Error.')
xlabel('training set size')
ylabel('log likelihood on test data set')
legend({'Coreset', 'Random Subset', 'Full Set'}, 'Location', 'Southeast')
if SAVE_RESULTS
    saveas(gcf, [resultDir '/mnist_features_llh_vs_size_errorbars'],'png');
    saveas(gcf, [resultDir '/mnist_features_llh_vs_size_errorbars'],'fig');
end

%%

img = train0(1,:);
imshow(reshape(img,28,28))


