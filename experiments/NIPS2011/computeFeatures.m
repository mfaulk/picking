%% Pre-process CSN data for NIPS 2011 coreset experiments
%

%% LtSt Feature vectors for anomaly detection
% 

% The CSN data needs to
% be presented as a matrix of feature vectors, or alternatively a matrix
% of time series. What data set to use? From previous experiments, I
% have cached several large (~1GB) .mat files. What's in these?
% 
% I'll want to load the cached segments of phone data,
% \begin{verbatim}
% load('*cache/Oct18/test01/trainingSegments'); 
% load('*cache/Oct18/test01/testingNormalSegments.mat');
% \end{verbatim}
% and then compute feature vectors and store the feature vectors in a
% matrix. This matrix can be saved, and used with the coresets. Feature
% parameters? What were good parameters from IPSN? These are specified
% in the script \verb=makeIpsnPlots.m=, e.g. for LtSt features, use 
% \begin{verbatim}
% disp('Oct. 19, test04. GMM LtSt features (2.5,5), train on B, test on A');
% load('experiments/Oct19/test04/gmm_ltst_switched.mat');
% \end{verbatim}
% to load the experimental values, and then 
% \begin{verbatim}
% >> bestParams
% bestParams = 
%     numFrequencyCoefficients: 16
%                   numMoments: 1
%           dimensionsRetained: 16
%                 numGaussians: 6
% \end{verbatim}
%
% SAC data to use: 
%   data.sacRootDir = 'data/sac/5-5.5__0-40km__HN';
%
import check.*
import feature.*
import phone.*
import sac.*
import gmm.*
import synthetic.*
import parallel.*
import roc.*


% get parameters
load('experiments/Oct19/test04/gmm_ltst_switched.mat');

nFFT = bestParams.numFrequencyCoefficients;
nMoms = bestParams.numMoments;
%nGaussians = bestParams.numGaussians;
dimensionsRetained = bestParams.dimensionsRetained;


ltLength = parameters.ltLength;
stLength = parameters.stLength;
deadTime = 1; % seconds of "dead time" between short term and long term buffers
segmentLength = ltLength + stLength + deadTime;

sampleRate = parameters.sampleRate;
threshold = parameters.threshold;
peakAmplitude = parameters.peakAmplitude;

% load phone segments for LtSt features

load('*cache/Oct18/test01/trainingSegments'); 
load('*cache/Oct18/test01/testingNormalSegments.mat');

% load SAC data segments

%sacRootDir = 'data/sac/5-5.5__0-40km__HN';
sacRootDir = 'data/sac/6-8__HN'

cSacRecords = SacLoader.loadSacRecords(sacRootDir);



% -------------------------------------------------------------
% Subsample the phone segments.
% If the data
% set of segments is too large, Matlab will be unable to
% serialize (not enough memory?) and will not be able to use
% parfor.

maxTrainingSegments = 40000;
maxTestingSegments = 40000;


numTotalTrainingSegments = length(cTrainingSegments);
numTotalTestingSegments = length(cTestingNormalSegments);

trainingIndicesSubset = random.randomSubset(numTotalTrainingSegments, maxTrainingSegments);
testingIndicesSubset = random.randomSubset(numTotalTestingSegments, maxTestingSegments);

cTrainingSegments = cTrainingSegments(trainingIndicesSubset);
cTestingNormalSegments = cTestingNormalSegments(testingIndicesSubset);

% LOOK: I'm switching them:
temp = cTrainingSegments;
cTrainingSegments = cTestingNormalSegments;
cTestingNormalSegments = temp;
clear temp;
% -------------------------------------------------------------

cSyntheticSegments= ...
    SegmentSynthesizer.createOnsetSegments(cTestingNormalSegments, cSacRecords, ...
    ltLength+deadTime, stLength, threshold, peakAmplitude, sampleRate);

 % compute training features. This set should be large enough that it can
 % be partitioned if needed to create a training set and a held-out
 % validation set (e.g. for model selection).
 
 [cTrainingFeatures, C, mu, sigma ]= ...
     GmLtStParameterSelection.computeTrainingLtStFeatures(cTrainingSegments, ltLength, stLength, nFFT, nMoms, dimensionsRetained, 2); % 2 is the "normal label"
 
 % this computes a bunch of labelled normal and anomaly features for
 % testing anomaly detection or a classifier.
 cTestingFeatures = ...
     GmLtStParameterSelection.computeTestingLtStFeatures...
     (cTestingNormalSegments, cSyntheticSegments, ltLength, stLength, nFFT, nMoms, C, mu, sigma, dimensionsRetained);
 % these are the labels for the testing features
 testingLabels = Feature.unpackLabels(cTestingFeatures);
 
 
% store features in a matrix
getFeatureVector = @(c) c.featureVector';
A = cellfun(getFeatureVector, cTrainingFeatures, 'UniformOutput', 0);
trainingMat = cell2mat(A)';
clear A;

A = cellfun(getFeatureVector, cTestingFeatures, 'UniformOutput', 0);
testingMat = cell2mat(A)';



% save the results, and maybe some meta-data?
readme = 'experiments/Oct19/test04/gmm_ltst_switched.mat, *cache/Oct18/test01/trainingSegments, *cache/Oct18/test01/testingNormalSegments.mat, data/sac/5-5.5__0-40km__HN';
savefile = 'experiments/NIPS2011/csn_baja_feature_matrix.mat';
save(savefile, 'trainingMat', 'testingMat', 'testingLabels','readme');


