%% anomalyMNIST
% Perform anomaly detection, using one some of the MNIST digit classes
% as "normal" data, and other digits as anomalies.
%
% author: Matt Faulkner
%
import parallel.*

parallel.openPool();

import convenience.*
import gmm.*
import roc.*

% -----------------------------------------------------------------------

load('experiments/MNIST/mnist_all')

SAVE_RESULTS = false;

resultDir = 'ResultsMNIST/03';
if SAVE_RESULTS
    if isdir(resultDir)
        error('resultDir exists. Choose a new result directory');
    end
    mkdir(resultDir);
else
    disp('=== NOT SAVING RESULTS ===')
end

% -----------------------------------------------------------------------

setSizes = [100, 250, 500, 750, 1000, 2000, 4000, 5900];
%setSizes = 2.^(5:12);

% coreset parameters
k = 2; 
coresetParams.k = k;
coresetParams.beta = 2*k;

% number of principal components to retain
d = 10;

nIterations = 2;

% -----------------------------------------------------------------------
% Training and Testing Data Sets
% Conventions:
%   -- "normal" data has label = 2, while "anomaly" (or "positive") has
%       label = 1

% create training data set

trainInputs = {train0};
trainSet = [];

for i=1:length(trainInputs);
    s = trainInputs{i};
    trainSet = vertcat(trainSet, s); % each row of the MNIST data is an image.
end
trainLabels = 2 * ones(size(trainSet,1),1);

% create test set

testNormalInputs = {test0};
testAnomalyInputs = {test1};

testSet = [];
testLabels = [];
for i=1:length(testNormalInputs)
    s= testNormalInputs{i};
    n = size(s,1);
    testSet = vertcat(testSet,s);
    testLabels = vertcat(testLabels, 2*ones(n,1));
end

for i=1:length(testAnomalyInputs)
    s= testAnomalyInputs{i};
    testSet = vertcat(testSet,s);
    n = size(s,1);
    testLabels = vertcat(testLabels, ones(n,1));
end

% ------------------------------------------------------------------------

% Normalize and prepare input data.

% the image data is uint8. Convert to double.
trainSet = double(trainSet);
testSet = double(testSet);

% identify dimensions with zero variance, and discard
trainStdDev = std(trainSet);
positiveVarIndices = find(trainStdDev ~= 0);

trainSet = trainSet(:,positiveVarIndices);
testSet = testSet(:,positiveVarIndices);

% normalize
trainMean = mean(trainSet);
trainStdDev = std(trainSet);

trainSet = bsxfun(@rdivide, bsxfun(@minus, trainSet, trainMean), trainStdDev);
testSet =  bsxfun(@rdivide, bsxfun(@minus, testSet, trainMean), trainStdDev);

% ------------------------------------------------------------------------

% take top d principal components to get the feature set. The rows of the
% input to princomp are the datapoints.
[C, score] = princomp(trainSet); 
Xtrain = score(:,1:d)'; % transpose so each column is a data point

scoreTest = testSet*C;
Xtest = scoreTest(:,1:d)'; % transpose so each column is a data point

% ------------------------------------------------------------------------

nTraining = size(Xtrain,2);
nTesting = size(Xtest,2);

if SAVE_RESULTS
% Experiment info
ExperimentInfo.nTraining = nTraining;
ExperimentInfo.nTesting = nTesting;
ExperimentInfo.setSizes = setSizes;
ExperimentInfo.coresetParams = coresetParams;
ExperimentInfo.nIterations = nIterations;
ExperimentInfo.d = d;
%ExperimentInfo.classWeights = classWeights;
save([resultDir '/ExperimentInfo'], 'ExperimentInfo');
end
% ------------------------------------------------------------------------

% NOTE: here I train and test on the same set, which shouldn't matter since
% I'm not going to use the resulting LLH values in this experiment.
[C, S] = AccuracyUtils.accuracyAcrossSetSizeParallel...
    (Xtrain, Xtrain, nIterations, coresetParams, setSizes);

nRestarts = 3;
fullsetGmm = gmm.Gmm.fitGmmWeighted(Xtrain, ones(1, size(Xtrain,2)), k, nRestarts);


% ------------------------------------------------------------------------

% data points for anomaly testing
Xa = Xtest;

% testing labels
Ya = testLabels;

nSizes = length(setSizes);

[ cAUC, cMeanAUC, cStdErrAUC ] = AccuracyUtils.aucParallel(C, Xa, Ya);
[ sAUC, sMeanAUC, sStdErrAUC ] = AccuracyUtils.aucParallel(S, Xa, Ya);


%fullsetAssignments = -log(fullsetGmm.evaluateProbability(Xa));
fullsetAssignments = - fullsetGmm.evaluateLogProbability(Xa);
infIndices = find(fullsetAssignments == inf);
if sum(infIndices) > 0
    warning(['AccuracyUtils.aucParallel: encountered ' num2str(sum(infIndices)) ' infinite assignments.']);
end
fullsetAssignments(infIndices) = -1000;

nThresh = 1000;
   
fullsetThresholds = linspace(min(fullsetAssignments), max(fullsetAssignments), nThresh);
[fullsetTP, fullsetFP] =  myROC( Ya, fullsetAssignments, fullsetThresholds );
fullsetAUC = auc(fullsetFP, fullsetTP)



figure()
hold all
errorbar(setSizes, cMeanAUC, cStdErrAUC, '--x');
set(gca,'XScale','log')
errorbar(setSizes, sMeanAUC, sStdErrAUC, '--o');
plot(size(Xtrain,2), fullsetAUC, 'rs');
%title('CSN: AUC vs set size');
xlabel('set size')
ylabel('Area under ROC curve');
if SAVE_RESULTS
    saveas(gcf, [resultDir '/mnist_anomaly'],'png');
    saveas(gcf, [resultDir '/mnist_anomaly'],'fig');
end



