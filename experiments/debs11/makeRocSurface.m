
%% The awesome plot
% This makes a surface from a set of ROC curves, and takes a slice through
% them corresponding to a maximum false positive rate. 
%
% Input:
%   sensor operating point    
%   system false positive constraint
%   vector of number of sensors per "band"

clear all;

import aggregate.*
import roc.*


load 'experiments/debs11/geoband.mat';

% I multiply these values by 2 for Android, so that detection performance
% is near 1 when all sensors report.
%
% For phidget, divide down by, say, 10

%nSensor = cumsum(geoband.central)*2;
%tag = 'centralAndroid';

nSensor = cumsum(geoband.corner)*2;
tag = 'cornerAndroid';

%nSensor = cumsum(geoband.side)*2;
%tag = 'sideAndroid';

%nSensor = floor(cumsum(geoband.central) / 50);
%tag = 'centralPhidget';

%nSensor = floor(cumsum(geoband.corner) / 50);
%tag = 'cornerPhidget';

% nSensor = floor(cumsum(geoband.side) / 50);
% tag = 'sidePhidget';

% use the script chooseOperatingPoint to choose these values

% Android operating point 
truePositiveSensor = 0.017852;
falsePositiveSensor = 0.003323; % one per 5 minutes

% Phidget  
% truePositiveSensor = 0.545000;
% falsePositiveSensor =  0.000100;


secondsInaYear = 31556926;
timeStep = 1; % seconds
falsePositivesPerYear = 24;

% limit on system false positive rate
FpMax = falsePositivesPerYear * (timeStep / secondsInaYear); 


fprintf('----------------------------------------------\n')
fprintf('system FP per year: %d\n', falsePositivesPerYear)
fprintf('system max FP: %1.9f\n\n', FpMax);

fprintf('Android TP per day: %d\n', truePositiveSensor)
fprintf('Android FP per day: %d\n', falsePositiveSensor)


fprintf('----------------------------------------------\n')

% now, for each number of sensors, find the best system level threshold,
% and generate a system ROC curve

cCellRoc = cell(length(nSensor),1);


for i=1:length(nSensor)
    n = nSensor(i);
        
    cellROC = computeCellROC(n, truePositiveSensor, falsePositiveSensor);
    
    disp(['Number of sensors experiencing event: ' num2str(n)])
    
    cCellRoc{i} = cellROC;
    
end

% now the magic. Make a surface. This can be done by getting the true
% positive rates for all curves at a fixed vector of false positive rates.
% These true positive rates become a matrix that is passed to surf.

fPoints = linspace(0,1,100)';
nCurves = length(nSensor);
pValues = zeros(length(fPoints), nCurves);

% the best obtainable true positive rate, given the system-level false
% positive constraint
bestTp = zeros(nCurves,1);


for i=1:nCurves
   roc = cCellRoc{i};
   pPoints = roc.interpolateTruePositiveRate(fPoints);
   pValues(:,i) = pPoints;
   pValues(1,i) = 0;
   bestTp(i) = roc.interpolateTruePositiveRate(FpMax);
end

times = (0:nCurves-1)*2;
p = surf(times, fPoints, pValues);
axis([0, times(end), 0, 1, 0, 1])
set(gca,'XTick',times)
%title(['System performance over time: ' tag])
h = get(gca, 'title');
set(h, 'FontSize', 16)
% %set(p,'LineWidth',2)
set(gca, 'FontSize', 16)


xlabel('seconds');
ylabel('False Alarm Rate');
zlabel('Detection Rate')
hold on

%bestTp_lifted = bestTp + max(bestTp)*0.05; 
%plot3(1:nCurves, FpMax*ones(nCurves,1), bestTp_lifted, '-k', 'LineWidth', 2 )

saveas(gcf, ['experiments/debs11/plots/roc_surface_' tag], 'fig')
saveas(gcf, ['experiments/debs11/plots/roc_surface' tag], 'png')
saveas(gcf, ['experiments/debs11/plots/roc_surface' tag], 'pdf')

%
%

figure()
plot(1:nCurves, bestTp)
axis([1,nCurves, 0,1])
h = get(gca, 'title');
set(h, 'FontSize', 16)
% %set(p,'LineWidth',2)
set(gca, 'FontSize', 16)
%title('Obtainable performance')
xlabel('seconds');
ylabel('Detection Rate')

saveas(gcf, ['experiments/debs11/plots/obtainableTPR_' tag], 'fig')
saveas(gcf, ['experiments/debs11/plots/obtainableTPR_' tag], 'png')

results.nSensor = nSensor;
results.truePositiveSensor = truePositiveSensor;
results.falsePositiveSensor = falsePositiveSensor;
results.falsePositivesPerYear = falsePositivesPerYear;
results.obtainableTPR = bestTp;
save(['experiments/debs11/' tag '_obtainableTPR'], 'results');

% Take a slice, corresponding to the maximum true positive rate of each ROC
% curve s.t. the false positive rate is below some constraint.
%
% This can be done by creating a vector of interpolation points using
% interp2. These points can be highlighted on the surface using plot3.



% I found some discussion of getting a cross section here:
% http://compgroups.net/comp.soft-sys.matlab/Section-of-surface
%
% x (1xn)
% y (mx1)
% Z (nxm)
% 
% [X,Y] = meshgrid(x,y);
% w = interp2(X,Y,Z,x,<y-coordinate of section>);
% plot(x,w) 
%

% the slice command could also be useful.

% as for highlighting the cross section on the surface, I wonder if I
% computed the cross section in the same format (e.g. an x vector, y
% vector, and z vector) if I could plot this on the same figure, and use a
% different marker or something to make it bold?

%% Plot best TPR curves on one plot
clear all;

% naive baseline
baseline = load('experiments/debs11/associate_pick_phidget_android.mat');

% side = load('experiments/debs11/sideAndroid_obtainableTPR.mat');
% corner = load('experiments/debs11/cornerAndroid_obtainableTPR.mat');
% central = load('experiments/debs11/centralAndroid_obtainableTPR.mat');
% naive = baseline.android_p1;
% tag = 'android';

side = load('experiments/debs11/sidePhidget_obtainableTPR.mat');
corner = load('experiments/debs11/cornerPhidget_obtainableTPR.mat');
central = load('experiments/debs11/centralPhidget_obtainableTPR.mat');
naive = baseline.phidget_p1;
tag = 'phidget';



% number of time steps
nSteps= length(side.results.nSensor);
times = (0:nSteps-1) * 2; 

tpr = [central.results.obtainableTPR, side.results.obtainableTPR,  corner.results.obtainableTPR];
figure()

p = plot(times, tpr, times, naive', '-*');

% p = plot(times, side.results.obtainableTPR, ...
%     times,  corner.results.obtainableTPR, ...
%     times, central.results.obtainableTPR, ...
%     times, naive', '-*');

h = get(gca, 'title');
set(h, 'FontSize', 16)
set(p,'LineWidth',2)
set(gca, 'FontSize', 16)
%title(['Obtainable Performance: ' tag])
xlabel('seconds');
ylabel('Detection Rate')
legend('Central','Side', 'Corner',  'Naive', 'Location', 'SouthEast')

saveas(gcf, ['experiments/debs11/plots/aggregateObtainableTPR_' tag], 'fig')
saveas(gcf, ['experiments/debs11/plots/aggregateObtainableTPR_' tag ], 'png')
saveas(gcf, ['experiments/debs11/plots/aggregateObtainableTPR_' tag ], 'pdf')
