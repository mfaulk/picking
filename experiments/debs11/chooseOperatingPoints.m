%% Choose operating points
% Given the following:
%   sensor ROC curve
%   constraint on maximum sensor false positive rate
%   number of sensors total
%   fraction of sensors typically experiencing quake
%   constraint on system false positive rate
% compute the sensor operating point and system operating point
%
% This computation does not use "cells" in the sense of the IPSN paper.
clear all;

import aggregate.*
import roc.*

% parameters

% for Android
%nSensor = [50 160 300 500 1000];

% for Phidgets
nSensor = [1,2,3,4,5];

secondsInaYear = 31556926;
secondsInaDay = 86400;
timeStep = 1; % seconds. Time step of picking algorithm.

% limit on system false positive rate
falsePositivesPerYear = 24; % system-level
FpMax = falsePositivesPerYear * (timeStep / secondsInaYear); 

% limit on android false positive rate. 
% sensorMessagesPerDay = 288; % 1 message every 5 minutes. For Android.
% sFpMax =  sensorMessagesPerDay * (timeStep / secondsInaDay); 

% limit on phidget false positive rate
sensorMessagesPerDay = 24; % 1 message every 5 minutes. For Android.
sFpMax =  sensorMessagesPerDay * (timeStep / secondsInaDay); 

fprintf('----------------------------------------------\n')
fprintf('system FP per year: %d\n', falsePositivesPerYear)
fprintf('system max FP: %1.20f\n\n', FpMax);

fprintf('Sensor FP per day: %d\n', sensorMessagesPerDay)
fprintf('Sensor max FP: %f\n\n', sFpMax);

fprintf('----------------------------------------------\n')

% Load ROC curve
% 
% disp('GMM LtSt on Android data')
% rocData = 'experiments/debs11/performance_40-50.mat';
% disp(['using sensor ROC curve: ' rocData]);
% load(rocData)
% 
% sensor_roc = ReceiverOperatingCharacteristic(tp, fp);
% sensor_roc = sensor_roc.convexHull();
% ReceiverOperatingCharacteristic.plot(sensor_roc)

disp('Using Phidget Data: STA/LTA ROC from Annie')
load 'experiments/debs11/phidgetDistROC.mat'

sensor_roc = ReceiverOperatingCharacteristic(tpr_40_50', fpr_40_50');
ReceiverOperatingCharacteristic.plot(sensor_roc)

fprintf('----------------------------------------------\n')



cCellRoc = cell(length(nSensor),1);
TP = zeros(length(nSensor),1);
FP = zeros(length(nSensor),1);
SensorTP = zeros(length(nSensor),1);
SensorFP = zeros(length(nSensor),1);

for i=1:length(nSensor)
    n = nSensor(i);
    
    [ tp, fp, cellROC, sensorTP, sensorFP] = ...
        optimizeCellTruePositive( n, sensor_roc, sFpMax, FpMax);
    
    
    disp(['Number of sensors experiencing event: ' num2str(n)])
    fprintf('system operating point: (%f, %f)\n' , tp, fp);
    fprintf('sensor operating point: (%f, %f)\n' , sensorTP, sensorFP);
    fprintf('\n')
    
    cCellRoc{i} = cellROC;
    TP(i) = tp;
    FP(i) = fp;
    SensorTP(i) = sensorTP;
    SensorFP(i) = sensorFP;
    
end

%
% plot all the ROC curves on the same figure.
%
% Could put the true positive and false positive rates into matrices, and
% plot. I'm not sure how to get different line styles for each curve.
%
% Could also use hold on, but then I think it will be harder to get the
% legend right (but I may use arrows to label curves, rather that legends

figure()
for i=1:length(nSensor)
   roc = cCellRoc{i};
   tpr = roc.truePositiveRates;
   fpr = roc.falsePositiveRates;
   plot(fpr,tpr)
   % semilogx might be more informative, but is hard to read.
   %semilogx(fpr, tpr)
   hold on
end
title('Fusion ROC for N Sensors')
xlabel('False Alarm Rate')
ylabel('Detection Rate')

