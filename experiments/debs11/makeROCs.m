%% Generate ROC curves for the SAC data sets from Annie

import check.*
import sac.*
import gmm.*
import synthetic.*
import feature.*
import roc.*

% ---> Might want to open a MatlabPool. 

% The cached values for Oct 18, test 01 are for:
%    GMM LtSt features (2.5s, 5s) train A test B

% Data 

%rootDir = '/home/mfaulkne/projects/csn/android_activities/Picking/';
rootDir = '';
% load cached testing normal segments (I actually trained using the
% "testing" segments, so the "training" segments get used for testing. See
% GmLtStParameterSelection for where this switch took place.
load('*cache/Oct18/test01/trainingSegments');

% --- root directory of SAC e,n,z data channels.
%sacDir = 'data/debs/5-6_HN_0-10km';
%dataLabel = '0-10'; % used for plot title, saved data, etc.
%threshold = 0.5; %m/s^2 for SAC event extraction

%sacDir = 'data/debs/5-6_HN_20-30km';
%dataLabel = '20-30'; % used for plot title, saved data, etc.
%threshold = 0.3; %m/s^2 for SAC event extraction

%sacDir = 'data/debs/5-6_HN_40-50km';
%dataLabel = '40-50'; % used for plot title, saved data, etc.
%threshold = 0.3; %m/s^2 for SAC event extraction

%sacDir = 'data/debs/5-6_HN_70-80km';
%dataLabel = '70-80'; % used for plot title, saved data, etc.
%threshold = 0.2; %m/s^2 for SAC event extraction

sacDir = 'data/debs/5-6_HN_90-100km';
dataLabel = '90-100'; % used for plot title, saved data, etc.
threshold = 0.05; %m/s^2 for SAC event extraction

sacRootDir = [rootDir sacDir];
assertDirectory(sacRootDir);

% Parameters

% load values from Oct 28, test 05 experiment, which used the same data as
% the Oct.18 test 01, but also saved the PCA parameters.
%
% bestParams are the parameters of the best GMM
% parameters contains the parameter ranges, and feature parameters
load('experiments/debs11/gmm_ltst.mat', 'parameters', 'bestParams');

ltLength = parameters.ltLength;
stLength = parameters.stLength;
deadTime = 1; % seconds of "dead time" between short term and long term buffers
segmentLength = ltLength + stLength + deadTime;

sampleRate = parameters.sampleRate;

% these might need to be specified for each data set. They are used in
% extracting SAC events for synthetic test data. In fact, I commented out the 
% peakAmplitude scaling in the SegmentSynthesizer
%
%threshold = parameters.threshold;
%peakAmplitude = parameters.peakAmplitude;
peakAmplitude = 1;

C = bestParams.C;
mu = bestParams.mu;
sigma = bestParams.sigma;
dimensionsRetained = bestParams.dimensionsRetained;
nFFT = bestParams.numFrequencyCoefficients;
nMoms = bestParams.numMoments;
gm = bestParams.gm;


% ------------------------------------------------------------------------
disp('Create testing segments from the supplied SAC data.')
% Can use the cached normal segments to avoid the costly 
% Android data processing steps.

% Subsample the phone segments.
% If the data
% set of segments is too large, Matlab will be unable to
% serialize (not enough memory?) and will not be able to use
% parfor.

cTestingNormalSegments = cTrainingSegments; % see note above about the test/train switch...
clear('cTrainingSegments');
maxTestingSegments = 10000;
numTotalTestingSegments = length(cTestingNormalSegments);
testingIndicesSubset = random.randomSubset(numTotalTestingSegments, maxTestingSegments);
cTestingNormalSegments = cTestingNormalSegments(testingIndicesSubset);

% ------------------------------------------------------------------------
disp('Make synthetic test data')
cSacRecords = SacLoader.loadSacRecords(sacRootDir);

% Create the correct onset segments
cSyntheticSegments= ...
    SegmentSynthesizer.createOnsetSegments(cTestingNormalSegments, cSacRecords, ...
    ltLength+deadTime, stLength, threshold, peakAmplitude, sampleRate);

disp('Create testing segments')

cTestingFeatures = ...
    GmLtStParameterSelection.computeTestingLtStFeatures(cTestingNormalSegments, cSyntheticSegments, ltLength, stLength, nFFT, nMoms, C, mu, sigma, dimensionsRetained);

% ------------------------------------------------------------------------
disp('apply GM to testing features, and compute ROC curve')

% evaluate training data
P = gm.evaluateProbability(cTestingFeatures);

TrueLabels = Feature.unpackLabels(cTestingFeatures);

Assignments = -log(P);

% a low hack to deal with the infinite!
infIndices = (Assignments == inf);
Assignments(infIndices) = 1000;
nThresh = 1000;

Thresholds = linspace(min(Assignments), max(Assignments), nThresh);

[tp, fp] =  myROC( TrueLabels, Assignments, Thresholds );
areaUnderCurve = auc(fp,tp);

% ------------------------------------------------------------------------

% Plots

figure()
p = plot(fp,tp);
grid on
title(['ROC of GMM, ' sacDir])

h = get(gca, 'title');
set(h, 'FontSize', 16)
set(p,'LineWidth',2)
set(gca, 'FontSize', 16)

xlabel('False Positive Rate')
ylabel('True Positive Rate')
saveas(gcf, ['experiments/debs11/plots/roc_' dataLabel], 'fig')
saveas(gcf, ['experiments/debs11/plots/roc_' dataLabel], 'png')

% ------------------------------------------------------------------------
% Save the plot data

save(['experiments/debs11/performance_' dataLabel '.mat'], 'fp', 'tp', 'areaUnderCurve');

%save('performance 0-10km.mat', 'fp', 'tp', 'areaUnderCurve');
%save('performance 20-30km.mat', 'fp', 'tp', 'areaUnderCurve');
%save('performance 40-50km.mat', 'fp', 'tp', 'areaUnderCurve');
%save('performance 70-80km.mat', 'fp', 'tp', 'areaUnderCurve');
%save('performance 90-100km.mat', 'fp', 'tp', 'areaUnderCurve');
                
                
%% Put all the same data on one plot
root = 'experiments/debs11/';

load([root 'performance_0-10.mat'], 'tp', 'fp', 'areaUnderCurve');
tp_0_10 = tp;
fp_0_10 = fp;
auc_0_10 = areaUnderCurve;
                
clear('tp', 'fp', 'areaUnderCurve');

% ---- 

load([root 'performance_20-30.mat'], 'tp', 'fp', 'areaUnderCurve');
tp_20_30 = tp;
fp_20_30 = fp;
auc_20_30 = areaUnderCurve;
                
clear('tp', 'fp', 'areaUnderCurve');

% ---- 
                
load([root 'performance_40-50.mat'], 'tp', 'fp', 'areaUnderCurve');
tp_40_50 = tp;
fp_40_50 = fp;
auc_40_50 = areaUnderCurve;
                
clear('tp', 'fp', 'areaUnderCurve');

% ---- 
                
load([root 'performance_70-80.mat'], 'tp', 'fp', 'areaUnderCurve');
tp_70_80 = tp;
fp_70_80 = fp;
auc_70_80 = areaUnderCurve;
                
clear('tp', 'fp', 'areaUnderCurve');

% ---- 
                
load([root 'performance_90-100.mat'], 'tp', 'fp', 'areaUnderCurve');
tp_90_100 = tp;
fp_90_100 = fp;
auc_90_100 = areaUnderCurve;
                
clear('tp', 'fp', 'areaUnderCurve');


figure()

p = plot(fp_0_10, tp_0_10, ...
    fp_20_30, tp_20_30, ...
    fp_40_50, tp_40_50, ...
    fp_70_80, tp_70_80, ...
    fp_90_100, tp_90_100);
legend('0-10 km', '20-30 km', '40-50 km', '70-80 km', '90-100 km', 'Location','SouthEast')
%title('Android ROC, magnitude 5-6')
h = get(gca, 'title');
set(h, 'FontSize', 16)
set(p,'LineWidth',2)
set(gca, 'FontSize', 16)
xlabel('p_0')
ylabel('p_1')
%xlabel('sensor false positive rate');
%ylabel('sensor true positive rate');

saveas(gcf, 'experiments/debs11/plots/aggregate_android_roc', 'fig');
saveas(gcf, 'experiments/debs11/plots/aggregate_android_roc', 'png');
saveas(gcf, 'experiments/debs11/plots/aggregate_android_roc', 'pdf');



%% Make Convex Hulls of Android ROC curves
clear;

import roc.*

root = 'experiments/debs11/';

A = load([root 'performance_0-10.mat'], 'tp', 'fp', 'areaUnderCurve');
B = load([root 'performance_20-30.mat'], 'tp', 'fp', 'areaUnderCurve');
C =load([root 'performance_40-50.mat'], 'tp', 'fp', 'areaUnderCurve');
D = load([root 'performance_70-80.mat'], 'tp', 'fp', 'areaUnderCurve');
E = load([root 'performance_90-100.mat'], 'tp', 'fp', 'areaUnderCurve');

rocA = ReceiverOperatingCharacteristic(A.tp, A.fp);
rocA = rocA.convexHull();

rocB = ReceiverOperatingCharacteristic(B.tp, B.fp);
rocB = rocB.convexHull();

rocC = ReceiverOperatingCharacteristic(C.tp, C.fp);
rocC = rocC.convexHull();

rocD = ReceiverOperatingCharacteristic(D.tp, D.fp);
rocA = rocA.convexHull();

rocE = ReceiverOperatingCharacteristic(E.tp, E.fp);
rocE = rocE.convexHull();

convexAndroidROCs.tpr0_10 = rocA.truePositiveRates; 
convexAndroidROCs.fpr0_10 = rocA.falsePositiveRates; 

convexAndroidROCs.tpr20_30 = rocB.truePositiveRates; 
convexAndroidROCs.fpr20_30 = rocB.falsePositiveRates; 

convexAndroidROCs.tpr40_50 = rocC.truePositiveRates; 
convexAndroidROCs.fpr40_50 = rocC.falsePositiveRates; 

convexAndroidROCs.tpr70_80 = rocD.truePositiveRates; 
convexAndroidROCs.fpr70_80 = rocD.falsePositiveRates; 

convexAndroidROCs.tpr90_100 = rocD.truePositiveRates; 
convexAndroidROCs.fpr90_100 = rocD.falsePositiveRates; 

save('experiments/debs11/convexAndroidROCS', 'convexAndroidROCs')


