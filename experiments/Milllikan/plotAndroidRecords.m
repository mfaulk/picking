%% Plot the Android recordings from the Millikan experiment
import phone.*
import check.*
import file.*

dataDir = 'data/Millikan';
assertDirectory(dataDir);


[cFiles, cNames] = listFiles( dataDir, 'acsn');
nFiles = length(cFiles);


for i=1:nFiles
    file = cFiles{i};
    name = cNames{i};
    disp(name)
    
    D = load(file);
    
    accel = D(:,1:3)';
    t = D(:,4)'; % in nanoseconds
    times = t * 10^-9; % convert to seconds
    times = times - times(1);
    
    xLabelString = 'seconds';
    
    % if the record is long, scale to minutes?
    if max(times) > 300
        times = times / 60;
        xLabelString = 'minutes';
        duration = times(end) - times(1) ;
        disp(['Duration: ' num2str(duration) ' minutes']);
    else
        duration = times(end) - times(1);
        disp(['Duration: ' num2str(duration) ' seconds']);
    end

    h = figure();
    plot(times, accel);
    title(file);
    xlabel(xLabelString)
    ylabel('m/s^2')
    saveas(h, name, 'png');
    close(h);
    
end