function [label, model, llh, nIterations] = wemgm(X, weights, init, vis)
% Perform EM algorithm for fitting the Gaussian mixture model on a weighted data set.
% 
% Input:
%   X - d x n data matrix
%   weights - 1 x n vector of nonnegative weights
%   init - k (1 x 1) or label (1 x n, 1<=label(i)<=k) or center (d x k)
%   vis - true to visualize
%
% Outputs:
%   label - [1 x n] matrix. label(i) is the cluter index data point i is
%           assigned to (ie, cluster label(i) has the highest 
%           responsibility for X(:,i)) in the returned model.
%   model - struct. The found Gaussian mixture model.
%   llh - a scalar [1 x 1]. "Log-Likeli-Hood" is the log of the likelihood
%         function EM algorithm maximizes.
%
% Written by Michael Chen (sth4nth@gmail.com).
%% initialization

    import gmm.*;

    if ~exist('vis','var')
        vis = false;
    end

    %fprintf('EM for Gaussian mixture: running ... \n');

    % initialization is basically "expectation" for the first round.
    R = initialization(X, weights, init);
    [~,label(1,:)] = max(R,[],2);
    R = R(:,unique(label));

    tol = 1e-6;
    maxiter = 500;
    % llh (Log-Likeli-Hood) : [1 x maxiter], initialized to -infinity for 
    %                         maximization
    llh = -inf(1,maxiter); 
    converged = false;
    t = 1;
    while ~converged && t < maxiter
        if vis
            hold off
            nw = max(weights,1e-6)/max(weights);
            scatter(X(1,:),X(2,:),nw*20);
            hold on    
        end

        t = t+1;
        model = maximization(X,weights,R);
        [R, llh(t)] = expectation(X,weights,model);
        if vis
            for i = 1:size(model.Sigma,3)
                h=PlotEllipse(model.Sigma(:,:,i),model.mu(:,i));
                set(h,'color','k');
            end
        end


        [~,label(1,:)] = max(R,[],2);
        idx = unique(label);   % non-empty components
        if size(R,2) ~= size(idx,2)
            R = R(:,idx);   % remove empty components
        else
            converged = llh(t)-llh(t-1) < tol*abs(llh(t));
        end

        if vis
            pause
        end

    end
    llh = llh(2:t);     % The final loglikelihood.
    if converged
        %fprintf('Converged in %d steps.\n',t-1);
    else
        fprintf('Not converged in %d steps.\n',maxiter);
    end
    nIterations = t-1;







    function R = initialization(X, weights, init)
    %{

    Inputs:
        X, weights, init - See documentation for wemgm above.

    Outputs:
        R - [n x init] responsibility matrix. Initially each entry of R is  
            either 0 or 1, that is, a data point fully belongs to exactly 1 
            cluster. Later on each entry will fall in [0,1].

    Description:
        Case 1:
            If init is a model struct, find the assignment of data points to
            a cluster by their responsibility (or posterior probability).
        Case 2:
            If init is [1 x 1] (k) scalar, i.e., init = k then initialization 
            randomly pick k data points and cluster X using Euclidean
            distance into k clusters. 
        Case 3:
            If init is [1 x n] (label) vector, simply expand it to the 
            representation of R.
        Case 4:
            If init is [d x k] (k centers), cluster X using Euclidean distance 
            like in case 2.

    %}
        import gmm.*;
        [d,n] = size(X);
        if isstruct(init)  % initialize with a model
            R  = expectation(X, weights, init); % Bug fix (Dai Wei)
        elseif length(init) == 1  % random initialization
            k = init;

            % Randomly pick k data points as the means (center) of k gaussians.
            idx = randsample(n,k, true, 1./weights);
            m = X(:,idx);   % m - (d x k) centers

            % label (n x 1) - label(i) is the center closest to X(:,i).
            % Note: argmax_over_j{ X(:,i) dot m(:,j) - 0.5 * |m(:,j)|^2 }
            %       is equivalent to argmin_over_j{ |X(:,i) - m(:,j) | ^2 }
            [~,label] = max(bsxfun(@minus,m'*X,sum(m.^2,1)'/2),[],1);
            while k ~= unique(label)
                idx = randsample(n,k);
                m = X(:,idx);
                [~,label] = max(bsxfun(@minus,m'*X,sum(m.^2,1)'/2),[],1);
            end
            % R - [n x k] matrix where R(i,j) = 1 iff m(:,j) is the closest 
            %     center to data X(:,i). Intuitively, it's the binary matrix 
            %     representation of label.
            R = full(sparse(1:n,label,1,n,k,n));
        elseif size(init,1) == 1 && size(init,2) == n  % initialize with labels
            label = init;
            k = max(label);
            R = full(sparse(1:n,label,1,n,k,n));
        elseif size(init,1) == d  %initialize with only centers
            k = size(init,2);
            m = init;
            [~,label] = max(bsxfun(@minus,m'*X,sum(m.^2,1)'/2),[],1);
            R = full(sparse(1:n,label,1,n,k,n));
        else
            error('ERROR: init is not valid.');
        end
    end






    function [R, llh] = expectation(X, weights, model)
    %{

    Inputs:
        X, weights - See documentation for wemgm above.
        model - struct of GMM. See the output of maximization function.

    Outputs:
        R - See the output of initialization function.
        llh - log-likelihood. See documentation for wemgm above.

    %}
        import gmm.*;
        mu = model.mu;
        Sigma = model.Sigma;
        w = model.weight;

        n = size(X,2);
        k = size(mu,2);

        % Log of responsibility matrix R
        logR = zeros(n,k);

        for i = 1:k
            logR(:,i) = loggausspdf(X,mu(:,i),Sigma(:,:,i));
        end

        % w * R is addition in log.
        logR = bsxfun(@plus,logR,log(w));
        T = logsumexp(logR,2);      % T ([n x 1] matrix). T(i) is the normalization
                                    %   constant for each data point.
        llh = (weights*T)/sum(weights);     % loglikelihood
        logR = bsxfun(@minus,logR,T);   % normalize each data point.
        R = exp(logR);
    end




    function model = maximization(X, weights,parR)
    %{

    Inputs:
        X, weights - See documentation for wemgm above.
        parR - [n x init] matrix. It's R from the ouput of initialization 
               function.

    Outputs:
        model - Struct of Gaussian mixture model that maximizes the likelihood 
                of X given clustering assignment parR.

    %}
        import gmm.*;
        %if (weights(1)~=1)
        %    1;
        %end
        [d] = size(X,1);
        n = sum(weights);
        k = size(parR,2);

        % R is the responsibility-weighted version of parR.
        R = (max(weights,1e-6)'*ones(1,k)).*parR;
        % Rrt = (sqrt(max(weights,1e-6))'*ones(1,k)).*parR;

        s = sum(R,1);
        % srt = sum(Rrt,1);
        w = s/n;                        % mixing-weights of each mean.
        mu = bsxfun(@times, X*R, 1./s); % new means.

        % Compute Sigma.
        Sigma = zeros(d,d,k);
        for i = 1:k
            Xo = bsxfun(@minus,X,mu(:,i));          % Substract mean
            Xo = bsxfun(@times,Xo,sqrt(R(:,i)'));   % Weight by R^(0.5)
            Sigma(:,:,i) = Xo*Xo'/s(i);             % The new Sigma.
            Sigma(:,:,i) = Sigma(:,:,i)+eye(d)*(1e-3); % add a prior for numerical stability
        end

        model.mu = mu;
        model.Sigma = Sigma;
        model.weight = w;

    end



    function y = loggausspdf(X, mu, Sigma)
    %{

    Inputs:
        X - See documentation for wemgm above.

    Outputs:
        y - [1 x n] matrix representing log(N(X|mu, Sigma)), that is, the 
            probability of the log of normal distribution with 
            [mean, deviation] = [mu, Sigma] at each data point of X.

    %}
        import gmm.*;
        d = size(X,1);
        X = bsxfun(@minus,X,mu);

        % Cholesky Factorization. Return value p suppress none-PD error, 
        %   which we show in (if p ~= 0). R = Sigma^(1/2)
        [R,p]= chol(Sigma);
        if p ~= 0
            error('ERROR: Sigma is not PD.');
        end
        q = sum((R'\X).^2,1);  % quadratic term (M distance)
        c = d*log(2*pi)+2*sum(log(diag(R)));   % normalization constant
        y = -(c+q)/2;
    end






    function s = logsumexp(x, dim)
    % Compute log(sum(exp(x),dim)) while avoiding numerical underflow.
    %   By default dim = 1 (columns).
    % Written by Michael Chen (sth4nth@gmail.com).
        if nargin == 1, 
            % Determine which dimension sum will use
            dim = find(size(x)~=1,1);
            if isempty(dim), dim = 1; end
        end

        % subtract the largest in each column
        y = max(x,[],dim);
        x = bsxfun(@minus,x,y);
        s = y + log(sum(exp(x),dim));
        i = find(~isfinite(y));
        if ~isempty(i)
            s(i) = y(i);
        end
    end

end

