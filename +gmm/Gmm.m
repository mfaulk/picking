classdef Gmm
    %GMM Core Gaussian Mixture Model functionality
    %   Detailed explanation goes here
    %
    % author: Matt Faulkner
    %
    % TODO: consider updating to use gmdistribution in Matlab's stats
    % toolbox. That provides an EM implementation as well that allows for
    % regularizing to keep covarinace matrices positive definite.
    
    % ====================================================================
    
    properties
        nGaussians      % number of Gaussians, also called "k"
        dataDimensions  % dimensionality of data point
        
        mu              % (d x k) Each column is a dx1 vector of means
        Sigma           % (d x d x k) Each page (i.e. S(:,:,i)) is a dxd covariance matrix
        weight          % (k x 1) Mixing weights
    end
    
    
    % ====================================================================
    
    methods
        
        % ----------------------------------------------------------------
        
        function obj = Gmm(mu, Sigma, weight)
            % constructor
            %
            % mu - (d x k) Each column is a dx1 vector of means
            % Sigma - (d x d x k) Each page (i.e. S(:,:,i)) is a dxd covariance matrix
            % weight - (k x 1) Mixing weights
            %
            import gmm.*

            [d,k] = size(mu);
            
            if any(size(Sigma) ~= size(zeros(d,d,k)))
                error('Sigma has inconsisten dimensions')
            end
            
            if any(size(weight) ~= size(zeros(k,1)) )
                error('weight has inconsistent dimensions')
            end
            
            if size(weight,2) ~=1
                error('weight has more than one column')
            end
            
            obj.nGaussians = k;
            obj.dataDimensions = d;
            obj.mu = mu;
            obj.weight = weight;
            obj.Sigma = Sigma;
        end
        
        % ----------------------------------------------------------------
        
        function data = sample(obj, n)
            % Input:
            %    n - number of samples to generate
            %
            % Output:
            %    data - [d x n] matrix. Each column is a sample point.
            import gmm.*
            
            label = randsample(obj.nGaussians,n,true,obj.weight);
            data = zeros(obj.dataDimensions,n);
            
            for i = 1:obj.nGaussians
                ni = sum(label==i);
                data(:,label==i) = ...
                    mvnrnd(obj.mu(:,i), obj.Sigma(:,:,i), ni)';
            end
        end
        
        % ----------------------------------------------------------------
        
        function p = evaluateProbability2D(obj, x1, x2, d1, d2, j, gmean)
        %{
            
        Descriptions:    
            This is to assist plotting projected gaussian contour plot on
            2D (in genericCoresetTestUtil.plot17dDataNGmm). 
            
        Inputs:
            x1, x2 - scalars. (x,y) coordinates on the 2D (d1-d2) plot.
            d1, d2 - scalars. the d1-th and d2-th dimensions.
            j - scalar. Evaluate only the probability from the j-th gaussian component.
            gmeans - [d x 1] vector. The mean of the j-th gaussian. This is 
                    to help finding the best slice for 2D.
            
        Outputs:
            p - scalar. Probability at the point.
            
        Author: Dai Wei.
            
        %}
            x = gmean;
            x(d1) = x1;
            x(d2) = x2;
            
            %p = obj.evaluateProbability(x, k);
            muJ = obj.mu(:,j);
            sigmaJ = obj.Sigma(:,:,j);
            % mvnpdf wants x and mu as row vectorsdists = emGaussian(data, k);
            weightJ = obj.weight(j);
            p = weightJ * mvnpdf(x', muJ', sigmaJ);
            
        end
        
        % ----------------------------------------------------------------
        
        function P = evaluateProbability(obj, X, k)
            % Evaluate the probability of data under the Gaussian mixture model
            %
            % Input:
            %    X - (d x n) matrix. Each column is a data point.
            %    k - (optional) scalar. If you want to include only the kth
            %    gaussian. (by Dai Wei)
            %
            % Output:
            %    P - (n x 1) vector of probabilities
            %
            % WARNING: this may be numerically sensitive (produce inf, NaN)
            %
            import gmm.*
            
            if size(X,1) ~= obj.dataDimensions
               size(X)
               obj.dataDimensions
               error('Gmm.evaluateProbability: X has wrong dimensionality.')
            end
            
            if exist('k','var')
                gussian_low = k;
                guassian_high = k;
            else
                gussian_low = 1;
                guassian_high = obj.nGaussians;
            end
            
            nDataPoints = size(X,2);
            
            P = zeros(nDataPoints,1);
            
            for i = 1:nDataPoints
                p = 0;
                x = X(:,i);
                for j = gussian_low : guassian_high
                    muJ = obj.mu(:,j);
                    sigmaJ = obj.Sigma(:,:,j);
                    % mvnpdf wants x and mu as row vectorsdists = emGaussian(data, k);
                    pJ = mvnpdf(x', muJ', sigmaJ);
                    weightJ = obj.weight(j);
                    p = p + weightJ * pJ;
                end
                P(i) = p;
            end
        end
        
        % ----------------------------------------------------------------
        
        function L = evaluateLogProbability(obj, X)
            % Evaluate the log probability of data under the Gaussian mixture model
            %
            % Input:
            %    X - (d x n) matrix. Each column is a data point.
            %
            % Output:
            %    L - (n x 1) vector of log probabilities
            %
            import gmm.*
            
            if size(X,1) ~= obj.dataDimensions
               size(X)
               obj.dataDimensions
               error('Gmm.evaluateProbability: X has wrong dimensionality.')
            end
            
            nDataPoints = size(X,2);
            
            L = zeros(nDataPoints,1);
            
            parfor i = 1:nDataPoints
                x = X(:,i);
                tempGmm = obj;
                tempGmm.weight = tempGmm.weight';
                L(i) = computeLLH(x, tempGmm);
            end
            
        end
        
        % ----------------------------------------------------------------
        
        function llh = logLikelihood(obj, X)
            % Input:
            %   X - (d,n) matrix of data points
            % Output:
            %   llh - scalar? log likelihood of X under the GMM
            % 
            %n = size(X,2);
            import gmm.*
            
            tempGmm = obj;
            tempGmm.weight = tempGmm.weight';
            llh = computeLLH(X, tempGmm);
            llh = llh';
        end
     end
    
    % ====================================================================
    
    
    methods(Static)
        
        % ----------------------------------------------------------------
        
        function [obj, nIters] = fitGmm(X, k, nRestarts)
           % Fit a GMM with k mixture components to a data set X.
           % Uses the EM algorithm. The best model of several runs is
           % returned.
           %
           % Input:
           %    X - (d,n) matrix of data points
           %    k - number of mixture components in the GMM
           %    nRestarts - number of runs of EM to use.
           %
           % Output:
           %    obj - a Gmm object.
           %    nIters - number of EM iterations for the returned model to
           %        converge.
           %
           
           % This is a wrapper for 
           %gm,  written by Michael Chen
           % (sth4nth@gmail.com), available at
           % http://www.mathworks.com/matlabcentral/fileexchange/26184-em-algorithm-for-gaussian-mixture-model
           %
           import gmm.* 
           
           n = size(X,2);
           uniformWeights = ones(1,n);
           [obj, nIters] = Gmm.fitGmmWeighted(X, uniformWeights, k, nRestarts);
        end
        
        
        % ----------------------------------------------------------------
        
        function [obj, nIters] = fitGmmWeighted(X, weights, k, nRestarts)
           % Fit a GMM with k mixture components to a data set X.
           % Uses the EM algorithm. The best model of several runs is
           % returned.
           %
           % Input:
           %    X - (d,n) matrix of data points
           %    weights - (1,n) vector of data point weights
           %    k - number of mixture components in the GMM
           %    nRestarts - number of runs of EM to use.
           %
           % Output:
           %    obj - a Gmm object.
           %    nIters - number of EM iterations for the returned model to
           %        converge.
           %
           
           % This is a wrapper for wemgm,  written by Michael Chen
           % (sth4nth@gmail.com), available at
           % http://www.mathworks.com/matlabcentral/fileexchange/26184-em-algorithm-for-gaussian-mixture-model
           %
           import gmm.*
           
           [~, model, ~, nIters] = wemgm_class.wemgm_restart(X, weights, k, nRestarts);
           obj = Gmm(model.mu, model.Sigma, model.weight');
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        function [obj] = gmmWeightedStep(X, k, nk, model, weights)
        
           import gmm.*
           
           if ~exist('weights','var')
                n = size(X,2);
                weights = ones(1,n);
           end
           
           
           [~, newmodel, ~, ~] = wemgm_class.wemgmStep(X, weights, model);
           nk = nk*(sum(newmodel.weight)/(sum(model.weight)+sum(newmodel.weight))); %Decreasing the effect low peaks have on learning
           obj = Gmm(((1-nk)*model.mu + nk*newmodel.mu), ((1-nk)*model.Sigma + nk*newmodel.Sigma), ((1-nk)*model.weight' + nk*newmodel.weight'));

        end

        
    end
    % ====================================================================
        
end

