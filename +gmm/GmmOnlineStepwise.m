classdef GmmOnlineStepwise
%{

    GmmOnline provides the abtraction for computing Gmm in a stepwise,
    online fashion. 
    
    TODO: 
    
    Author: Dai Wei
    Author: Alejandro Carbonara
%}

    
    properties %(GetAccess = private)
        
        nUpdatesMax     % [1 x 1], scalar: expected number of updates, 
                        %   which is also  the maximum number of calls
                        %   to function fitGmmOnlineStepwise(...).
        
        nUpdates        % [1 x 1], scalar: the number of updates, i.e., 
                        %   the number of calls to fitGmmOnlineStepwise
                        %   so far.
        
        cGmmModels      % [nUpdatesMax x 1] (column) cell arrays of 
                        %   Gmm models (represented by Gmm  
                        %   objects) computed over nupdates data chunks.
                        %   cGmmModels{i} is the Gmm model computed after 
                        %   receiving the i-th chunck of data stream.
                        %   Note that cGmmModels{0} is uninitialized.
                        
        chunksize       % scalar: number of data points in each chunk. 
                        %   It has to be consistent across all incoming 
                        %   stream of data chunks to maintain the numerical 
                        %   stability of stepwise online EM.
                        
        historyindex    % scalar: used to loop through history
                        %
                        
        history         % [historysize x 1]: used to store history
                        %    Size is inputted by user.
                        
        % Gaussian fitting parameter
        k               % [1 x 1], scalar. A Gaussian fitting parameter:   
                        %   number of mixture components in the GMM
                        
    end
    
    methods
        
        function obj = GmmOnlineStepwise(nUpdatesMax, csize, k, historysize)
            %{
            
            Constructor.
            
            Input:
                nupdates - [1 x 1], scalar. See documentation in 
                           "properties" above.
                csize - [1 x 1], scalar: chunksize. See documentation for 
                        chunksize in "properties" above.
                k - 
                nRestarts - [1 x 1], scalar. number of runs of EM to use.
                
                historysize- The number of chunks that the algorithm
                             remembers before combining them 
            %}
        
            obj.nUpdatesMax = nUpdatesMax;
            obj.nUpdates    = 1;
            obj.cGmmModels  = cell(obj.nUpdatesMax, 1);
            obj.chunksize = csize;
            obj.k = k;
            obj.historyindex = 1;
            obj.history = [];
        end
        
        % ----------------------------------------------------------------
        
        function fitGmmOnlineStepwise(obj, X, nRestarts)
           %{
            
            Inputs:
                X - [d x n] matrix of data points. Remember that principle
                    component analysis (PCA), if conducted, should only be 
                    done on this segment of data to simulate the streaming 
                    data. 
                    [ Further note: n = chunksize ]
                nRestarts - scalar: number of runs of EM to use.
            
            Outputs:
                gmm - Gmm object containing the updated gmm model
                      parameters.
                nIters - number of EM iterations for the returned model to
                         converge.
            
            %}
            import gmm.*
            assert(obj.nUpdates < obj.nUpdatesMax);
            %assert(length(X) == obj.chunksize);
            obj.nUpdates = obj.nUpdates + 1;

            if obj.historyindex == size(obj.history,1)
                obj.history = [obj.history, X];
                obj.historyindex = 1;
              
                % Find gmm_t, the gmm learned from the t-th chunk of data 
                %   stream.
                [gmm_t, nIters] = Gmm.fitGmm(obj.history, obj.k, nRestarts);
                obj.cGmmModels{nUpdates} = gmm_t;
                obj.history = [];
            else
                obj.history = [obj.history, X];
                
                %
                obj.history
                obj.k
                nRestarts
                %
                
                [gmm_t, nIters] = Gmm.fitGmm(obj.history, obj.k, nRestarts);
                obj.cGmmModels{nUpdates}=gmm_t;
            end
        end
        
        
        % ----------------------------------------------------------------
        
        function gmm = getGmmModel(obj, t)
            %{
            
            Inputs:
                t - scalar. This function will return the model learned at
                    time t. See outputs.
            
            Outputs:
                gmm - a Gmm object, which is a Gmm model computed after 
                      receiving the i-th chunck of data stream.
                      [ Note that cGmmModels{0} is uninitialized.]
            
            %}
            
            assert(t <= nUpdates);
            gmm = obj.cGmmModels{t};
        end
        
    end
    
end

