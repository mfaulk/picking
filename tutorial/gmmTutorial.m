%% Tutorial for Gaussian Mixture models and EM
%
% author: Matt Faulkner
%
% Note: Matlab provides the gmdistribution class, which implements some of
% the same GMM functionality.


%% Construct a GMM, and plot the PDF

import gmm.*

% number of components:
k = 10;

% dimensionality:
d = 2;

% create means in some bounded square:
scaleFactor = 10;
mu = scaleFactor*rand(k,d) - scaleFactor/2;

% and random variance
sigma = zeros(d,d,k);
for i=1:k
   sigma(:,:,i) = 5 * rand(d,d) .* eye(d); 
end

% mixing weights
p = rand(1,k);
p = p / sum(p);

model = Gmm(mu', sigma, p');

% evaluate the pdf over a grid
x = -10:0.5:10;
y = -10:0.5:10;
z = zeros(length(x), length(y));
for i=1:length(x)
    for j=1:length(y)
        z(i,j) = model.evaluateProbability([x(i) ; y(j)]);
    end
end

% and plot the pdf
figure()
surf(x,y,z)
title(['GMM with ' num2str(k) ' mixture components'])
grid off
axis off
%shading interp

%hold on;
% level set (threshold)
%t = max(z(:)) / 10;
%[C,lh]=contour3(x,y,z,[t,t], 'r'); % to draw a single contour at level t, use [t,t]. Way to suck, Matlab.
%set(lh,'linewidth',3);

% -------------------------------------------------------------------------

% Fit a new GMM to data sampled from the above GMM.
% This fits a GMM using the weighted EM algorithm. Weights are set to be
% uniform.

nSamples = 1000;
X = model.sample(nSamples);

newk = 5;

newModel = gmm.Gmm.fitGmmWeighted(X, ones(1,nSamples), newk, 10);

for i=1:length(x)
    for j=1:length(y)
        z(i,j) = newModel.evaluateProbability([x(i) ; y(j)]);
    end
end

figure()
surf(x,y,z)
title(['GMM fit using ' num2str(newk) ' mixture components'])
grid off
axis off