%% sacTutorial - Loading and processing Seismic Analysis Code files.
%
% author: Matt Faulkner
%
% ------------------------------------------------------------------------
% Please see the following references as well:
% Intro to SAC, http://geophysics.eas.gatech.edu/classes/SAC/
% SAC users guide, http://www.iris.edu/software/sac/manual.html
% Where to get some SAC files, http://www.data.scec.org/STP/stp.html
% ------------------------------------------------------------------------
import sac.*

sacDir = 'data/sac/6-8__HN';

% Look at the data. (Press enter to see the next one)

viewSacWaveforms(sacDir);

% Loading SAC data

cSacRecords = SacLoader.loadSacRecords(sacDir);

% can get meta-data:
sacRecord = cSacRecords{1}
disp(['sample rate: ' num2str(sacRecord.sampleRate)])
disp(['distance to epicenter: ' num2str(sacRecord.distanceToEvent) ' km'])

figure()
hold on
plot(sacRecord.eWaveform.accel, 'r')
plot(sacRecord.nWaveform.accel, 'g')
plot(sacRecord.zWaveform.accel, 'b')




