%% phoneTutorial - loading and processing Android accelerometer data (.acsn files)
%
% author: Matt Faulkner
% June 27, 2011

%% load a .acsn file, and plot
import phone.*

acsnFile = 'tutorial/acsn/m2.acsn';
acsnDir = 'tutorial/acsn';

phoneRecord = PhoneRecord(acsnFile);

% you could also use phoneRecord.display() to plot
figure()
title(acsnFile)
t = phoneRecord.data(4,:);
plot(t, phoneRecord.data(1:3, :))
xlabel('seconds')
ylabel('m/s^2');

%% load a directory of .acsn files

% I often name cell array variables to start with a 'c' 
cPhoneRecords = PhoneRecord.loadPhoneRecordDir(acsnDir);
disp(['Loaded ' num2str(length(cPhoneRecords)) ' phone records']);

%% segment PhoneRecord objects into 2 second time series

% the synthetic package contains functions for making synthetic
% earthquakes, but also some general purpose code for segmenting phone
% records
import synthetic.*

samplesPerSecond = 50; 

% later, I'm going to make Long-term, short-term features, so I'll make the
% segments long enough to leave room for those computations:
ltLength = 5; % seconds
stLength = 2.5; % seconds
deadTime = 1; % seconds of "dead time" between short term and long term buffers
segmentLengthSeconds = ltLength + stLength +deadTime;

% these are UniformTimeSeries objects, in the timeSeries package.
cPhoneSegments = SegmentSynthesizer.segmentPhoneRecord(cPhoneRecords{1}, segmentLengthSeconds, samplesPerSecond);
plot(cPhoneSegments{1}.getTimes, cPhoneSegments{1}.X);

% alternatively, you could segment each file and concatenate the segments, like this:
cPhoneSegments = {};
for i=1:length(cPhoneRecords)
   cTemp = SegmentSynthesizer.segmentPhoneRecord(cPhoneRecords{i}, segmentLengthSeconds, samplesPerSecond);
   cPhoneSegments = [cPhoneSegments; cTemp]; % for some reason using {} here doesn't work. Look into this...
end
clear cTemp;


%% create synthetic earthquakes
% (see the sac tutorial)

import sac.*
import synthetic.*

sacDir = 'data/sac/6-8__HN';
cSacRecords = SacLoader.loadSacRecords(sacDir);

threshold = 0.1;
peakAmplitude = 0; % this isn't used anymore

cSyntheticSegments = ...
                SegmentSynthesizer.createOnsetSegments(cPhoneSegments, cSacRecords, ...
                ltLength+deadTime, stLength, threshold, peakAmplitude, samplesPerSecond);

%% compute some features?