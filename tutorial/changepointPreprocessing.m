%% Examples for loading, processing and saving changepoint data sets.
% 
% Make sure that the Picking directory is on Matlab's path. The path can be
% permanently updated via File >> Set Path...

% author: Matt Faulkner

%% Produce "chunks" of phone data, suitable for evaluating the rate of 
% change points due to normal user accelerations.

import parallel.*
import check.*
import phone.*
import synthetic.*
import check.*

disp('Starting...')
datestr(now)

% path to phone data directory
phoneDataDir = '/cs/research/mls/csnAndroidLogs/A';
if ~isDirectory(phoneDataDir)
   disp([phoneDataDir ' cannot be found or is not a directory']); 
end

% length of each chunk, in seconds
segmentLengthSeconds = 3600; % 1 hour

% samples per second
samplesPerSecond = 50;

% to enable parallel execution in parfor loops
openPool;

% the gravity removal step of loading a phone record is expensive, but it
% is parallelized, so make sure the matlab pool is open
cPhoneRecords = PhoneRecord.loadPhoneRecordDir(phoneDataDir);
disp(['Loaded ' num2str(length(cPhoneRecords)) ' phone records']);

% Segment the phone records to obtain a large number of shorter time series

% these are UniformTimeSeries objects, in the timeSeries package.

cPhoneSegments = {};

for i=1:length(cPhoneRecords)
    % Note: this will throw an error if the phone record is not long
    % enough
    cNew = SegmentSynthesizer.segmentPhoneRecord(cPhoneRecords{i}, segmentLengthSeconds, samplesPerSecond);
    cPhoneSegments = [cPhoneSegments ; cNew];
end

save('tutorial/hourSegments', cPhoneSegments)

disp('Done.')
datestr(now)

%% 

load('tutorial/hourSegments.mat');  

seg = cPhoneSegments{2};
plot(seg.getTimes, seg.X);
xlabel('seconds')
ylabel('m/s^2')

%% Create synthetic phone recordings of quakes, by overlaying earthquake
% signals onto phone data. 

% at what point in the time series should a quake be overlaid? Random time?
% Middle time? At a given time?

% 1) create segments of SAC data (onset segments?)
% 2) superimose SAC segments onto phone data

import sac.*

sacDir = 'data/sac/6-8__HN';

% Look at the data. (Press enter to see the next one)
%viewSacWaveforms(sacDir);

w1 = 5; % lead-in time, in seconds
w2 = 150; % lead-out time, in seconds
threshold = 0.05; % m/s^2

cSacOnsetSegments = SacLoader.getOnsetSegments(sacDir, w1, w2, threshold);

% A sanity check:
s = cSacOnsetSegments{1};
plot(s.getTimes, s.X);
clear s;
%% Combine
% Probably the easiest way to add the quake segments at a particular time
% in the phone time series is to set the start time of the quake segments
% to be the desired time, and then add the time series.

import timeSeries.*


quakeStartTimeSeconds = 1000; % time, in seconds, within the phone data to add a quake

shiftFn = @(timeSeries) timeSeries.setStartTime(quakeStartTimeSeconds); 
cShiftedSacOnsetSegments = cellfun(shiftFn, cSacOnsetSegments, 'UniformOutput',0);

% A sanity check:
s = cShiftedSacOnsetSegments{1};
plot(s.getTimes, s.X);
clear s;

% randomly add one of the SAC segments to each of the phone segments
nSacSegments = length(cShiftedSacOnsetSegments);
cSyntheticPhoneSegments = cell(size(cPhoneSegments));
for i=1:length(cPhoneSegments)
    import random.*
    phoneSegment = cPhoneSegments{i};
    j = randomSubset(nSacSegments,1);
    sacSegment = cShiftedSacOnsetSegments{j};
    syntheticPhoneSegment = phoneSegment.add(sacSegment);
    cSyntheticPhoneSegments{i} = syntheticPhoneSegment;
end
    
synth = cSyntheticPhoneSegments{6};
plot(synth.getTimes, synth.X);
    
    
%% Concatenate phone segments into one multi-dimensional time series.
% Useful for testing centralized changepoint detection in networks.

% Extract and concatenate the data matrices of each UniformTimeSeries object.
% Due to rounding differences in resampling, the time series may differ in
% length by a few samples. Truncate the time series to avoid this.

getDataLength = @(timeSeries) size(timeSeries.X, 2);
minSegmentLength = min(cellfun(getDataLength, cPhoneSegments));

getData = @(timeSeries) timeSeries.X(:,1:minSegmentLength); 
cSegmentData = cellfun(getData, cPhoneSegments, 'UniformOutput',0);

networkData = cell2mat(cSegmentData);

